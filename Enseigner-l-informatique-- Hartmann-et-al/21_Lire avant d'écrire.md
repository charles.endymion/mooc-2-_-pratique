**21**

**Lire avant d'écrire**

L'élève T. secoue la tête : « je n'arrive pas à présenter mon site Web
sur 3 colonnes sans passer par un tableau ! Les règles du W3C précisent
bien qu'il ne faut pas abuser des tableaux pour la mise en page, mais je
peux imbriquer mes éléments comme je veux, ça ne marche pas ! ».
L'enseignant identifie immédiatement la source du problème : par manque
de compréhension de la structure d'un site Web complexe, l'élève a
essayé de composer son propre site en assemblant de petits éléments qui
sont tout simplement inadaptés les uns aux autres. L'enseignant lui
conseille d'analyser un site existant. « Ben oui, j'ai déjà essayé »,
rétorque l'élève, « mais je ne l'ai pas vraiment compris ! ».
L'enseignant sait maintenant à quoi s'en tenir : il va aussi falloir
apprendre à lire.

**Problème :** la création d'un nouveau contenu est une tâche cognitive
exigeante souvent abordée dans l'enseignement sans que les étudiants
n'aient préalablement appris à lire les contenus existants. Les
étudiants adoptent alors fréquemment une approche empirique consistant à
ajouter petit à petit de nouveaux éléments, faire un essai, corriger,
refaire un essai, supprimer puis recommencer. Cette approche à tâtons
peut s'avérer utile en présence de petits problèmes, mais est inadaptée
aux difficultés de grande ampleur.

En informatique, il faut souvent élaborer ou même créer les contenus
soi-même : présentation, tableau, page Web ou même un programme.
L'enseignement commence en général directement par la création de
contenus, par exemple les sites Web : les supports de cours présentent
dans leur majorité quelques éléments de la structure tels que *h1, p* ou
*img* et illustrent à l'aide d'exemples simples comment les utiliser
dans la création d'un site Web. L'attente implicite est que les
étudiants, une fois qu'ils auront appris à connaître tous les éléments
nécessaires, soient en mesure de développer des sites plus élaborés sans
avoir préalablement analysé un exemple complexe. En réalité, ils
apprennent à « écrire » avant de « lire ».


Dans les matières classiques, il semble évident d'apprendre la lecture
avant l'écriture. Il est vrai que les enfants apprennent très tôt
l'écriture de lettres individuelles, mais il s'agit là de les entraîner
à avoir des gestes précis et l'écriture de textes entiers n'interviendra
que plus tard. Dans l'enseignement d'une langue étrangère aussi, la
lecture s'apprend avant l'écriture. En cours de physique, l'enseignant
accomplit lui-même de nombreuses tâches avant de demander aux élèves de
réaliser leurs propres expériences en toute autonomie. Cet ordre prend
également en compte les niveaux de plus en plus sophistiqués des
objectifs pédagogiques cognitifs tels qu'ils ont été définis par Bloom :
connaissances, compréhension, utilisation, analyse, synthèse et
évaluation -- la compréhension vient avant l'utilisation, l'analyse
avant la synthèse (voir par exemple [Blo56]).


Dans l'enseignement de l'informatique, peu d'importance est accordée à
la variante « lire avant d'écrire », notamment dans l'enseignement de la
programmation qui commence en général directement par l'écriture d'un
programme. Les étudiants créent de petits programmes mais les programmes
existants ne sont que rarement analysés. Robert Glass argumente en
faveur d'une formation dans laquelle les futurs programmeurs sont tout
d'abord initiés à la lecture [Gla02]. Il défend sa position en
évoquant notamment des recherches qui démontrent qu'une révision du code
permet d'identifier 60 à 90 % des erreurs dans un programme, chiffre
nettement supérieur à celui des autres approches d'une programmation
sans erreur.


Un autre point qui souligne l'importance économique de la lecture pour
le développement de logiciels : la maintenance représente 40 à 80 % du
coût total du développement et de l'utilisation d'un programme
[Gla02]. Corbi rappelle qu'en cas de modifications apportées aux
logiciels existants, plus de 50 % du temps est consacré à l'étude du
texte source et de la documentation, avec environ 3,5 fois plus de temps
passé sur le code source que sur la documentation [Cor89]. En résumé,
ces études signifient qu'une part considérable du coût total du
développement et de la maintenance des logiciels résulte de l'étude du
code source. Par conséquent, il est tout à fait justifié de faire de la
lecture des codes de programme un sujet d'enseignement.


Malgré son importance majeure, la « compréhension du programme » n'est
généralement pas un sujet reconnu, que ce soit en formation ou même dans
la littérature consacrée au développement de logiciels. Par conséquent,
il n'existe que très peu d'indications sur la manière d'enseigner la
lecture des programmes. Corbi mentionne trois approches : la première
est une approche ascendante qui consiste à commencer par étudier de
petits fragments de programme pour ensuite progresser vers des unités
sémantiques de plus en plus grandes. La deuxième approche est
descendante et commence par la structure grossière du programme pour
plonger de plus en plus profondément dans les détails. La troisième
approche est un mélange opportuniste des deux premières qui sont
combinées en fonction de la situation. L'approche choisie dépend au
moins en partie de ses préférences personnelles.


Un autre argument en faveur d'une lecture accrue des programmes dans
l'enseignement est que dans la pratique, les développeurs découvrent
continuellement des faits nouveaux en lisant les codes des programmes.
Ils sont constamment confrontés à des technologies qui leurs sont
inconnues et, dans ces situations, agissent généralement selon le modèle
Rechercher / Lire / Coller : ils recherchent des exemples significatifs
de la technologie concernée à l'aide d'un moteur de recherche, lisent
ces exemples et essaient de les comprendre, puis récupèrent l'un d'entre
eux pour le modifier et finalement l'utiliser pour résoudre leur
problème.


Cette méthode présente cependant un risque : sous la pression du temps,
la découverte des nouvelles technologies se limite à ce qui est
strictement nécessaire pour résoudre le problème courant et leur
compréhension reste alors superficielle.


**Solution :** avant de créer eux-mêmes leurs propres contenus en cours
d'informatique, il peut s'avérer très utile pour les étudiants de
commencer par lire et analyser des contenus existants. Ils disposent
alors d'une multitude d'exemples auxquels ils peuvent se référer lors de
la création de leurs propres contenus.

**Exemple 1 : formation appliquée à la présentation**

L'utilisation compétente des applications standard telles que le
traitement de texte, la PAO, les feuilles de calcul et les logiciels de
présentation, outre des connaissances techniques, impose également des
compétences fondamentales en matière de mise en forme. Les livres qui
traitent de ce sujet contiennent généralement plusieurs bons et mauvais
exemples de mise en page, et il est intéressant en cours d'examiner plus
précisément quelques-uns de ces exemples comparatifs, c'est-à-dire de
les « lire ». Des ouvrages tels que *Praktische* *Typographie* de Ralph
Turtschi [Tur00] offrent une première approche des domaines du
traitement de texte et de la PAO. Richard Mayer [May01], dans
*Multimedia Learning*, propose 7 principes économiquement fondés et
faciles à appliquer pour la mise en forme des graphiques et des dessins.


Le livre d'Edward Tufte, *The Visual Display of Quantitative
Information*, aborde les principes de mise en forme des diagrammes
quantitatifs [Tuf01]. Dans l'enseignement, ces principes peuvent par
exemple être traités dans un cours consacré aux diagrammes dans une
feuille de calcul. Les effets typiques de la manipulation des diagrammes
peuvent être traités en utilisant différentes représentations des mêmes
chiffres.


**Exemple 2 : lecture du HTML et du CSS**


La préparation techniquement correcte des sites Web suppose de bien
comprendre la séparation du contenu, de la structure et de la mise en
page. Les balises HTML définissent la structure d'une page et englobent
en même temps le contenu de cette dernière. Les éléments individuels de
cette structure sont mis en forme avec des instructions CSS. Il existe
de nombreuses tentatives de création de sites WEB avec des éditeurs
WYSIWYG, mais une compréhension réellement approfondie de la structure
d'un site Web ne peut s'acquérir que par un examen méticuleux du code
source HTML et CSS, par exemple le code source de sites Web bien
aboutis.



De nombreux sites Web sont inutilement compliqués et ne conviennent pas
comme exemples dans l'enseignement. Les fichiers CSS contiennent souvent
de nombreux petits « trucs » difficiles à comprendre et généralement non
documentés qui sont destinés à tenir compte des particularités des
différents navigateurs. Quelques sites Web sont explicitement consacrés
à la mise en œuvre techniquement correcte d'un design au graphique
attrayant, par exemple le projet www.csszengarden.com. Ce site Web se
base sur une unique page HTML dont l'observateur peut modifier la
présentation en sélectionnant un fichier CSS parmi les multiples
proposés.



La structure du site www.csszengarden.com a été élaborée avec soin afin
d'offrir le plus grand espace de liberté possible au concepteur qui
écrira le code CSS. Le nombre de balises HTML qui a été utilisé à cet
effet est plus élevé que ce qui serait normalement nécessaire pour une
telle mise en page. [L'exemple suivant montre le code HTML de
l'introduction « The Road to Enlightenment » :]{lang="en-GB"}


    <div id="preamble">
    <h3><span>The Road to Enlightenment</span></h3>
    <p class="p1"><span>Littering a dark and dreary road lay the
    past relics of browser-specific tags, incompatible
    <acronym title="Document Object Model">DOM</acronym>s, and
    broken <acronym title="Cascading Style Sheets">CSS</acronym>
    support.</span>
    </p> <!-- ... -->
    </div>


Cet exemple illustre comment le développeur du site Web emploie des
conteneurs *div* tels que *preamble* pour la séparation structurelle des
blocs du contenu. Une autre particularité est que le contenu proprement
dit ne se trouve pas directement dans des éléments tels que *p* ou *h3*,
mais en plus emballé dans un élément *span*.


La figure 21.1 représente une portion de ce site Web avec la mise en
page standard. Le fragment de code ci-après représente le fragment CSS
dont la fonction est la mise en forme d'un titre de section :


    h3 {
    font: italic normal 12pt georgia;
    letter-spacing: 1px;
    margin-bottom: 0px;
    color: #7D775C;
    }



<img alt="Fig. 21.1" src="Images/21.1_figure.jpg" width="450px"/>

**Fig. 21.1** Mise en page standard de www.csszengarden.com



Cette mise en page peut à présent être comparée à d'autres sans qu'il
soit pour autant nécessaire de modifier le code HTML. De nombreuses
mises en page font appel aux éléments *span*, qui sont en réalité
superflus, pour superposer une image à du texte. Le texte est toujours
présent et reste ainsi accessible aux moteurs de recherche et lecteurs
d'écran. La mise en page « Mozart », représentée dans la Figure 21.2,
fait appel à cette technique. Le fragment CSS suivant représente une
portion du code CSS correspondant qui illustre l'utilisation des
sélecteurs pour masquer un texte et afficher une image à sa place :


    #preamble h3 span {
    display: none;
    }
    #preamble h3 {
    margin: 0px;
    height: 26px;
    background: url(preamble.gif);
    }


Les observations ci-dessus relatives à la conception de sites Web
montrent à quel point la lecture du travail des autres peut être riche
en enseignements. Les bons exemples ne sont malheureusement pas toujours
faciles à trouver.


**Exemple 3 : lecture d'un code de programme**


Il existe en principe deux supports de cours pour la lecture d'un code
de programme dans l'enseignement. La première catégorie englobe les
ouvrages de formation sur un langage de programmation donné. Ceux-ci
contiennent généralement les différentes constructions syntaxiques du
langage ainsi qu'une introduction à ses bibliothèques d'instructions les
plus importantes. Les exemples de programmes se limitent à des
illustrations pertinentes d'une construction syntaxique ou d'un élément
de la bibliothèque. Il est rare de trouver dans de tels ouvrages des
exemples de programmes plus complexes qui pourraient servir de modèles
pour la lecture de programmes. La deuxième catégorie d'ouvrages
pédagogiques traite des algorithmes et des structures de données. Les
exemples de programme sont expliqués en pseudo-code ou dans un langage
choisi. Même ici, les exemples sont limités à un aspect très spécifique
et sont rarement assez réalistes pour pouvoir être lus pendant un cours.
Certains livres tels que *Programming Pearls* de Jon Bentley [Ben99]
constituent cependant des exceptions et illustrent explicitement comment
résoudre un problème avec des méthodes très différentes.

<img alt="Fig. 21.2" src="Images/21.2_figure.jpg" width="450px"/>


**Fig. 21.2.** Mise en page « Mozart » de www.csszengarden.com


Les projets dits « Open Source » représentent une autre possibilité
d'étude des codes de programme. Les parties de ceux-ci adaptées à
l'enseignement sont principalement celles qui sont compréhensibles plus
ou moins isolément, c'est-à-dire qui peuvent être lues sans qu'il soit
nécessaire de procéder à une étude approfondie de l'architecture
générale d'un système. L'éventail s'étend des petits utilitaires aux
grands systèmes et la fondation Apache (www.apache.org) propose ici un
choix particulièrement intéressant de projets Open Source connus. Le
projet GNU Classpath (www.gnu.org/software/classpath) s'est fixé pour
objectif de proposer une implémentation Open Source de Java. Un aspect
intéressant, par exemple, est la façon dont le projet a implémenté les
structures de données classiques dans le package java.util.


Nous considérons qu'un exemple concret est l'implémentation d'une
méthode de calcul de la valeur absolue de nombres complexes.
Mathématiquement, la valeur absolue d'un nombre complexe *z* ayant une
partie réelle *x* et une partie imaginaire *y* est définie comme suit :

<img alt="Fig. 21.3" src="Images/21.3_figure.jpg" width="250px" height="60px"/>

Dans la majorité des classes Java disponibles sur Internet, la méthode
est implémentée directement d'après cette définition :


    public double abs() {  
    return Math.sqrt(x*x + y*y);  
    }


L'implémentation par les Apache Commons est cependant toute différente :
le code source de *org.apache.commons.math.complex.Complex* contient une
implémentation considérablement plus complexe pour le calcul de la
valeur absolue (ici abrégée pour la vérification de cas particuliers) :



    public double abs() {  
    if (Math.abs(x)<Math.abs(y)) {  
    if (y == 0.0) {  
    return 0.0;  
    }
    double q = x / y;
    return (Math.abs(y) * Math.sqrt(1 + q*q));
    }
    else {
    if (x == 0.0) {
    return 0.0;
    }
    double q = y / x;
    return (Math.abs(x) * Math.sqrt(1 + q*q));
    }
    }


En convertissant cette implémentation en une expression mathématique, le
calcul se lit comme suit :


<img alt="Fig. 21.4" src="Images/21.4_figure.jpg" width="450px" height="180px"/>


Cet exemple illustre les connaissances fondamentales qui peuvent être
acquises par l'étude d'un code écrit par d'autres. L'implémentation
facile à comprendre avec *Math.sqrt(x*x + y*y)* est vouée à l'échec
pour une grande partie de la plage des valeurs positives des données de
type *x* et *y*. L'élévation au carré provoque un dépassement de
capacité pour <img alt="Fig. 21.5" src="Images/21.5_figure.png" width="150px" height="20px"/>. 
L'implémentation plus complexe contourne ce problème à
l'aide du quotient *q* qui est toujours inférieur ou égal à 1. La
solution plus complexe n'est pas non plus parfaite. Il est vrai que
l'algorithme évite un dépassement de capacité, mais il n'est pas très
efficace pas toujours numériquement stable.


L'exemple ci-dessus montre que la tâche en apparence insignifiante
« calcul de la valeur absolue des nombres complexes » mène à des aspects
passionnants et fondamentaux du numérique tels que l'efficacité, la
stabilité numérique et le dépassement de capacité.


Il existe différents concours de programmation qui fournissent eux aussi
des idées, par exemple l'ACM Programming Contest (www.acm.org/contest),
les Olympiades internationales d'informatique (www.ioinformatics.org) ou
encore le Concours de Programmation Multi-langage en France
http://www.topdevone.com/index_fr.php). Les solutions n'étant
malheureusement publiées que dans de rares cas, il existe bien des
énoncés de problèmes, mais pas de codes de programme.
