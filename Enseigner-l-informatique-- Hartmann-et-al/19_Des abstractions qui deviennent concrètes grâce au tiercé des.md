**19**


**Des abstractions qui deviennent concrètes grâce au tiercé des
représentations**


C'est la fin d'une journée de cours, O. et K. prennent un pot : « j'ai
beaucoup de mal à faire comprendre le concept de pointeur à mes
étudiants. Ce terme est tellement abstrait, mais indispensable dans la
programmation en langage C. » « Chez moi, les problèmes sont apparus
beaucoup plus tôt », répond K. « Aujourd'hui, j'ai essayé d'expliquer
quel effet la copie d'une formule dans une cellule de feuille de calcul
peut avoir sur les autres cellules. »

 

**Problème :** l'abstraction joue un rôle important en informatique.
Nous ne pouvons pas « toucher » les bits et les octets, et tout ce que
nous voyons des systèmes d'exploitation et des programmes, ce sont leurs
interfaces à l'écran. De nombreux thèmes abordés dans l'enseignement de
l'informatique sont ainsi difficiles à comprendre pour les étudiants.

 

Quelle est la différence entre POP et IMAP dans les programmes de
messagerie électronique ? Comment pouvons-nous clairement nous
représenter l'IMAP ? Qu'est-ce qu'une feuille de style dans un programme
de traitement de texte et où se trouve-t-elle ? Quelles sont les
différences entre les divers protocoles de réseau ? Qu'est-ce qu'un
système de chiffrement à clé publique, un sémaphore ou le modèle
Abstract Factory en conception orientée objet ? De nombreux concepts
informatiques sont très difficiles à illustrer. Dans l'enseignement, la
présentation de thèmes aussi abstraits représente ainsi un véritable
défi.

 

Le processus d'abstraction, au cours duquel le monde réel est réduit à
un modèle mettant en valeur les éléments essentiels, est d'une
importance capitale pour l'informatique. Une fois compris, un modèle
abstrait détaché de la réalité concrète s'avère être un outil très
utile.

 

Il existe un deuxième mode d'abstraction qui est lui aussi souvent
appliqué en informatique : certains aspects d'un fait sont
volontairement ignorés ou, en d'autres termes, encapsulés. Le
programmeur n'a ni le besoin ni la nécessité de se préoccuper des
détails d'une bibliothèque de programmes. À un niveau supérieur
d'abstraction, il est inutile de connaître les niveaux inférieurs ainsi
que leur fonctionnement. Le système de fichiers, par exemple, simplifie
l'accès aux fichiers, mais peu de gens connaissent la technique employée
pour accéder à un CD-ROM.

 

L'abstraction est une méthode utile, mais elle est exigeante et demande
beaucoup d'expérience. Les enfants apprennent au moyen d'objets concrets
et doivent pouvoir imaginer les événements et les séquences
d'opérations. Plus nous prenons de l'âge, plus nous devenons réceptifs
aux explications présentées visuellement ou simplement par écrit.
Toutefois, nous apprécions très souvent un bon exemple de notre vie
quotidienne. En informatique aussi, de nombreuses situations peuvent
s'expliquer à l'aide de simples analogies au quotidien.

 

Jerome Bruner a défini une classification des différentes formes de
représentation qui convient très bien à l'enseignement  [Bru88]. En
s'inspirant de Piaget, il distingue trois niveaux de représentation :

 

**représentation énactive** Perception des faits par ses propres
actions. Ce niveau de représentation est particulièrement prononcé chez
les enfants. Ces derniers apprennent par leurs propres actions, en
touchant des objets, par l'observation. Les enfants n'ont pas besoin de
mode d'emploi pour conduire un tricycle.

 

**représentation iconique** À ce niveau, les faits sont représentés par
des images. Un homme est aussi capable de se représenter des objets
concrets, des événements et des processus à l'aide de visualisations. La
brochure d'un hôtel ou un plan de ville suffisent souvent pour se faire
une idée d'un lieu.

 

**représentation symbolique** Perception des faits par des symboles
(texte, dessin, etc.). Lorsque nous entendons le mot « arbre », nous
pouvons sans difficulté nous imaginer de quoi il s'agit. Nous n'avons
pas besoin de grimper dans un arbre ni d'en voir une photo. Les
représentations symboliques ont le grand avantage d'être précises et
concises, et sont particulièrement adaptées lorsque l'on possède déjà
une idée intuitive pertinente d'un sujet.

 

Tout le monde possède la capacité de se déplacer avec plus ou moins de
souplesse entre les différents niveaux de représentation. D'après
Bruner, cette capacité de déplacement est essentielle dans
l'enseignement : les opérations mentales devraient toujours se refléter
si possible sur plusieurs niveaux.

 

La forme de représentation énactive convient particulièrement pour
introduire un thème. La substance devient plus accessible aux étudiants,
qui la mémorisent mieux. Nous distinguons deux autres modes de
représentation énactive, en plus de la représentation énactive pure (où
chaque élève devient lui-même actif avec des objets physiques) :

 

**semi-énactive** L'enseignant effectue une démonstration énactive, les
élèves ne font qu'observer. Comme l'enseignement de l'informatique
s'adresse rarement à de jeunes enfants, il n'est pas obligatoire que
tous les élèves aient eux-mêmes une activité énactive.

 

**énactive virtuelle** Les opérations énactives sont simulées dans un
environnement assisté par ordinateur par des manipulations sur des
objets. Des exemples bien connus sont les environnements d'apprentissage
dans lesquels les étudiants peuvent contrôler un robot virtuel à
l'écran.

 

Les représentations énactives, ou analogies, ne sont pas quelque chose
que l'on peut sortir spontanément de sa manche pendant un cours. Il faut
une idée brillante, dont on peut s'inspirer au quotidien ou par des
exemples issus des publications spécialisées et d'Internet. Des
représentations énactives peuvent également être recherchées en commun
avec les élèves dans le cadre d'une métaréflexion à la fin d'un sujet.

 

Il ne faut pas non plus sous-estimer le travail de préparation des
représentations énactives, celui-ci doit être optimisé. Les
représentations énactives peuvent être divisées approximativement en
trois classes en fonction de l'effort requis :

 

**présent partout** Certains éléments se retrouvent dans presque chaque
salle de classe, notamment les élèves eux-mêmes. Ces derniers peuvent
facilement être placés, déplacés et, avec un peu d'imagination, ils
peuvent même être copiés. On peut utiliser à cet effet les chaises, les
tables, les vestes, les casquettes, les crayons, les stylos, etc.

 

**facile à réaliser** Ce qui veut dire : (1) les matériels nécessaires
sont disponibles rapidement et à moindre coût. (2) Ils peuvent être
approvisionnés en nombre suffisant de sorte que pratiquement tous les
étudiants peuvent connaître leur propre expérience énactive. (3) Les
matériels peuvent être apportés en classe sans difficulté particulière.
Les matériels qui répondent à ces critères sont le papier, le carton, la
ficelle, le ruban adhésif, les trombones, les élastiques, les bouteilles
en plastique, les rouleaux de papier toilette (pleins ou vides), les
boîtes à œufs, le papier d'aluminium, etc.

 

**fabrication compliquée** Un effort important ne se justifie que
lorsque le résultat peut être utilisé de manière répétitive dans
l'enseignement. Il doit exister un rapport favorable entre les travaux
préparatoires et le rendement, à savoir l'amélioration de la réussite de
l'apprentissage. Des exemples de représentations plus complexes sont les
labyrinthes en bois destinés à représenter physiquement les procédures
de retour sur trace, les petits ordinateurs en carton qui présentent le
fonctionnement d'un processeur ou encore les boîtes en carton de tailles
et de couleurs différentes pour illustrer les systèmes de fichiers.

 

De plus amples informations sur le tiercé des représentations sont
disponibles dans la littérature standard sur la pédagogie en général,
par exemple [Aeb01], ou en rapport avec les sciences naturelles [FL93].

 

**Solution :** l'enseignement et la pensée ne doivent pas se dérouler de
manière formelle et abstraite dans des textes, symboles et formules. De
nombreux thèmes peuvent très facilement être visualisés par des images
ou compris par une manipulation active.


**Exemple 1 : un modèle en bois pour le traitement de texte**

La formation à une application, par exemple un traitement de texte,
obéit très souvent au même schéma. Celui-ci commence par une brève
explication de la fonction : à quoi servent les tabulations ? Combien de
sortes existe t-il ? Comment les utiliser ? De nombreux stagiaires ne
réussissent même pas à organiser correctement cette quantité
d'informations. La formatrice effectue ensuite une démonstration devant
les stagiaires curieux qui observent l'écran de projection. Elle regarde
son ordinateur portable, les stagiaires et la formatrice s'observent en
fait mutuellement. La formatrice se sert de la souris pour montrer
comment sélectionner et placer les taquets de tabulation. L'interface
utilisateur est cependant beaucoup trop petite pour pouvoir suivre les
explications sur l'écran de projection, le curseur de la souris est en
retard sur les explications et un clic de souris n'est pas visible.


Peut-être serait-il plus efficace de se contenter de présenter le
principe fondamental des tabulations puis de laisser ensuite les élèves
découvrir par eux-mêmes les différentes formes. La Figure 19.1
représente « Word en bois », un modèle énactif de traitement de texte
réalisé à partir d'objets tels que des lattes en bois, des tringles à
rideau et des pinces à linge.

La formatrice choisit un taquet de tabulation en haut à gauche et le
place à la main sur la règle. Tout est transparent, suffisamment grand
et directement visible. Les taquets de tabulation peuvent être déplacés,
la corde de délimitation suit le mouvement. Le modèle en bois peut bien
évidemment servir à illustrer d'autres concepts et d'autres opérations.
Il peut en outre être étendu à un environnement approprié pour des
démonstrations semi-énactives sur le traitement de texte.


**Exemple 2 : l'algorithme du trajet le plus court**

La recherche du trajet le plus court entre l'origine et la destination
est une tâche essentielle des planificateurs d'itinéraire. Sur le plan
abstrait, il s'agit de trouver dans un graphe dirigé et pondéré le
chemin le plus court entre un nœud de départ *S* et un nœud destinataire
*Z*. La méthode de Dijkstra permet d'accomplir efficacement cette tâche
[Dij59]. Celle-ci s'inspire de l'idée selon laquelle le plus court
chemin entre le nœud de départ *S* et le nœud destinataire *Z* est
déterminé en calculant tous les trajets possibles et en choisissant le
plus court. Les propriétés suivantes sont ici prises en compte : si le
chemin le plus court de *S* à *Z* passe par le nœud *P*, les trajets
partiels de *S* à *P* et de *P* à *Z* sont alors eux aussi les chemins
les plus courts. La méthode de Dijkstra appartient à la catégorie des
algorithmes dits gloutons (greedy algorithm), avec lesquels un point est
gagné à chaque étape.

 
<img alt="Fig. 19.1" src="Images/19.1_figure.jpg" width="450px"/>


**Fig. 19.1.** Représentation énactive d'un traitement de texte (Idée de
Paul Miotti)

Cette description n'éclaire pas beaucoup mieux qu'une description
symbolique sous la forme d'un code de programme ou d'un pseudo-code tel
que le suivant (extrait de  [Sed92]) :

 

procedure matrixpfs;  
var k,min,t: integer;  
begin  
for k:=1 to V do begin  
val[k]:=-unseen; dad[k]:=0  
end;  
val[0]:=-(unseen+1);  
min:=1;  
repeat  
k:=min; val[k]:=-val[k]; min:=0;  
if val[k]=unseen then val[k]:=0;  
for t:=1 to V do  
if val[t] <0 then begin  
if (a[k,t] <>0) and (val[t]<-priority) then begin  
val[t]:=-(priority); dad[t]:=k  
end;  
if val[t]>val[min] then min:=t;  
end  
until min=0;  
end;

 

La description de l'algorithme en pseudo-code a en outre pour
inconvénient majeur qu'il faut commencer par une introduction à la
notation formelle. Les étudiants doivent d'abord comprendre la notation
avant de pouvoir se concentrer sur l'idée fondamentale dont s'inspire
l'algorithme.

 

Une description du déroulement sous la forme d'une séquence d'images
comme dans la Figure 19.2, accompagnée d'un texte descriptif, serait
nettement plus explicite qu'un programme en pseudo-code. La séquence
d'images illustre la représentation iconique statique d'un processus
dynamique. Il serait également possible d'animer le déroulement de cette
séquence d'images, une variante à première vue convaincante et facile à
réaliser avec les possibilités informatiques actuelles. Une
représentation statique est néanmoins souvent préférable à une
animation, une visualisation dynamique pouvant imposer un effort
cognitif supplémentaire de la part des étudiants. En plus de la
présentation visuelle du fait, il faut également interpréter le
déroulement dans le temps et en extraire des informations pertinentes et
cohérentes. Pour de plus amples informations à ce sujet particulièrement
intéressant pour l'enseignement de l'informatique, nous renvoyons à
[Low04].

Lorsque l'explication fait appel à une animation, il est important que
les étudiants puissent en maîtriser parfaitement le déroulement dans le
temps. Il est même préférable de pouvoir disposer de différentes
représentations qui peuvent être modifiées par l'utilisateur. La
Figure 19.3 représente une animation simplifiée contrôlée par
l'utilisateur de la méthode de Dijkstra [Mül].

Les étudiants peuvent choisir le nombre de nœuds puis suivre chacune des
étapes du processus. Cette forme de présentation donne aussi une idée
d'une représentation véritablement énactive : un réseau routier est
dessiné à la craie dans la cour de l'école. Les élèves se tiennent sur
le point de départ et chacun marche jusqu'à un nœud directement voisin
du nœud de départ. Chaque élève compte ses pas et note ce nombre à la
craie dans le nœud de destination. Ils poursuivent ensuite leur trajet
dans toutes les directions possibles en totalisant continuellement les
pas franchis et en veillant à ne parcourir chaque trajet qu'une seule
fois. Lorsqu'un élève arrive sur un nœud qui contient déjà un nombre de
pas inférieur au sien, il hérite de ce nombre plus petit. Ce procédé
suppose bien évidemment la présence d'un nombre suffisant d'élèves et un
graphe de taille raisonnable.

Cependant, la méthode peut également être mise en œuvre sur une table
avec des personnages virtuels : une ficelle sert alors à mesurer les
distances et le réseau de chemins est tracé à l'aide d'un ruban adhésif.

L'exemple de la représentation énactive d'un algorithme de plus court
chemin dans un graphe révèle un aspect que beaucoup de représentations
énactives ont en commun : l'enseignant n'annonce en aucun cas la
solution à l'avance, mais emporte les étudiants dans un bref voyage de
découverte.

**Exemple 3 : espaces colorimétriques et profondeurs de couleur dans le
traitement d'image**

Les espaces colorimétriques et les profondeurs de couleur jouent un rôle
essentiel en imagerie. L'utilisation d'un grand nombre de couleurs
augmente la qualité des images, mais produit également des fichiers
image volumineux. La taille des fichiers peut être réduite en utilisant
moins de couleurs, mais au détriment de la qualité des images.

<img alt="Fig. 19.2" src="Images/19.2_figure.jpg" width="350px"/>

**Début**  
**Étape 1**  
**Étape 2**  
**Étape 3**

**Fig. 19.2.** Séquence d'images de l'algorithme de plus court chemin


<img alt="Fig. 19.3" src="Images/19.3_figure.jpg" width="350px"/> 

**Fig. 19.3.** Animation de l'algorithme de plus court chemin

 
Le compromis entre une bonne qualité d'image et la taille du fichier
peut être illustré comme suit : les élèves reçoivent chacun la même
image et une feuille de papier à petits carreaux. Un groupe reçoit une
boîte avec un grand nombre de crayons de couleur et un deuxième groupe
une petite boîte ne contenant qu'une douzaine de crayons de couleur. Un
troisième groupe reçoit exactement trois crayons de couleur. Il s'agit à
présent de dessiner l'image sous forme matricielle, pixel après pixel,
sur la feuille de papier à petits carreaux. Les avantages et les
inconvénients des différentes profondeurs de couleur apparaissent très
vite. De plus, si un quatrième groupe se voit offrir la possibilité de
choisir dans la grande boîte les douze crayons de couleur dont il a
besoin pour dessiner l'image, il vit alors une expérience énactive du
principe de la palette de couleurs (Figure 19.4).

L'utilisation de papier quadrillé avec des carreaux de différentes
tailles permet en outre de démontrer l'influence de la résolution sur la
qualité et la taille de l'image.

 

<img alt="Fig. 19.4" src="Images/19.4_figure.jpg" width="350px"/>


**Fig. 19.4.** Palettes de couleurs énactives (Idée : Beat Döbeli
Honegger)

**Exemple 4 : le polymorphisme dans la programmation orientée objet**

Le polymorphisme est un terme abstrait dans la programmation orientée
objet. Des explications de ce concept sont disponibles dans le matériel
pédagogique, par exemple sur la base de différents types de véhicules :
une classe *Voiture de sport* est dérivée d'une classe fondamentale
*Véhicules motorisés*. La classe *Voiture de sport* ne possède pas de
méthode *accélération* propre, mais hérite de la méthode de la classe
fondamentale. Un problème se pose avec une famille spéciale de voitures
de sport : les *Voitures fusées* qui possèdent les mêmes
caractéristiques que les autres *Voitures de sport*, mais où la méthode
*accélération* se base sur un principe de fonctionnement différent. Deux
méthodes *accélération* sont donc nécessaires. Dans un langage de
programmation orienté objet, la méthode associée à la classe d'un objet
est invoquée au moment de l'exécution. Ce comportement est appelé le
polymorphisme. Le polymorphisme désigne la superposition de noms de
méthode. Il existe dans une hiérarchie de classes différentes formes de
mise en œuvre d'une méthode, qui ne portent pas toujours le même nom.
L'objet se comporte différemment en fonction de la classe à laquelle il
est associé au moment de l'exécution : il est polymorphe.

Cette description textuelle se déplace immanquablement sur le niveau de
la représentation symbolique, même si l'on essaie d'y établir une
référence familière à des objets concrets tels que des voitures. Comment
alors trouver une analogie compréhensible énactive et plausible avec la
vie quotidienne ? Pour commencer, il est indispensable de bien
comprendre l'idée fondamentale du polymorphisme. Joseph Bergin le
présente comme suit  [Ber02] :

 

you are thinking about the services that your program must provide. You
discover that one service might be provided in more than one way.
Different clients may require different versions, but generally a given
client will always require the same version.

 

[How can we provide several versions of the same service cleanly and
efficiently? If you provide several different methods to provide the
versions of this service, you may need to write if statements
(selection) to choose between them. This is a poor solution as these if
statements tend to proliferate through the program, making modifications
difficult and error prone. ] [*. . .*]

 

[Therefore, consider having distinct objects provide the different
versions of the service. ] [*. . .*]

*(Vous réfléchissez aux services que votre programme doit pouvoir
fournir et vous découvrez qu 'un service pourrait être fourni de
plusieurs manières. Des clients différents peuvent exiger des versions
différentes, mais un client donné exigera généralement toujours la même
version. Comment pouvons-nous offrir proprement et efficacement
plusieurs versions d 'un même service ? Si vous proposez plusieurs
méthodes différentes pour fournir les versions de ce service, vous serez
peut-être contraint d 'écrire des instructions* ***if*** *(de sélection)
pour que ce choix soit possible. Cette solution est inadaptée, car ces
instructions* ***if*** *ont tendance à proliférer à travers le
programme, ce qui rend les modifications difficiles et sujettes aux
erreurs.  [ ... ]Par conséquent, envisagez de prévoir des objets
distincts pour fournir les différentes versions du service.  [ ... ])*

Vous essayez à présent d'appliquer ce que vous avez lu aux élèves et aux
activités quotidiennes et demandez à tout le monde de se lever ! Vous
donnez comme instruction : « vous devez chacun fermer votre chaussure
gauche ». Tout le monde comprend l'ordre, mais son application présente
de nombreuses différences : certains ont des fermetures Velcro, d'autres
des fermetures éclair et il existe en outre différentes techniques pour
nouer des lacets. Tout le monde a ainsi compris ce qu'est le
polymorphisme, certains même en riant.
