**13**


**Pédagogie expérientielle**


T. estime que son enseignement doit être le plus proche possible de la
réalité et organise ainsi fréquemment des exercices pratiques. Les
futurs techniciens de l'assistance informatique montent et démontent
eux-mêmes les disques durs ou les puces de mémoire, mesurent le débit
des données dans le réseau et configurent des routeurs et des pare-feux.
Ces exercices donnent cependant souvent lieu à des situations de
stress : certains participants les terminent rapidement, alors que
d'autres luttent contre des problèmes inattendus. Toute tentative visant
à synchroniser les travaux pratiques est vouée à l'échec. Dès, que les
étudiants sont absorbés dans leurs tâches, ils ne prennent plus guère
note des explications données par la suite par T. Au cours des dernières
heures, un étudiant a même eu une réaction agacée et a exigé de pouvoir
accomplir ses tâches lui-même et dans le calme.


**Problème :** les exercices pratiques sur ordinateur font partie
intégrante de l'enseignement de l'informatique. Des rythmes de travail
différents et des problèmes inattendus rendent la conception des
exercices difficile. De plus, les stagiaires n'apprécient pas vraiment
d'être dérangés par de nouvelles explications pendant qu'ils sont
concentrés sur leur tâche.


Dans l'enseignement de l'informatique, une méthode pédagogique qui se
concentre sur les étudiants et les élèves s'avère très efficace.
L'utilisation efficace et rationnelle de la messagerie électronique ou
du traitement de texte au quotidien dans le monde professionnel, la
création de sites Web ou encore la programmation ne peuvent pas
s'apprendre uniquement en écoutant les présentations des enseignants.
Des exercices pratiques sont indispensables. Ceux-ci doivent être
personnalisés et ne doivent pas simplement consister à accomplir
systématiquement les tâches décrites dans un manuel ou à saisir un code
de programme. Les exercices doivent en outre être motivants pour les
étudiants, la méthode appropriée ici est celle de la pédagogie
expérientielle.


La pédagogie expérientielle personnalise les exercices pratiques et les
divise en petites séquences qui peuvent être traitées indépendamment par
les étudiants. Ces dernières sont en outre conçues de telle sorte que
quasiment tous les étudiants parviennent à un résultat positif. En
pédagogie expérientielle, l'explication de la matière par l'enseignant
se limite aux bases nécessaires pour que les étudiants puissent ensuite
l'approfondir et en acquérir des connaissances supplémentaires par
eux-mêmes. Les expériences menées en cours contiennent des éléments
nouveaux, stimulant ainsi la motivation et le sens des responsabilités
des étudiants. Ce n'est plus l'enseignant qui occupe le centre du cours
 : il dispose ainsi de plus de temps pour des questions individuelles.
La pédagogie expérientielle a en outre un effet secondaire positif dans
le sens où elle permet d'accroître la confiance en soi et favorise
l'autonomie. En effet, il est souvent nécessaire en informatique de
pouvoir se familiariser avec de nouveaux sujets de manière autonome, par
exemple à l'aide d'un tutoriel. La pédagogie expérientielle développe
cette capacité.


La check-list ci-après (qui s'inspire de [FFE04]) résume les
principaux points à prendre en compte lors de la création d'expériences
pédagogiques :


**quelque chose de nouveau** Les expériences à vocation pédagogique ne
sont pas de simples exercices ou tâches répétitives. Les élèves doivent
apprendre quelque chose de nouveau et cette nouveauté doit faire l'effet
d'une petite « découverte ».


**une formulation écrite des tâches** Les expériences pédagogiques
doivent être préparées par écrit. Les instructions doivent être claires
et concises et contenir toutes les informations nécessaires pour
accomplir la tâche.


**la procédure à suivre** Les tâches, l'objectif et, le cas échéant, les
instructions menant vers la solution sont prédéfinies. La pédagogie
expérientielle ne consiste pas à apprendre à découvrir.


**La durée** Les expériences pédagogiques durent généralement de 10 à
30 minutes et s'intègrent parfaitement dans des cours de 45 à
60 minutes. Dans l'idéal, une expérience pédagogique contient une tâche
supplémentaire pour les plus rapides.


**Une structure de réponse formelle** Les étudiants doivent savoir ce
qui doit ressortir de l'expérience pédagogique. Plus la formulation de
la structure de réponse est claire, plus l'efficacité pédagogique de
l'expérience sera élevée.


**Degré de difficulté** L'expérience pédagogique doit pouvoir être menée
à bien en parfaite autonomie par la majorité des étudiants. Les
expériences trop difficiles n'apportent rien aux élèves qui perdent
alors leur confiance en soi.


**Solution :** la personnalisation est un aspect très important dans
l'enseignement de l'informatique, en particulier dans la conception des
exercices pratiques. Les expériences pédagogiques permettent de
personnaliser de courtes séquences de 10 à 30 minutes : le professeur
enseigne de manière traditionnelle pendant la première moitié du cours,
puis présente par écrit une expérience au cours de laquelle les élèves
apprendront quelque chose de nouveau.


**Exemple 1 : indexation de documents texte par les moteurs de
recherche**


Parmi les douzaines d'abrégés de cours sur la collecte d'informations
sur l'Internet, les deux expériences pédagogiques \[HNS00\] ci-après ont
fait leurs preuves. La première expérience concerne le classement des
documents en faisant exclusivement appel au papier, ce qui veut dire
qu'elle se déroule sans ordinateur. Les stagiaires reçoivent une fiche
de travail sur laquelle figure une requête ainsi qu'un document en
rapport avec cette demande et un autre non pertinent. En se basant sur
cette situation initiale, les stagiaires doivent imaginer une règle
selon laquelle le système de recherche pourra identifier le document
plus pertinent en tant que tel. La Figure 13.1 représente un extrait de
l'expérience pédagogique pour une règle de classement.  

----

**Information requise :** vous recherchez des informations relatives à
un accident aérien à Lockerbie.  
Vous présentez la requête ci-dessous au système de recherche et obtenez
un document pertinent (à gauche) et un document moins pertinent (à
droite).  
**Requête :**  
avion accident Lockerbie  
**Document le plus pertinent**  
Un avion s'écrase à Lockerbie.  
Les autorités annoncent une catastrophe.  
**Document moins pertinent**  
Un avion s'écrase à Phnom Penh, Cambodge.  
Il semble que l'accident soit lié à une erreur humaine.  

Donnez une règle qui permet ici de différencier un document plus
pertinent d'un autre qui l'est moins.  
N'oubliez surtout pas qu'un système de recherche ne doit avoir recours
qu'à des informations statistiques et qu'il ne suit pas le même
cheminement de pensée cognitive qu'un être humain. 

----

**Fig. 13.1.** Expérience pédagogique de classement de documents texte


La deuxième expérience pédagogique traite de l'indexation des documents
texte. Celle-ci est menée par les stagiaires sur un ordinateur avec un
véritable système de recherche. Ces derniers doivent découvrir à cette
occasion le fonctionnement de l'indexation avec un système de recherche
de leur choix : y a-t-il distinction entre les majuscules et les
minuscules ? Comment sont traités les accents ? Les caractères
génériques sont-ils pris en charge ? Le système de recherche
décompose-t-il les mots ? Ces connaissances aident les stagiaires à
affiner leurs recherches et à analyser plus précisément les résultats.
Ils pourront ensuite retrouver ces propriétés par eux-mêmes sur de
nouveaux moteurs de recherche et ne sont pas obligés d'apprendre par
cœur les informations sur les différents systèmes.



| **Système de recherche OmniSearch** | |
| ----| ----|
| Majuscules/minuscules | ignorées <p>*Bern, bern, bERn* trouvent toujours les mêmes documents </p> |
| Accents | Les accents ne font pas l'objet d'un traitement particulier <p>*- *Bâle* ne trouvera pas les mêmes documents que *bale*<p>|
| Caractères génériques | ils ne sont pas pris en charge <p>- *qua** ne trouve aucun document </p> |
| Décomposition des mots | non <p>- *microondes* renverra moins de résultats que *micro ondes* </p> |

----

**Système de recherche : ___________**  
Majuscules/minuscules  
Accents  
Caractères génériques  
Décomposition des mots 

----


**Fig. 13.2.** Expérience pédagogique d'indexation de documents texte


La structure de réponse formelle de cette expérience pédagogique est
prédéfinie à l'aide d'un tableau. Le premier tableau contient déjà un
exemple fictif, le deuxième devra être complété par les étudiants au
cours de l'expérience. La Figure 13.2 représente l'expérience
pédagogique.


**Exemple 2 : compression des données**


Les expériences pédagogiques peuvent idéalement être combinées avec des
applets interactifs qui permettent aux étudiants de mener leurs
expériences. Dans un cours sur la compression des données, par exemple,
l'introduction par l'enseignant est suivie d'un examen approfondi de
l'algorithme de Huffman. Les étudiants sont chargés de mener plusieurs
expériences liées les unes aux autres pour lesquelles ils doivent
trouver des réponses à l'aide des applets associés. La Figure 13.3
représente la première expérience pédagogique, la Figure 13.4 l'applet
associé \[Swi\].


**Expérience pédagogique 1 a)**

En préparant notre valise, nous pouvons exercer une pression pour en
faire sortir l'air et ainsi y faire entrer plus de vêtements. Nous avons
entendu parler du code de Huffman pendant le cours et savons qu'il
fonctionne selon le même principe.

**Expérience :**  
comment doit se présenter le texte saisi pour qu'il puisse être
facilement comprimé ?

**Procédure :**
-   Pour mener cette expérience, utilisez l'applet dont vous trouverez
    les instructions abrégées sur la feuille jointe.
-   Comprimez plusieurs mots pour arriver à la solution. Exemple :
    pouvoir, disproportionné, fenêtre, souris, AAAAAABBCCED,
    ABCDEFGHIJKL
-   Formulez votre réponse en trois phrases complètes au plus.

**Fig. 13.3.** Expérience pédagogique sur l'algorithme de Huffman


<img alt="Fig. 13.1. Expérience pédagogique de classement de documents texte" src="Images/13_figure.jpg" width="450px"/>


**Fig. 13.4.** Applet sur l'algorithme de Huffman
