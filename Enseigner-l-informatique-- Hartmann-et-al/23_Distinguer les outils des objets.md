**23**


**Distinguer les outils des objets**


Les élèves réalisent des exercices pratiques sur le thème des images
numériques : ils scannent des images, effectuent des prises de vue avec
leur appareil photo numérique et copient/collent des images depuis le
Web. Et voilà que S. commence à fustiger verbalement Photoshop au motif
que le programme refuse soudainement de sélectionner librement les
couleurs. M. est agacée par les grandes taches qui apparaissent dans le
ciel de sa photo de plage, il n'y a décidément rien à tirer du format
d'image JPEG.


**Problème :** lors des exercices pratiques dans l'enseignement de
l'informatique, en plus des concepts abstraits de la théorie, les élèves
sont également confrontés à des applications concrètes sous la forme
d'outils et d'objets. Il n'est cependant pas toujours facile
d'identifier l'origine des problèmes qui surviennent : est-ce dû à
l'outil utilisé ? Les objets traités sont-ils en cause ? L'outil utilisé
est-il inadapté aux objets traités ?


Les travaux réalisés en informatique consistent bien souvent à manipuler
des objets de différentes natures, par exemple des photos, et ces
travaux sont réalisés à l'aide d'outils, par exemple des logiciels de
traitement d'image.


Les deux niveaux -- objet et outil -- se caractérisent par des concepts
fondamentaux. Une image peut être codée sous la forme d'une matrice de
points ou à l'aide de vecteurs, elle peut être compressée avec ou sans
perte de qualité. Un logiciel de traitement d'image fait appel à des
concepts tels que les calques, les masques ou les filtres.


Il existe également une mise en œuvre concrète sur les deux niveaux :
les objets de type images matricielles sont concrétisés sous la forme de
fichiers GIF et JPEG, par exemple, et les outils se présentent sous la
forme de divers produits qui appliquent différemment les concepts
fondamentaux. Le tableau ci-après illustre les concepts et leur mise en
œuvre au niveau des objets et outils en prenant pour exemple le
traitement d'image.


|  | **Concepts**  | **Mise en œuvre** |
| -----------|-----------|-----------|
| **Outils** | Traitement d’images avec <p> calques, masques, etc. </p> | Produits tels que <p> Photoshop, CorelDraw, etc.</p> |
| **Objets** | Graphiques matriciels ou vectoriels, compression <p>avec/sans perte d’informations, etc. </p>| EMF ou EPS, GIF ou JPEG, <p>etc.</p> |


La distinction entre les objets et les outils est difficile pour les
élèves, mais elle est importante et les aide à catégoriser de manière
appropriée les problèmes rencontrés. Le problème a-t-il pour origine une
compression excessive de l'image, un format d'image incorrect ou une
fonctionnalité insuffisante de l'outil ? Les questions posées dans le
tableau ci-dessous aident à faire la distinction entre les objets et les
outils d'une part et les concepts et leur mise en œuvre de l'autre.


|  | **Concepts**  | **Mise en œuvre** |
| -----------|-----------|-----------|
| **Outils** | Quels sont les aspects indépendants du produit dans les <p> outils servant au traitement des objets ? </p><p>Tâches et opérations types, etc. ? </p> | Comment les concepts d’outil sont-ils<p> mis en œuvre dans un produit concret ?</p> |
| **Objets** | Quels sont les aspects indépendants du produit dans <p>les objets traités ?</p><p> Propriétés et catégories types, etc. ? </p>| Comment les concepts d’objet sont-ils </p><p>mis en œuvre dans les types d’objet concrets ?</p> |


L'observation de la clôture d'un jardin permet de constater que la
division entre les concepts et leur mise en œuvre dans les produits
ainsi que la distribution des objets et des outils ne sont pas
spécifiques à l'informatique. Nous allons prendre un exemple dans un
thème proche du traitement d'image, à savoir les dimensions de la
formation au métier de peintre. La ligne directrice de la réglementation
suisse relative à la formation des peintres est la suivante :


La tâche du peintre consiste à appliquer des peintures, enduits et
autres et matériaux structurels ainsi qu'à poser des papiers peints,
revêtements et tissus. Il embellit ainsi les bâtiments, les équipements
et les objets et les protège contre les intempéries et autres
influences.


Une liste est ensuite dressée avec les concepts relatifs aux objets
(matériels) et les outils qui sont importants pour la profession de
peintre, ainsi que les connaissances et compétences nécessaires pour les
travaux pratiques. Le tableau ci-après est un exemple.


|  | **Concepts**  | **Mise en œuvre** |
| -----------|-----------|-----------|
| **Outils** | Connaître les différentes opérations ainsi que les matériaux et les outils utilisés à cet effet lors des travaux préliminaires sur le bois et ses produits dérivés, les supports minéraux, les métaux, les matières plastiques, les anciennes peintures et enduits, les tissus et étoffes, etc. | Préparation du support comme le ponçage, l’enlèvement de la rouille, l’isolation, la neutralisation, le blanchissement, etc. <p>Travaux de lasure et d’imprégnation sur des supports minéraux et le bois ainsi que la teinture et le vernissage.</p> |
| **Objets** | Principes fondamentaux de la théorie des couleurs, mélange de couleurs, influence de la brillance et de la structure du support sur la teinte. | Connaître les décapants, produits de lessivage et neutralisants courants, agents de blanchiment et détergents, pigments, liants, solvants et diluants, additifs, teintes et vernis. |


**Concepts Mise en œuvre**


**Solution :** la distinction entre les niveaux de l'objet et de l'outil
mais aussi entre les concepts et leur mise en œuvre permet aux étudiants
de structurer plus facilement un thème. Les enseignants en informatique
doivent toujours amener les étudiants à prendre conscience de cette
structure.


**Exemple 1 : formation appliquée à la messagerie électronique**


Les messages électroniques, ou courriels, sont omniprésents et la
majorité des utilisateurs sait à peu près s'en servir. Ce sont
généralement les premiers problèmes qui dévoilent leur compréhension
réelle des concepts fondamentaux de la messagerie électronique :
pourquoi ne parviens-je pas à envoyer cet e-mail ? Est-ce à cause des
paramètres SMTP incorrects dans mon client de messagerie ? Le serveur
SMTP est-il momentanément indisponible ? Ai-je saisi le bon mot de
passe ? Et même si l'utilisation des courriels et des programmes de
messagerie semble presque triviale, il existe tout de même quelques
concepts à la fois au niveau de l'objet et au niveau de l'outil. Le
tableau ci-après indique quelques exemples.



|  | **Concepts**  | **Mise en œuvre** |
| -----------|-----------|-----------|
| **Outils** |  – Bureau avec stylos et crayons, papier, papier carbone, boîte aux lettres interne pour les courriers entrants et sortants, corbeille à papier  <p>– Boîte aux lettres et réseau de distribution de la poste </p> | – Clients de messagerie comme Outlook ou Thunderbird, ou encore messagerie Web par le biais du navigateur  <p>– Compte de messagerie avec accès par Web, POP ou IMAP</p><p>– Serveur SMTP chez le fournisseur de services ou serveur de messagerie Web</p><p>– Internet (TCP/IP, DNS, …)</p> |
| **Objets** |   – Concept de « lettre » avec sa structure standard, l’en-tête de lettre et la signature  <p>– Carnet d’adresses et listes de destinataires</p> |   – Courriel avec expéditeur, destinataire, objet, contenu et pièces jointes ; adresse sous la forme utilisateur@domaine  <p>– Adresses et listes d’adresses </p> |

**Exemple 2 : systèmes d'exploitation**


Tous les utilisateurs d'ordinateur sont confrontés aux systèmes
d'exploitation, les techniciens d'assistance IT souvent avec plusieurs
systèmes d'exploitation et des versions différentes. Tous les systèmes
d'exploitation se basent sur des concepts communs au niveau de l'objet
et offrent des fonctions d'outil similaires. La mise en œuvre de ces
objets et de ces outils dépend du système d'exploitation spécifique. Le
tableau ci-après indique quelques exemples pour Windows.




|  | **Concepts**  | **Mise en œuvre** |
| -----------|-----------|-----------|
| **Outils** | – Gestion des fichiers <p>– Gestion des processus</p><p>– Gestion des périphériques</p>| – Explorateur<p>- Gestionnaire de tâches</p> <p>– Panneau de configuration</p>|
| **Objets** | – Répertoires, fichiers <p>– Processus</p> <p>– Matériel interne et périphériques</p>| – FAT 32, NTFS<p>– Applications, processus du système d’exploitation et services</p><p>– Carte graphique, moniteur, réseau</p>|


**Exemple 3 : algorithmes et structures de données**


La confrontation aux concepts et à leur mise en œuvre est
quasi-obligatoire dès que l'on aborde le thème des algorithmes et des
structures de données. Nous allons ici évoquer le Backtracking, ou
retour sur trace, qui est un sujet d'étude à associer au niveau de
l'objet. La récursivité représente ici un moyen élégant de décrire un
algorithme d'usage général qui pourra ensuite être utilisé pour
différentes applications. Deux exemples classiques du Backtracking sont
la recherche du chemin pour sortir d'un labyrinthe et la recherche d'une
solution à un problème au jeu de dames.


|  | **Concepts**  | **Mise en œuvre** |
| -----------|-----------|-----------|
| **Outils** | Développement de logiciels avec projets, débogueur, etc. | Produits comme Eclipse, Visual Studio, IntelliJ IDEA, etc. |
| **Objets** | Algorithme général pour le Backtracking | Mise en œuvre pour résoudre un problème particulier dans un langage de programmation |


Le concept de Backtracking peut être traité en décrivant l'algorithme
général en pseudo-code. Il devient alors évident que la notion de
Backtracking est indépendante d'une part de la demande, comme le
problème du labyrinthe, et d'autre part d'un langage de programmation
spécifique. La mise en œuvre du concept pour une application choisie
sera réalisée dans un langage de programmation réel tel que C# ou Java.


Le niveau outil du thème des algorithmes et des structures de données
présente des concepts tels que les compilateurs et les débogueurs et
s'étend jusqu'aux mises en œuvre sous la forme d'environnements de
développement intégrés. Il existe ici une multitude d'outils concrets,
suivant le langage de programmation choisi, depuis le simple éditeur de
texte jusqu'aux environnements avec gestion de projet et de version
intégrée en passant par les éditeurs de texte disposant de
fonctionnalités additionnelles. Selon l'outil utilisé, l'enseignement
devra prévoir plus ou moins de temps pour l'initiation à ses concepts et
à son utilisation.
