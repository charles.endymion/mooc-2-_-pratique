**25**


**Travail sur ordinateur : les mains dans le dos !**


Le cours sera aujourd'hui consacré à des exercices pratiques sur la
programmation et M., l'enseignante, a tout parfaitement organisé :
différents algorithmes de tri ont été étudiés au cours des quatre cours
précédents, avec un examen approfondi d'Insertion Sort et de Quicksort
et les premières réflexions sur la mise en œuvre. Il s'agit à présent
pour chaque stagiaire de mettre en œuvre lui-même une procédure et de la
tester. Le disque dur central contient déjà une structure de programme
permettant de générer une séquence de nombres aléatoires et la méthode
d'affichage à l'écran de la séquence de nombres triée est elle aussi
prédéfinie. Les stagiaires n'ont plus qu'à se concentrer sur
l'algorithme.


Cependant, les premiers problèmes apparaissent au bout de cinq minutes à
peine : monsieur I. ne peut pas éditer son programme, madame V. ne peut
pas accéder au code du programme, monsieur D. demande s'il peut
sélectionner un Pivot aléatoire avec Quicksort, le programme de madame
P. fonctionnait bien au début mais est resté bloqué avec une exception
de pointeur nul, monsieur Z. n'arrive pas à trouver une erreur de
syntaxe dans son programme et le programme de monsieur L. fonctionne
avec dix chiffres ou moins, mais plus avec onze.


**Problème :** les pièges sont très nombreux lors d'un travail pratique
sur ordinateur : les problèmes d'infrastructure, les erreurs de
manipulation, les bogues logiciels ou des connaissances insuffisantes
sont autant de complications qui peuvent entraver le bon déroulement
d'un cours voire le bloquer complètement.


Pour l'enseignant, la pratique sur ordinateur constitue un véritable
défi. Il est constamment confronté à des questions et des problèmes à
différents niveaux et doit être capable de faire face rapidement à des
situations nouvelles. Comme d'autres attendent aussi son aide, la
tentation est grande de procéder comme une hotline, à savoir se
contenter de résoudre le problème plutôt que d'indiquer des stratégies
de résolution. De plus, ce sont généralement les étudiants les plus
insistants qui reçoivent le plus d'aide alors que les plus réservés
risquent d'être ignorés.


Suite à de mauvaises expériences lors des cours sur ordinateur, de
nombreux enseignants essaient de planifier les exercices pratiques dans
les moindres détails. Le résultat est que les étapes d'apprentissage
sont de plus en plus courtes et que la formation devient en réalité
l'énoncé d'une suite d'instructions. Pendant un cours sur le traitement
de texte, par exemple, un enseignant qui réalise lui-même l'intégration
délicate des adresses à partir de la base de données sur l'ordinateur de
l'étudiant ne communiquera aucun enseignement durable. Il est vrai que
le stagiaire sera content à la fin du cours, mais il sera totalement
incapable d'accomplir lui-même une tâche similaire sur son poste de
travail quelques temps plus tard, alors qu'il devrait justement acquérir
les connaissances nécessaires pour pouvoir classifier et résoudre en
toute autonomie les problèmes rencontrés dans son quotidien
professionnel. Le plus important dans l'enseignement de l'informatique
est donc l'aide à l'auto-assistance. Par conséquent, la règle
fondamentale que doit appliquer tout enseignant pendant les exercices
pratiques sur ordinateur : garder les mains dans le dos !


La mise en application au quotidien de cette règle élémentaire n'est
toutefois pas si simple : grande est la tentation, par quelques clics de
souris, d'insérer la ligne manquante dans le programme qui reste planté.
L'enseignant doit lui-même s'exercer à respecter cette règle. Il est en
outre important de bien faire comprendre l'objectif de la règle aux
étudiants et c'est en gardant les mains dans le dos que l'enseignant
devient en réalité un conseiller qui pose des questions telles que : «la
condition d'interruption de votre boucle est-elle correcte ? »,
« pouvez-vous isoler la source de l'erreur en éliminant toutes les
parties non importantes du programme ? » ou encore « vous aviez déjà
rencontré des problèmes d'impression. Vous souvenez-vous des causes ? ».
L'enseignant essaie de stimuler la métaréflexion des étudiants, qui
doivent réfléchir aux causes du problème et trouver des solutions.


Outre le principe des « mains dans le dos », il existe d'autres méthodes
pour communiquer aux stagiaires des stratégies d'aide à
l'auto-assistance et pour soulager l'enseignant du stress subi pendant
les exercices sur ordinateur :


**file d'attente d'assistance** L'enseignant ne répond à aucune
question. Toutes les questions doivent être inscrites sur un chevalet à
feuilles mobiles ou au tableau dans une file d'attente d'assistance avec
le nom de celui qui pose la question et une description détaillée du
problème. Nous avons vu pour la première fois la mise en application
d'une telle file d'attente chez Johann Penon à Berlin.


La file d'attente d'assistance présente plusieurs avantages : chacun des
stagiaires prend plus de temps pour essayer de résoudre le problème tout
d'abord par lui-même ou en collaboration avec d'autres stagiaires. La
description du problème oblige en outre à une nouvelle réflexion, car
personne ne viendra inscrire « chez moi ça ne marche pas » dans la liste
des questions ! L'enseignant peut traiter chaque demande l'une après
l'autre, personne n'est favorisé ou délaissé. Lorsque des problèmes et
des questions similaires surviennent, les personnes concernées peuvent
directement s'adresser à ceux qui ont déjà trouvé la solution. De plus,
à la fin des exercices, le formateur peut faire une synthèse des
questions les plus courantes à l'aide de la liste et en reparler.


**discussion en direct ou chat** Une méthode peu conventionnelle mais
efficace d'assistance mutuelle. L'élève décrit son problème et espère
que l'un de ses camarades pourra l'aider. Par de nombreux aspects, la
discussion en direct correspond à la file d'attente d'assistance, mais
l'enseignant n'y joue qu'un rôle secondaire. La discussion en direct est
plus anonyme et sera parfois utilisée plus assidûment. Le protocole de
discussion peut en outre servir de base à de futurs débats.


**réunions régulières sur les erreurs et FAQ** Il est important
d'aborder les erreurs en toute connaissance de cause : les erreurs et
les problèmes les plus courants sont compilés et débattus au cours de
réunions régulières consacrées aux erreurs, par exemple après chaque
demi-journée de cours ou une fois par mois à l'école. Quelle est la
catégorie de l'erreur ? S'agit-il d'un simple oubli ou d'une difficulté
logique fondamentale qui pourra aussi se retrouver dans un autre
contexte ? Les erreurs les plus fréquentes peuvent en outre être
consignées sous la forme d'une FAQ (Foire Aux Questions). Et une FAQ
publiée sous la forme d'une grande affiche sur le mur de la salle de
classe suscitera assurément plus d'attention qu'une belle page sur
l'intranet de l'école.


**Solution :** une règle fondamentale que doit appliquer tout enseignant
pendant les exercices pratiques sur ordinateur est de garder les mains
dans le dos ! En suivant cette règle, il devient un conseiller qui
apporte son aide dans la recherche des solutions et stimule le travail
autonome des étudiants. Cette règle peut être complétée par des files
d'attente d'assistance, des réunions sur les erreurs et des FAQ.
