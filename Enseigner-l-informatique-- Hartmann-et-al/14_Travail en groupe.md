**14**


**Travail en groupe**


Décidément le temps manque : comment voulez-vous enseigner l'utilisation
efficace d'un logiciel de présentation en seulement dix cours ? Quelles
sont les fonctions à aborder en détail ? Le concept de modèle de
diapositive ou les animations définies par l'utilisateur ? Quelques
stagiaires s'intéressent plus particulièrement à l'intégration de
fichiers audio dans différents formats tandis que deux autres ne
possèdent quasiment aucune connaissance préalable et n'utilisent ni
traitement de texte ni logiciel de présentation dans leur travail
quotidien. En résumé : un logiciel de présentation possède une multitude
de fonctions ,le spectre des connaissances préalables des stagiaires est
donc large.


**Problème :** la plupart des produits contiennent une foule de détails
qui ne peuvent être traités que partiellement dans un cours. Le niveau
de connaissance préalable des étudiants sur le produit est très
disparate ;il est par conséquent difficile d'intégrer ce facteur dans
l'organisation de la formation.


Dans de nombreux thèmes abordés lors de l'enseignement de
l'informatique, il convient de communiquer à la fois des principes
conceptionnels et des compétences spécifiques au produit, afin que les
étudiants puissent ensuite utiliser un outil avec compétence et
efficacité. Pour des raisons de temps, il est impossible de traiter
toutes les fonctions d'un logiciel dans un cours et il y a peu d'intérêt
à communiquer à l'avance des connaissances spécifiques à un produit. De
nombreux stagiaires n'auront jamais besoin d'une partie de ces
fonctions, ou alors -être seulement à un moment où ils en auront oublié
l'essentiel. Il est alors fort probable qu'ils utilisent le produit
concerné dans une version plus récente où les fonctions s'utilisent
différemment. Il convient par conséquent d'appliquer les règles
empiriques ci-après pour les formations relatives à des outils
concrets :


-   après la formation, les stagiaires doivent se sentir suffisamment en
    confiance pour pouvoir, le cas échéant, acquérir les connaissances
    nécessaires en utilisant des sources d'information appropriées.
    Etant donnée la courte durée de vie des produits, il est
    particulièrement important de promouvoir l'aptitude à
    l'apprentissage continu.


-   une formation sur les produits doit également aborder les concepts
    fondamentaux, seul moyen de pouvoir transférer les connaissances
    acquises à des outils similaires.


-   si l'on veut exploiter les possibilités d'un outil, un aperçu de ses
    fonctionnalités est indispensable. Cet aperçu peut se limiter aux
    fonctions caractéristiques.


En renonçant à vouloir traiter l'intégralité d'un logiciel dans un
cours, il devient possible de mieux prendre en considération les
différentes connaissances préalables des étudiants. Les plus avancés
traiteront des tâches difficiles et des exercices plus simples
permettront aux débutants de réussir eux aussi leurs manipulations.


La méthode pédagogique du travail en groupe permet de personnaliser
l'enseignement et de promouvoir les compétences méthodologiques de
l'apprentissage autonome. David Johnson et Roger Johnson soulignent cinq
caractéristiques de la pédagogie de groupe qui sont également
importantes pour l'enseignement de l'informatique \[JJ75\] :


1. Le travail en groupe encourage la cohésion entre les étudiants. Soit
on coule ensemble, soit on nage ensemble. Il est très utile d'apprendre
à « nager » ensemble : un individu échoue et capitule plus rapidement
qu'un groupe.


2. La collaboration au sein d'un groupe a un effet motivant et accélère
ainsi le processus d'apprentissage.


3. Dans un groupe, chaque membre doit assumer sa part de responsabilité
et contribuer à la réalisation des objectifs.


4. Le travail en groupe favorise les compétences sociales telles que la
communication au sein d'une équipe, la confiance, la prise de décision
dans un groupe de projet et la gestion des conflits.


5. Le travail en groupe encourage la réflexion sur les processus de la
dynamique de groupe et contribue ainsi à une efficacité accrue dans la
réalisation de projets informatiques.


Une bonne pédagogie de groupe va bien au-delà de la simple invitation
par l'enseignant à traiter un sujet dans des groupes. En l'absence de
mesures appropriées, c'est souvent le membre le plus fort du groupe qui
en prend la direction et accomplit lui-même le travail ; il n'est donc
pas rare qu'un travail en groupe engendre des tensions sociales. La
réussite de l'enseignement varie elle aussi : les membres actifs du
groupe en bénéficient le plus. Une bonne pédagogie de groupe doit donc
veiller à ce que tous les étudiants apportent leur contribution.


Il existe de nombreuses formes de pédagogie de groupe. Elles se
distinguent par la taille des groupes et l'activité au sein du groupe.
Les deux formes suivantes conviennent tout particulièrement à
l'informatique :

**méthode du puzzle** Des groupes de 3 à 6 élèves sont constitués et
chaque groupe reçoit une tâche à accomplir ainsi que le matériel
nécessaire, puis il doit se familiariser avec la tâche pendant la séance
des experts. Les élèves élaborent eux-mêmes une partie de la substance.
Les groupes sont ensuite réorganisés et les experts transmettent leur
savoir aux autres pendant la séance éducative (Fig. 14.1).


**Séance des experts Séance éducative**


<img alt="Fig. 14.1. Division en une séance des experts et une séance éducative" src="Images/14.1_figure.jpg" width="450px"/>

**Fig. 14.1.** Division en une séance des experts et une séance éducative

Grâce à la méthode du puzzle, les étudiants traitent de manière autonome
un sujet maîtrisable et en deviennent des experts. C'est justement cette
aptitude qui sera exigée plus tard, lorsqu'ils devront manipuler des
ordinateurs. De plus, la transmission à d'autres des connaissances
acquises renforce l'estime de soi. Un autre aspect à ne pas négliger est
que les étudiants expliquent les faits dans leur propre jargon, plus
accessible aux autres étudiants que les explications parfois abstraites
de l'enseignant.

Le plus important dans l'application de la méthode dite du puzzle est
que tous les élèves aient le sentiment d'avoir réussi. Il est donc
recommandé de confier les sujets les plus simples aux élèves les moins
doués, notamment des sujets pouvant être divisés en plusieurs parties de
taille à peu près égale.


Un inconvénient de la méthode du puzzle est que tout le monde ne traite
pas l'ensemble de la substance aussi bien. Les experts d'un sujet
comprendront mieux celui-ci et le traiteront d'autant plus efficacement.
Peut-être que l'une ou l'autre erreur se glissera dans la séance
éducative. Par conséquent, la méthode du puzzle convient pour des thèmes
incluant une multitude d'aspects individuels qui ne doivent pas être
traités dans leur intégralité. La méthode du puzzle est moins
recommandée en guise d'introduction aux concepts fondamentaux d'un
thème.


**travail en partenariat** Le travail en partenariat est la plus petite
forme de travail en groupe : chaque groupe ne compte ici que deux
personnes. Les travaux durent rarement plus d'une heure, la durée
typique d'un travail en partenariat étant de 10 à 20 minutes.


Dans un travail en partenariat, les groupes se voient confier de petites
tâches bien précises. Celles-ci ne doivent pas être trop difficiles et
se limitent généralement à l'explication mutuelle ou à une discussion
factuelle.


Les travaux en partenariat sont adaptés aux problèmes où interviennent
deux rôles. Dans un algorithme de tri, l'un des partenaires peut ainsi
fournir des instructions en fonction d'un programme préétabli, alors que
l'autre exécute l'algorithme, par exemple au moyen de cartes à jouer. Là
où les choses deviennent intéressantes, c'est lorsque les deux
partenaires sont assis dos à dos : ils ont des opinions différentes et
peuvent discuter de ce que pourrait être l'idée centrale de
l'algorithme. Les différences d'opinion interviennent dans de nombreux
thèmes de l'informatique. Les travaux en partenariat tels qu'ils sont
indiqués contribuent à la prise de conscience de ce fait par les
étudiants. Les thèmes les plus proches du travail en partenariat
incluent le cryptage et le décryptage, le chiffrement et la
crypto-analyse, les programmes de contrôle et le point de vue du robot
dans une commande de robot ou encore les architectures client-serveur.


**Solution :** les travaux en partenariat et la méthode du puzzle sont
des formes de travail en groupe particulièrement bien adaptées à
l'enseignement de l'informatique. Des travaux en partenariat de courte
durée peuvent montrer les différentes opinions sur un sujet. La méthode
du puzzle est recommandée pour les thèmes aux aspects très détaillés  ;
elle prépare les élèves à leur adaptation autonome à de nouveaux sujets
dans leur future vie professionnelle.


**Exemple 1 : le puzzle pour la procédure de tri**


Il existe plusieurs procédures de tri, chacune adaptée à un problème
différent, mais le temps manque dans l'enseignement de l'informatique
pour traiter en détails tous les algorithmes de tri importants et les
mettre en œuvre dans un programme. Le traitement en groupe des
différentes procédures selon la méthode du puzzle semble justement
s'imposer ici.


Nous allons présenter ci-après un exemple concis de l'aspect que
pourraient prendre les documents relatifs à une procédure de tri pour un
groupe d'experts. Le puzzle a été compilé par Rolf Grun. Les textes
s'adressent à des élèves.


**Introduction**


Dans notre vie quotidienne, nous voulons toujours que les informations
soient triées : annuaires téléphoniques, emplois du temps, classements,
carnets d'adresses, pour ne citer que quelques exemples. Le tri de ces
données s'effectue principalement à l'aide d'ordinateurs, ce qui est
parfaitement justifié car le tri, bien que simple, reste un travail de
longue haleine. Il existe différents modes de tri et, au cours des
deux prochaines heures, vous allez découvrir quatre méthodes de tri
différentes. Vous apprendrez à connaître l'une d'entre elles et
l'enseignerez ensuite à vos camarades de classe. Ceux-ci, à leur tour,
vous présenteront les trois autres. A l'issue des deux heures, vous
aurez compris les quatre méthodes et serez capables de les programmer.


**Instructions de travail**


Vous pouvez apercevoir sur la fiche de travail une série de cartes à
jouer qui seront triées d'après une méthode donnée. Au début, les cartes
sont disposées au hasard et à la fin elles sont triées dans un ordre
croissant. Si vous ne connaissez pas les cartes, regardez la dernière
ligne. Elles y sont triées d'après leur valeur : la plus petite tout à
gauche et la plus grande au bout à droite. Essayez de trouver la règle
de tri des cartes. Appliquez ensuite cette règle aux cartes que vous
avez reçues.

Après avoir trouvé ces règles, appliquez-les pour résoudre les problèmes
suivants :


1. triez la série de chiffres 8, 3, 1, 7, 2 d'après vos règles et
écrivez la nouvelle série après chaque étape.


2. utilisez vos propres mots pour formuler l'opération de tri. Pendant
la séance éducative, votre formulation servira à expliquer la méthode à
vos camarades.


Conseil : procédez de bas en haut et marquez ce qui est déjà trié. À
mesure que vous progressez de haut en bas, la partie triée devient de
plus en plus importante.


Il existe pour chaque méthode de tri une fiche de réponse qui doit
permettre aux experts, au cours de leur séance, de vérifier s'ils ont
tout bien compris. Les solutions présentées doivent être concises et ne
pas anticiper une contribution de la part des élèves.


L'enseignement d'après la méthode du puzzle ne doit pas s'encombrer de
trop de papiers. La possibilité d'explorer soi-même les faits à l'aide
de cartes à jouer pour ensuite transmettre les connaissances acquises à
ses camarades de classe rend ce cours intéressant. Chaque groupe
d'experts pourra ensuite mettre en œuvre la méthode de tri apprise sous
la forme d'un programme et les experts pourront alors à leur tour
expliquer leur code de programme à leurs camarades de classe en
appliquant la méthode du puzzle.

<img alt="Fig. 14.2. Fiche de travail pour le tri par insertion (Insertion Sort)" src="Images/14.2_figure.jpg" width="450px"/>
 
**Fig. 14.2.** Fiche de travail pour le tri par insertion (Insertion
Sort)


**Exemple 2 : le puzzle pour le paramétrage d'un navigateur**


Différents paramètres tels que la mémoire cache, le serveur proxy,
l'historique, les cookies, etc. doivent être abordés au cours d'une
formation sur les applications Internet. Si le formateur présente
lui-même les détails, les stagiaires risquent de ne pas en retenir
beaucoup. La méthode du puzzle convient parfaitement à la présentation
des paramètres d'un navigateur, car il n'est pas indispensable que tous
les stagiaires aient la même compréhension de chacun des détails.


Une forme ouverte de la méthode du puzzle est même possible avec ce
thème : le formateur ne remet pas le matériel complet à tous les groupes
d'experts, mais se contente de donner des indications sur la
documentation appropriée. Chaque groupe d'experts doit recueillir
lui-même des informations de fond sur son sujet et déterminer dans
quelle partie du navigateur doivent intervenir les paramètres
correspondants. Cette forme ouverte d'un puzzle communique aux
stagiaires les compétences méthodologiques nécessaires pour pouvoir
procéder en toute autonomie à des clarifications similaires avec
d'autres programmes d'application.

**Exemple 3 : travail en partenariat sur des données et des formules
dans une feuille de calcul**


Les travaux en partenariat contribuent à une grande diversité dans
l'enseignement des tableurs : la stagiaire A. reçoit une feuille de
calcul contenant certains chiffres, c'est-à-dire la vue des microdonnées
de la feuille. Le stagiaire B. reçoit la même feuille de calcul, mais
avec des formules de calcul, c'est-à-dire la vue des macrodonnées de la
feuille. A. et B. ne voient que leurs propres informations (Fig. 14.3).
B. donne maintenant des instructions de calcul à A. conformément aux
indications dans la feuille de calcul, puis A. effectue les calculs et
reporte les résultats sur la feuille.


Un tel travail en partenariat permet notamment d'établir une bonne
compréhension des références de cellule absolues et relatives. Les
références de cellule sont un sujet difficile pour beaucoup d'élèves. B.
doit formuler les règles de calcul en langage courant. Exemple :
« ajoute 1 à la valeur de la cellule B1 et multiplie le résultat par le
contenu de la cellule C5. Additionne ensuite le contenu de la cellule B3
au résultat, puis note ce que tu obtiens dans la colonne C, dans la
cellule après le chiffre précédent. Répète la même opération avec ce
nouveau chiffre. »



<img alt="Fig. 14.3. Les deux vues lors des calculs dans une feuille de calcul" src="Images/14.3_figure.jpg" width="450px"/>

**Fig. 14.3.** Les deux vues lors des calculs dans une feuille de calcul


Le travail en partenariat permet, en procédant de manière analogue, de
donner un aperçu de la différence entre les indications absolues et
relatives d'un emplacement dans les programmes de dessin, du filtrage ou
du tri des données dans un tableur ou encore des requêtes de base de
données simples.

**Exemple 4 : travail en partenariat sur un robot programmable**


Des robots réels ou virtuels sont souvent utilisés pour illustrer les
cours de programmation. L'interaction du programme et du robot peut être
simulée par un travail en partenariat : deux élèves sont assis dos à
dos, l'un joue le rôle du robot, l'autre celui du programme. Le
programme doit commander le robot pour qu'il accomplisse une tâche
prédéfinie. L'important ici est que le programme ne puisse pas voir le
monde du robot. Ce qu'il veut connaître de ce monde, il doit l'apprendre
à partir des valeurs qui lui sont communiquées par les capteurs du
robot. Ce dernier exécute des instructions. Les deux élèves
réfléchissent continuellement : celui qui joue le rôle du programme
cherche à savoir quel plan est suivi par le robot et l'élève qui est le
robot essaie de comprendre comment pourrait être structuré le programme.
Une fois l'exercice terminé, les deux partenaires peuvent échanger leurs
rôles.

<img alt="Fig. 14.4. Travail en partenariat pour la commande d'un robot" src="Images/14.4_figure.jpg" width="450px"/>


**Fig. 14.4.** Travail en partenariat pour la commande d'un robot


