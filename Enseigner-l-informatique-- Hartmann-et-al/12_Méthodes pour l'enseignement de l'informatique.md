**12**

**Méthodes pour l'enseignement de l'informatique**


M. est fier de l'enseignement pratique qu'il dispense. Les leçons sont
toutes bâties selon le même modèle éprouvé et immuable : introduction du
sujet (de quoi s'agit-il, pourquoi cette matière est-elle importante,
que faut-il ensuite savoir faire ?), puis illustration des faits par un
exemple concret et enfin réalisation pratique sur ordinateur d'une série
d'exercices soigneusement préparés. Les problèmes éventuellement
rencontrés sont abordés à la fin du cours et des modèles de solution
sont alors indiqués. Beaucoup d'élèves apprécient cette forme
d'enseignement avec des objectifs clairs, mais d'autres s'en plaignent.
Une classe entière, après quelques semaines, s'est même mise à critiquer
les cours qui lui semblaient ennuyeux et monotones.


**Problème :** les exercices sur ordinateur constituent une partie
importante de l'enseignement pour de nombreux thèmes en informatique  ;
les leçons se partagent généralement entre une partie théorique,
articulée autour de l'enseignant, et une partie pratique à la structure
clairement définie. Toutefois, cette organisation de l'enseignement
peut, à la longue, devenir monotone.


Certains élèves apprécient un enseignement bien organisé et encadré par
l'enseignant, alors que d'autres préfèrent découvrir les choses
nouvelles par eux-mêmes en revendiquant un espace de liberté. De même,
certains élèves ont un esprit vif et comprennent rapidement ce qui leur
est présenté, alors que d'autres auront besoin d'étudier un sujet donné
plus longuement. Il existe des modes de pensée différents et donc des
modes d'apprentissage différents. L'assimilation de connaissances par
les femmes est généralement différente à celle des hommes.


Cette liste pourrait être prolongée presque indéfiniment. Ce qu'il est
important de noter ici, c\'est que les différences entre les étudiants,
les matières à enseigner et les caractères des enseignants suggèrent des
méthodes d'enseignement diversifiées. Une méthode d'enseignement unique
ne peut pas répondre à toutes les attentes. La diversité est importante,
chaque enseignant devrait disposer d'un répertoire de méthodes
d'enseignement.


L'appel à la diversité méthodologique n'est pas contesté sur le plan
didactique et pédagogique en général. Nous n'insisterons donc pas ici
sur l'aspect sémantique de la méthode d'enseignement, pas plus que nous
n'indiquerons une liste des méthodes d'enseignement courantes avec leurs
avantages et leurs inconvénients respectifs. Nous renvoyons ici à la
littérature spécialisée, par exemple la présentation générale,
rationnelle et concise par Wiechmann \[Wie02\], l'ouvrage de référence
relatif aux « recettes » de l'enseignement de Grell \[GG00\] ou encore
les observations pratiques de Meyer \[Mey03\].


Le choix judicieux de la méthode d'enseignement revêt une importance
toute particulière dans l'enseignement de l'informatique, car on a
souvent à faire ici à des groupes hétérogènes d'étudiants ou d'élèves,
notamment dans le cas des stages de formation où les connaissances
préalables des participants sont souvent très différentes. Un
enseignement trop centré sur l'enseignant a pour conséquence que
certains élèves sont débordés, tandis que d'autres s'ennuient. Un rythme
d'apprentissage uniforme est quasiment impossible à tenir lors des
travaux pratiques sur ordinateur : très vite apparaîtront des problèmes
au niveau du système ou de l'utilisation qui laisseront certains
étudiants ou stagiaires à la traîne. Une approche personnalisée est donc
recommandée.

Un autre aspect spécifique à l'enseignement de l'informatique est la
multiplicité des détails dans de nombreux domaines : qu'il s'agisse des
systèmes d'exploitation ou des langages de programmation et même des
applications telles que les traitements de texte ou les tableurs, il
existe de nombreux détails spécifiques au produit. En général, il n'est
guère justifié ou tout simplement impossible de traiter tous ces détails
dans un cours. Au contraire, il est nécessaire de communiquer aux élèves
et étudiants les compétences méthodologiques afin qu'ils puissent
acquérir les connaissances nécessaires « juste à temps ». Cette
compétence méthodologique est toutefois difficile à transmettre par un
simple cours magistral, les méthodes d'enseignement doivent ici être
totalement différentes.


Chacune des méthodes d'enseignement courantes est en principe également
adaptée à l'enseignement de l'informatique. Nous allons présenter dans
les chapitres suivants cinq méthodes d'enseignement particulièrement
bien adaptées aux exigences spécifiques de l'enseignement de
l'informatique :


**pédagogie expérientielle** La méthode expérientielle est une méthode
pédagogique adaptée à de courtes séances de formation d'une durée de 20
à 30 minutes. La matière, déjà traitée, est approfondie et élargie au
cours de nouvelles expériences. Dans l'enseignement de l'informatique,
la méthode expérientielle convient à l'organisation d'exercices
individuels sur l'ordinateur.

**programme dirigé** Pour simplifier, le programme dirigé peut être
considéré comme le « grand frère » de la méthode expérientielle. Il
s'agit en fait d'un matériel d'auto-apprentissage pour une durée de 2 à
10 leçons. Les programmes dirigés se basent sur le principe Mastery
Learning ou Pédagogie de la maîtrise : l'étudiant doit parfaitement
comprendre un sujet avant de passer au suivant. Pour les plus rapides,
un programme dirigé contient un additif (complément). Dans
l'enseignement de l'informatique, les programmes dirigés sont adaptés à
des groupes d'apprentissage hétérogènes ou à des thèmes abstraits
particulièrement difficiles.


**travail en groupe** Le travail en groupe est une méthode
d'enseignement très répandue dans le monde entier. La sociabilité occupe
ici une place prépondérante par rapport à la communication et la
compréhension de la substance elle-même. Le travail en groupe est exigé
dans de nombreuses professions de l'informatique et cette méthode
d'enseignement permet d'acquérir les compétences et l'expérience
nécessaires à cet effet. Cependant, le travail en groupe peut également
être utilisé pour des sujets qui ne peuvent être traités que de manière
ponctuelle et non pas par une approche globale. La division d'un sujet
en différents groupes, également appelée mosaïque de groupes, est une
méthode d'enseignement bien adaptée ici.


**apprentissage par la découverte** L'enseignement de l'informatique se
caractérise souvent par la transmission d'une théorie suivie d'exercices
pratiques. Des aspects importants tels que le travail autonome, la
créativité et la réflexion critique sont peu pris en compte ici.
L'apprentissage par la découverte met explicitement l'accent sur ces
aspects.


**pédagogie de projet** Les projets occupent une position dominante dans
le domaine de l'informatique. Il est donc tout naturel d'appliquer la
pédagogie de projet dans l'enseignement de l'informatique. Les étudiants
s'attaquent à un problème ou au développement d'un produit en
collaboration avec un ou plusieurs enseignants. Les problèmes à résoudre
sont ensuite déterminés, un plan de projet est établi et le travail est
finalement réalisé, tout ceci en commun. Les projets conviennent pour
donner un aperçu de la nature des systèmes informatiques complexes.


**Solution :** l'enseignement selon un même modèle séquentiel répétitif
ne tient pas compte des modes de pensée et des modes d'apprentissage
différents et devient rapidement monotone. Chaque enseignant se doit de
posséder un répertoire de méthodes d'enseignement différentes. Les
méthodes particulièrement adaptées à l'enseignement de l'informatique
sont celles qui personnalisent l'apprentissage et qui tiennent également
compte des faits difficiles à assimiler et abstraits ainsi que des
sujets présentant un niveau de détail très élevé.
