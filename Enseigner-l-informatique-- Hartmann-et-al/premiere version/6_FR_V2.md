**6**

\

**L'enseignement de l'informatique englobe la connaissance des concepts
et la connaissance des produits**

\

La chargée de coursprofesseure d'informatique H. prévoit d'organiser à
l'université populaire une session de formation ayant pour thème la
recherche sur l'Internet. La description de cette formation pourrait
être la suivante : « Sachez enfin trouver ce que vous recherchez !
Contenu du cours : utilisation avancée des navigateurs Firefox et
Internet Explorer à l'aide des raccourcis clavier, utilisation des
moteurs de recherche Google, MSN Search et Yahoo, possibilités de
recherche avancée pour trouver des films, moteurs de recherche spéciaux,
bases de données et encyclopédies/dictionnaires sur l'Internet. » Mais
cette description pourrait aussi ressembler à cela : « Décortiquez avec
nous les mécanismes des services d'information et laissez-vous initier
aux principes fondamentaux de la recherche d'informations sur
l'Internet. Sommaire : Cclassification des services d'information,
collecte de documents, modèles d'extraction tels que le modèle de
l'espace vectoriel, extraction booléenne et probabiliste, indexation de
textes, mécanismes d'analyse. »

\

**Problème :** lLa connaissance des concepts et la connaissance des
produits jouent toutes deux un rôle essentiel dans le domaine de
l'informatique. Si les enseignantes et les enseignants se concentrent
sur la communication des connaissances conceptuelles, les étudiants
manqueront de pratique et ne pourront pas mettre leurs connaissances en
application. À l'opposé, si les enseignantes et les enseignants
accordent trop d'importance à la transmission de la connaissance des
produits, l'enseignement ne sera pas pérenne et lespérenne. Les
étudiants ne pourront alors pas adapter et appliquer leurs connaissances
à des situations nouvelles.

\

Il existe différentes manières de classifier les connaissances. Une
classification simple est suffisante dans notre cas : la connaissance
des concepts englobe les relations fondamentales et applicables à long
terme d'un domaine de spécialité et tandis que la connaissance des
produits englobe le savoir nécessaire à l'utilisation d'un produit
concret, par exemple un logiciel ou un composant physique. La
connaissance conceptuelle est plus générale, car elle s'applique à tous
les produits et à toutes les versions de ceux-ci. Ci-après un tableau
comparatif des deux notions :

\

**Connaissance des produits Connaissance des concepts**

liée au produit indépendante du produit

court terme long terme

apprentissage par cœur, restitution comprendre et organiser

faits isolés relations

peu de transfert possible transfert possible

concret abstrait

\

Dans l'enseignement de l'informatique, il est tentant de mettre l'accent
uniquement sur la connaissance des produits : premièrementproduits.
Premièrement, les mutations rapides dans le domaine de l'informatique
ont pour effet que l'attention est avant tout attirée par les nouveaux
produits ; et, deuxièmementDeuxièmement, les enseignants disposent de
peu de temps pour identifier les concepts fondamentaux en toute
sérénité. Cela est d'autant plus vrai pour les domaines dont ils ne sont
pas des spécialistes. Troisièmement, la pression pour pouvoir utiliser
immédiatement les connaissances acquises est élevée ;-- la connaissance
du produit peut être appliquée immédiatement -- et
quatrièmementimmédiatement. Quatrièmement , la connaissance des produits
semble à priori moins contraignante pour les étudiants que la
connaissance des concepts.

\

Mais il est tout aussi tentant de se concentrer uniquement sur la
connaissance des concepts. L'enseignant évite ainsi d'avoir à tenir
continuellement compte des nouveaux développements et il est inutile de
mettre sans cesse les fiches de cours à jour lorsque de nouvelles
versions des produits arrivent sur le marché. Le professeur peut
aisément acquérir les connaissances nécessaires dans les manuels
scolaires et, un aspect non négligeable, il est moins en « concurrence »
avec les étudiants parfois mieux informés sur les derniers produits.

\

::: {#Section1 dir="ltr"}
Un enseignement de l'informatique de bonne qualité inclut à la fois la
connaissance des concepts et la connaissance des produits. Aucun de ces
deux aspects n'est à négliger lors de la formation : la connaissance des
produits encourage les manipulations et permet aux étudiants de mettre
leur savoir en application.  ; Lla connaissance des concepts permet
d'organiser les faits dans un contexte plus large et facilite ainsi
l'apprentissage. Elle permet en outre de transférer à de nouvelles
situations les connaissances acquises antérieurement.

\

**Solution :** lL'utilisation avantageuse des moyens informatiques est
conditionnée à la fois par la connaissance des produits et par la
connaissance des concepts. Un enseignement peut être considéré comme
particulièrement efficace lorsque les enseignants sont systématiquement
en mesure d'établir la relation entre le concept et le produit.

\

**Exemple 1 : Ccopier/Coller**

\

L'opération Copier/Coller est un mécanisme important d'accroissement de
la productivité en informatique qui est rendu possible grâce à une
collaboration étroite entre le système d'exploitation et les
applications. Les aspects suivants font partie de la connaissance du
concept et sont indépendants de l'application utilisée ou du système
d'exploitation :

\

**nNotions fondamentales** L'opération Copier/Coller consister à copier
des objets d'un endroit à un autre. Ces objets peuvent être des segments
de texte, des images ou des graphiques. La fonction de copie est
possible au sein d'une même application et généralement aussi entre des
applications différentes. La variante Couper/Coller déplace les objets.
Les fonctions Copier/Coller et Couper/Coller peuvent généralement être
activées par un raccourci clavier ou une ligne de menu.

\

**cCondition préalable** L'opération Copier/Coller ne peut avoir lieu
qu'après avoir sélectionné l'objet souhaité. Cette sélection est parfois
implicite, par exemple lorsque l'objet concerné est le seul disponible.
Du fait de cette condition, les opérations Copier/Coller sont souvent
aussi accessibles par le biais d'un menu contextuel.

\

**Ppresse-papiers** Il est nécessaire de stocker temporairement l'objet
entre le moment où il est copié ou coupé et le moment de le coller.
Cette tâche revient au système d'exploitation qui fait souvent appel au
Presse-papiers.

\

**tTypes de données** Le comportement de la fonction Copier/Coller dans
les différentes applications n'est pas toujours prévisible. Que se
passe-t-il lorsque vous copiez un segment de texte d'un traitement de
texte et le collez dans un programme de dessin ? Ces deux applications
utilisent des types de données totalement différents. Avec certaines
applications, l'opération Coller ne donne aucun résultat et avec
d'autres, le type de données est adapté automatiquement et
unautomatiquement. Un segment de texte, par exemple, est alors converti
en données graphiques. Mais la question de savoir ce qui est réellement
copié se pose pour chaque type de données. Lorsque vous copiez un texte
à partir d'un navigateur Internet, par exemple, vous pouvez copier soit
uniquement les caractères, soit ou l'ensemble du texte avec toutes les
informations de mise en forme. Au moment de coller également, vous
pouvez parfois choisir entre différentes options.

\

Cette connaissance succincte du concept s'avère fort utile face à un
produit réel, mais la connaissance du produit proprement dit est
indispensable pour le travail au quotidien. Sous Windows, par exemple,
il faut savoir que les fonctions Copier, Couper et Coller sont
accessibles par les raccourcis clavier Ctrl-C, Ctrl-X et Ctrl-V ou, dans
les applications standard, à partir du menu « Édition » ou du menu
contextuel qui s'affiche en cliquant avec le bouton droit de la souris.
Sous certaines variantes du système Unix, il faut savoir que l'insertion
d'un segment de texte sélectionné s'effectue en cliquant sur le bouton
central de la souris.

\

**Exemple 2 : sSystèmes de fichiers**

\

Les systèmes d'exploitation accomplissent de multiples fonctions. Parmi
celles-ci, le système de fichiers permet de disposer d'une
représentation du disque dur et des autres supports de stockage. Un
certain nombre de connaissances conceptuelles est ici nécessaire : les
fichiers et les dossiers, la structure en arborescence des répertoires,
les métadonnées des objets dans le système de fichiers ou les droits
d'accès.

\

![](6_FR_V2_html_dce45ae5c2994c7d.gif){width="736" height="313"}

\

**Fig. 6.1.** Exemple d'un interpréteur de ligne de commande

\

Mais la simple connaissance des concepts n'est d'aucune utilité lorsque
l'on se retrouve devant un système d'exploitation de type Unix avec un
interpréteur de ligne de commande qui ne dispose d'aucun menu graphique
et réagit exclusivement à des saisies au clavier. La connaissance du
produit est indispensable dans cette situation et il faut alors
connaître les principales instructions disponibles. Quelques exemples :
afficher le chemin du répertoire actuel avec pwd, basculer dans un autre
répertoire avec cd, afficher tous les objets du répertoire actuel, y
compris leurs détails, avec ls -al. Et il Il existe des centaines
d'instructions de ce type pour contrôler un système d'exploitation. Il
faut au moins en maîtriser un jeu de base pour pouvoir utiliser
efficacement un interpréteur de ligne de commande.

\

**Exemple 3 : sSyntaxe des adresses Internet**

\

Les utilisateurs savent généralement qu'ils peuvent saisir un segment de
texte tel que
[[http://www.wikipedia.fr](http://www.wikipedia.fr/)]{.underline} dans
un navigateur Internet pour afficher la page Web concernée. Et gGrâce à
la standardisation des navigateurs, la majorité des utilisateurs sait
aussi où il faut saisir ces segments de texte, à savoir dans le champ
tout en haut de la fenêtre. Il s'agit ici d'une connaissance du produit
indispensable à l'utilisation d'un navigateur Internet. Mais une petite
dose de connaissance du concept, notamment la compréhension de la
structure fondamentale d'une adresse Internet :
Protocole://Adresse\_Serveur/Chemin, s'avère ici bien utile dans
différentes situations :

\

-   lLe nom du serveur peut en principe être choisi à volonté. Grâce à
    cette connaissance, les étudiants savent qu'une adresse de serveur
    ne doit pas obligatoirement commencer par www, mais peut aussi se
    présenter sous la forme
    [<http://fr.wikipedia.org/wiki/Navigateur_Web>]{.underline}.

-   Ccertaines pages Web sont parfois introuvables. La connaissance du
    chemin après l'adresse du serveur permet aux étudiants de supprimer
    la totalité ou une partie de celle-ci après un message d'erreur et
    de réessayer, par exemple avec http://www.wikipedia.fr.

-   Et commePuisqu 'une adresse telle que
    ftp://ftp.mozilla.org/pub/mozilla.org/ obéit aux mêmes règles de
    syntaxe, le transfert des connaissances acquises à d'autres
    protocoles de réseau devient possible.
:::
