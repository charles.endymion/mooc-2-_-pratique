**2**

\

[[**Les**
]{style="font-style: normal"}]{style="text-decoration: none"}~~[[**[professeurs]{style="background: #00ffff"}**]{style="text-decoration: none"}]{style="font-style: normal"}~~[[**[
enseignants
]{style="background: #00ffff"}**]{style="font-style: normal"}]{style="text-decoration: none"}[[**d'informatique
enseignent
l'informatique**]{style="font-style: normal"}]{style="text-decoration: none"}

\

[[[La direction d\'une école supérieure de commerce décide de mettre en
place un
]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}[[[[LMS
( Learning Management System ou
]{style="background: #00ffff"}]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}[[[système
de gestion de
l'apprentissage]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}[[[[)]{style="background: #00ffff"}]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}[[[
]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}[[[et
d'adapter plusieurs cours à cet environnement d'apprentissage et de
pédagogie. La direction de l'école confie ce projet à la professeure
d'informatique S. Celle-ci doit évaluer différents produits, recommander
une solution technique, élaborer des scénarios pédagogiques et en
présenter les avantages et les inconvénients. En tant que professeure
d'informatique, elle semble prédestinée pour cette
mission.]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}

\

[[[Un nouveau logiciel de simulation a en outre été acquis pour les
cours de formation à la gestion d'entreprise, lequel doit être utilisé
via l'Internet conjointement avec
]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}~~[[[[l]{style="background: #00ffff"}]{style="font-weight: normal"}]{style="text-decoration: none"}]{style="font-style: normal"}~~[[[[d]{style="background: #00ffff"}]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}[[[es
étudiants d'autres écoles supérieures. L'utilisation
pédagogique-didactique d'une simulation collaborative est une approche
totalement nouvelle pour de nombreux professeurs. Que se passerait-il si
S. n'était pas en mesure d'assurer un
cours ?]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}

\

[[**Problème :**
]{style="font-style: normal"}]{style="text-decoration: none"}[[[les
moyens informatiques sont présents à tous les niveaux de la formation,
d]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}~~[[[[epuis
l]{style="background: #00ffff"}]{style="font-weight: normal"}]{style="text-decoration: none"}]{style="font-style: normal"}~~[[[es
logiciels standard jusqu'aux
]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}[[[[outils
spécialisés en passant par les
]{style="background: #00ffff"}]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}[[[logiciels
d'apprentissage et le
]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}[[[[e-learning
(formation en
ligne)]{style="background: #00ffff"}]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}[[[.
]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}[[[Les
]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}~~[[[[professeurs
]{style="background: #00ffff"}]{style="font-weight: normal"}]{style="text-decoration: none"}]{style="font-style: normal"}~~[[[[enseignantes
et enseignants d'informatique sont souvent consultés à ce propos en tant
qu'experts
]{style="background: #00ffff"}]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}~~[[[[dans
ces domaines, sont souvent interrogés sur
]{style="background: #00ffff"}]{style="font-weight: normal"}]{style="text-decoration: none"}]{style="font-style: normal"}~~[[[des
questions pédagogiques et didactiques. Mais de nombreux enseignants se
sentent dépassés par ces questions,
]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}~~[[[[aussi
bien sur le
]{style="background: #00ffff"}]{style="font-weight: normal"}]{style="text-decoration: none"}]{style="font-style: normal"}~~[[[[tant
]{style="background: #00ffff"}]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}[[[au
plan professionnel qu'au niveau de leur emploi du
temps.]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}

\

[[[L'utilisation de l'ordinateur en tant qu'outil et support
d'apprentissage donne lieu à des questions d'ordre pédagogique et
didactique : sur quels principes doit se baser une bonne présentation ?
Comment utiliser judicieusement les cyberquêtes ou les podcasts dans
l'enseignement de l'histoire ? Quels sont les avantages des systèmes de
calcul formel pour l'enseignement des mathématiques ? Faut-il mettre en
place un
]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}[[[[LMS
(système de gestion de
l'apprentissage)]{style="background: #00ffff"}]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}[[[
]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}[[[au
niveau de l\'ensemble de l'école ? Un peu partout, un avis compétent sur
ces questions est attendu de la part des professeurs d'informatique,
mais cette attente est infondée. Un professeur d'informatique n'est pas
en soi un expert dans l'utilisation appropriée des outils et des
supports de TIC dans l'enseignement, tout comme le professeur d'économie
n'est pas prédestiné à choisir les logiciels pour la gestion de
l']{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}~~[[[[école]{style="background: #00ffff"}]{style="font-weight: normal"}]{style="text-decoration: none"}]{style="font-style: normal"}~~[[[[établissement]{style="background: #00ffff"}]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}[[[.
]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}

\

[[[Si les professeurs d'informatique sont sollicités en tant que
conseillers pédagogiques
]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}[[[[et
didactiques
]{style="background: #00ffff"}]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}[[[pour
l'utilisation des outils de TIC, des logiciels d'enseignement ou
]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}~~[[[[pour]{style="background: #00ffff"}]{style="font-weight: normal"}]{style="text-decoration: none"}]{style="font-style: normal"}~~[[[[de]{style="background: #00ffff"}]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}[[[
l'e-]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}~~[[[[L]{style="background: #00ffff"}]{style="font-weight: normal"}]{style="text-decoration: none"}]{style="font-style: normal"}~~[[[[l]{style="background: #00ffff"}]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}[[[earning,
ils sont alors détournés de leur véritable vocation, à savoir
l\'enseignement de l'informatique. De plus, l'emploi des professeurs
d'informatique en tant que conseillers didactiques et pédagogiques n'est
généralement justifié ni sur le plan économique, ni sur le plan des
compétences. L'évaluation de l'usage approprié d'un outil TIC dans une
spécialité impose d'être familiarisé avec les objectifs, les méthodes et
le contenu de ladite spécialité. Il appartient ainsi au professeur de
chimie, et non au professeur d'informatique, de juger de l'avantage à
utiliser un logiciel donné pour la modélisation
moléculaire.]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}

\

[[[La mise en application d'une stratégie d'e-Learning complète n'est
pas non plus en premier ressort de la compétence de l'informatique. Il
existe en effet un risque élevé
]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}~~[[[[pour
]{style="background: #00ffff"}]{style="font-weight: normal"}]{style="text-decoration: none"}]{style="font-style: normal"}~~[[[que
la stratégie mise en œuvre soit centrée sur la technologie si, dans un
établissement de formation, les tâches et les décisions dans le domaine
de l'e-Learning sont exclusivement confiées aux personnes issues du
monde de l'informatique. Ce ne sera alors pas l'apprentissage qui se
retrouvera au premier plan, mais l'utilisation des outils
TIC.]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}

\

[[**Solution :**
]{style="font-style: normal"}]{style="text-decoration: none"}[[[les
]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}~~[[[[professeurs
enseignantes et
enseignants]{style="background: #00ffff"}]{style="font-weight: normal"}]{style="text-decoration: none"}]{style="font-style: normal"}~~[[[
d'informatique n'ont pas vocation à traiter des questions pédagogiques
et didactiques des outils TIC dans l'enseignement, ni à choisir les
logiciels d'apprentissage et encore moins à élaborer ou mettre en œuvre
une stratégie d'e-Learning. Les
]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}~~[[[[professeurs
enseignantes et enseignants
]{style="background: #00ffff"}]{style="font-weight: normal"}]{style="text-decoration: none"}]{style="font-style: normal"}~~[[[d'informatique
doivent se
]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}~~[[[[cantonner
]{style="background: #00ffff"}]{style="font-weight: normal"}]{style="text-decoration: none"}]{style="font-style: normal"}~~[[[[limiter]{style="background: #00ffff"}]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}[[[
à leur domaine de compétence et ne doivent pas assumer seuls la
responsabilité de ces
questions.]{style="font-weight: normal"}]{style="font-style: normal"}]{style="text-decoration: none"}
