**5**

\

**Le besoin en formation continue des professeurs enseignants
d'informatique est très élevé**

\

R. adore son métier. Il enseigne l'informatique dans un lycée technique
et ses cours vont de l'introduction aux technologies des réseaux jusqu'à
la modélisation des applications en entreprise. Il ne s'ennuie jamais et
sa passion pour l'informatique est stimulée à la fois par l'ampleur des
sujets à enseigner et les défis posés par le renouvellement incessant
des produits. Mais il arrive parfois que R. atteigne ses limites. Il a
ainsi accumulé au cours des derniers mois une bonne vingtaine de
magazines qu'il n'a pas encore trouvé le temps de lire. et De plus
l'initiation aux services du Web pendant son temps libre fait quasiment
partie du domaine de l'impossible. Si seulement il pouvait laisser de
côté tout le bazar de l'école pendant deux semaines et pour assister en
toute sérénité à un cours sur les services Web !

\

**Problème :** pPlus que toute autre spécialité, l'informatique est en
perpétuelle évolution. De nouveaux produits et de nouvelles versions des
produits voient quotidiennement le jour et le cycle des innovations en
informatique est plus court que partout ailleurs. Les offres de
formation continue spécifiquement destinées aux enseignantes et aux
enseignants de l'informatique sont rares, celles qui s'adressent aux
entreprises sont généralement trop onéreuses. De plus, il n'existe
quasiment aucune offre qui propose également la mise en œuvre
pédagogique des nouvelles matières dans l'enseignement. La formation
continue des professeurs d'informatique est une nécessité absolue, mais
malheureusement ardue, laborieuse et coûteuse.

\

La formation continue des professeurs enseignants d'informatique est un
besoin vital à tous les niveaux, mais sa mise en application n'est pas
aussi simple. Les écoles publiques ne font quasiment aucune distinction
entre les besoins en formation continue des différentes matières
enseignées. Les formations continues sont majoritairement proposées par
des professeurs enseignants pour des professeurs enseignants et les
coûts se situent à un niveau nettement inférieur à celui des offres
comparables pour les entreprises.

\

Mais cette configuration ne peut généralement pas être transposée à la
formation continue en informatique. Il est fort probable que l'un des
professeurs enseignants d'anglais de l'établissement ait étudié en
détail le dramaturge Edward Albee et sera soit ainsi en mesure de
proposer une formation sur cet auteur. Mais il n'en est pas de même avec
les nouvelles technologies de l'informatique : il faut ici faire appel à
une expertise extérieure et généralementextérieure, généralement
coûteuse, avec pour difficulté supplémentaire que ces experts ne font
pas partie du monde de l'enseignement et ne savent souvent pas répondre
aux besoins didactiques d'une école.

\

Dans de nombreuses écoles, le problème de la formation continue en
informatique est devenu un véritable cercle vicieux. L\'école
rechignant : comme l'école rechigne à leur payer des sessions de
formation adéquates, les enseignants se perfectionnent par leurs propres
moyens et la qualité laisse souvent à désirer. Le risque de choix erroné
des sujets à étudier est élevé, tout comme celui de l'absence d'échange
d'expérience avec les collègues de la même spécialité. La formation
continue en autarcie a en outre pour effet que le problème de la
formation continue en informatique n'est absolument pas perçu en tant
que tel par les établissements scolaires. Les efforts des professeurs
enseignants d'informatique ne sont quasiment pas reconnus, ceux-ci se
sentent alors surexploités et, après quelques années, deviennent
frustrés et résignés.

\

Pour casser ce cercle vicieux, il est indispensable de définir une
stratégie claire à la foistant du côté de la direction de l'école et que
du côté des enseignants concernés.  : les Les chefs d'établissement
doivent déclarer considérer que la formation continue en informatique
fait comme partie intégrante des tâches de la direction et doivent
élaborer des stratégies de formation à long terme en collaboration avec
les enseignants. Ils doivent en outre bien faire comprendre au corps
enseignant les raisons pour lesquelles l'informatique bénéficie d'un
statut différent en matière de formation continue, notamment d'un budget
supérieur, d'un quota d'heures inférieur pour les professeurs
enseignants d'informatique et de congés de formation plus nombreux.

::: {#Section1 dir="ltr"}
\

Les professeurs enseignants d'informatique doivent préparer eux-mêmes
une stratégie de formation continue. En l'absence d'organisation
précise, il y a risque de se disperser dans les détails ou de manquer
des développements importants. Une stratégie de formation continue
pourrait concrètement inclure les éléments suivants :

\

**Tendances du développement** Un professeur enseignant d'informatique
doit pouvoir suivre et identifier en temps voulu les tendances à long
terme du secteur de l'informatique. L'abonnement à un magazine
spécialisé de renom, par exemple *Communications* *of the ACM*, s'avère
par conséquent rentable. Ce magazine contient en outre souvent des
articles ayant pour thème l'informatique et la formation.

\

**Produits actuels** Un professeur d'informatique doit disposer d'une
vue d'ensemble sur le marché actuel des matériels et des logiciels. Là
aussi, la lecture de magazines tels que *01 Informatique*, par exemple,
peut s'avérer judicieuse.

\

**Aspects didactiques** Il n'existe pas de publications pédagogiques sur
tous les sujets, mais la recherche de produits appropriés se justifie.
Pour l'enseignement secondaire, nous recommandons de consulter les
documents et les articles de l'*Association Enseignement Public &
Informatique* (site Web http://www.epi.asso.fr). Il existe également des
listes d'abonnement et des forums de discussion Internet sur de nombreux
sujets, nous pouvons par exemple citer les liens consacrés à l'éducation
sur le site du SIGCSE de l'ACM (www.sigcse.org/topics).

\

**Sessions de perfectionnement** Contrairement à d'autres spécialités,
les outils utilisés pour l'enseignement de l'informatique sont les mêmes
que ceux pour le travail en entreprise. À titre d'exemple, il n'existe
pas de systèmes d'exploitation spécialement conçus pour les écoles. Par
conséquent, pour assurer la cohésion avec le monde réel, les professeurs
d'informatique doivent également avoir la possibilité d'assister à des
sessions de perfectionnement qui ont pour thèmes les nouvelles
technologies et ou les nouveaux produits et qui s'adressent aux
entreprises. Un contact régulier avec les informaticiens des entreprises
représente une source d'enrichissement notable pour les professeurs
d'informatique.

\

**Perfectionnement en équipe** L'élaboration de nouveaux sujets d'étude
dans le cadre d'ateliers, par petits groupes de 2 à 6 participants, est
une forme de perfectionnement très efficace. Le travail en équipe joue
de toute façon un rôle prépondérant dans le domaine de l'informatique.
et le Le risque de se disperser dans des détails est moindre au sein
d'un groupe d'apprentissage. Il faudrait institutionnaliser ce genre
d'atelier au sein d'une école pour éviter la nécessité de présenter de
nouvelles demandes de mise en disponibilité chaque année.

\

**Solution :** lLe cycle d'innovation rapide du secteur de
l'informatique entraîne pour les professeurs des besoins en formation
continue nettement plus importants que dans les autres spécialités. Les
professeurs d'informatique doivent se tenir informés de manière ciblée
sur lesdes dernières tendances de l'informatique, ldes produits actuels
et dles nouveaux matériels pédagogiques.
:::
