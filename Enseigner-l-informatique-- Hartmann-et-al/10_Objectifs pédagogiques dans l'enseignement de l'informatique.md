**10**


**Objectifs pédagogiques dans l'enseignement de l'informatique**


W., enseignant d'informatique, exprime sa frustration à ses collègues
pendant la pause café : « je voulais montrer aujourd'hui à mes élèves le
rôle des feuilles de style dans la séparation entre le contenu, la
structure et la mise en page. Etant donné qu'ils veulent tous créer des
pages Web, j'ai choisi comme exemple le XML et le CSS. La principale
difficulté pendant les exercices émanait des sélecteurs et personne n'a
finalement compris que les mêmes idées se retrouvent dans les feuilles
de style de Word ! ». Sa collègue, H., ne comprend que trop bien ce
problème : « j'ai moi aussi l'impression que les élèves se dispersent
trop dans les détails pendant les travaux pratiques. Un peu le principe
de l'arbre qui cache la forêt ! On voit le résultat lors des
contrôles\... »


**Problème :** l'enseignement de l'informatique communique la
connaissance des concepts et des produits ainsi que les compétences.
Cependant, les élèves et les étudiants ont souvent du mal à en extraire
les points essentiels et courent le risque de se perdre dans les
détails.


Un enseignement de qualité se caractérise par des objectifs clairs, et
pas seulement en informatique. La formulation et la communication des
objectifs pédagogiques a des effets positifs tant pour l'étudiant que
pour l'enseignant. Ces objectifs contribuent, lors des examens, à ne
contrôler que les sujets et les compétences traités pendant les cours.
Ils montrent pourquoi un enseignant accorde de l'importance à tel ou tel
contenu et constituent un moyen de communication avec les étudiants, les
parents et les autres enseignants. L'effet positif de la formulation
d\'objectifs pédagogiques explicites sur la formation des élèves est
également très bien documenté de manière empirique. Les objectifs
pédagogiques sont particulièrement importants dans l'enseignement de
l'informatique. Les élèves étudient les concepts fondamentaux et les
transpositions spécifiques au produit de ces concepts, et ils peuvent
bien souvent aussi acquérir puis exercer des compétences dans la
manipulation des outils informatiques concernés. Des objectifs
pédagogiques clairement formulés contribuent à garder une bonne vue
d'ensemble dans cet environnement complexe.


Benjamin Bloom distingue trois types d'objectifs pédagogiques : les
objectifs affectifs, qui concernent le développement ou la modification
des centres d'intérêt, des prédispositions, des valeurs et des aptitudes
sociales ; les objectifs psychomoteurs qui se rapportent à l'acquisition
et à l'utilisation d'aptitudes physiques, et les objectifs cognitifs qui
concernent la pensée, la connaissance, la résolution des problèmes et
l'expression des facultés intellectuelles. Nous allons ici nous limiter
aux objectifs cognitifs en tenant compte de certains aspects des
objectifs affectifs.


Le modèle des niveaux cibles offre un guide pratique pour la formulation
des objectifs pédagogiques \[ES71\]. Il distingue trois niveaux
d'objectifs pédagogiques : l'idée directrice, les objectifs de
disposition et les objectifs pédagogiques opérationnels. À ce modèle,
nous ajoutons un quatrième niveau pour l'enseignement de
l'informatique : les idées fondamentales. Les quatre niveaux du modèle
des niveaux cibles étendu sont résumés dans la Figure 10.1.


| **Idées fondamentales** | 
| -----------| 
| le sujet à enseigner |
| **Idées directrices** |
| Pourquoi enseigner quoi ? |
| **Idées fondamentales choisies** |
| **Objectifs de disposition** |
| **Objectifs pédagogiques opérationnels** |
| **Conditions générales** |
| Temps, infrastructure, |
| connaissances préalables, |
| plans pédagogiques, etc. |


**Fig. 10.1.** Les quatre niveaux des objectifs pédagogiques


**Idée directrice** Les idées directrices représentent le cadre de
référence. Elles donnent les raisons pour lesquelles un élément doit
être appris et retiennent comme conséquence ce qui est à apprendre. Les
idées directrices soulignent la pertinence d'un sujet et classifient ce
dernier dans un contexte plus large.


**Idées fondamentales** Chaque sujet possède ses idées fondamentales.
L'idée directrice ainsi que les conditions générales et le public ciblé
ou encore le temps disponible déterminent les idées fondamentales à
choisir pour un enseignement spécifique.


**Objectifs de disposition** Les objectifs de disposition englobent des
aspects affectifs tels que les prédispositions, les motivations ou la
volonté d'attitude. Les objectifs de disposition répondent à la
question : que pourront en principe accomplir les étudiants après la
formation et comment ce savoir-faire s'exprimera-t-il dans leur
comportement ?


**Objectifs pédagogiques opérationnels** Ils fixent les connaissances
concrètes des étudiants après le cours et comment celles-ci pourront
être vérifiées.


La formulation des objectifs pédagogiques en fonction des quatre niveaux
justifie dans l'enseignement de l'informatique une séparation claire des
contenus à long terme et à court terme (voir tableau ci-dessous) et
contribue ainsi à une meilleure réutilisation des cours préparés.

| **Niveau**  | **Référence au produit** | 
|-----------|-----------|
| Idée directrice | indépendante du produit |  
| Idées fondamentales | indépendantes du produit |  
| Objectifs de disposition | indépendants du produit | 
| Objectifs pédagogiques opérationnels | dépendants ou indépendants du produit | 


**Solution :** les objectifs pédagogiques justifient le choix de la
substance traitée et communiquent aux étudiants les contenus essentiels
et ce qu'ils doivent précisément apprendre. Les objectifs pédagogiques
communiquent les prestations d'enseignement qui sont attendues par les
étudiants au niveau conceptionnel et les compétences concrètes qu'ils
doivent acquérir.


Il existe, en pédagogie générale, de nombreuses publications et
recommandations relatives aux objectifs pédagogiques dans la didactique
en général : un grand classique est *Taxonomie des objectifs
pédagogiques* de Benjamin S. Bloom \[Blo56\]. Les articles de Karl Frey
dans le manuel Curriculum \[Fre75\] sont également à recommander.
Hilbert Meyer a publié un programme de formation relatif à l'analyse des
objectifs pédagogiques \[Mey84\].


**Exemple 1 : la fraude sur Internet**


De nombreux internautes utilisent les services en ligne pour le commerce
électronique, que ce soit pour faire des achats, pour mettre des objets
aux enchères ou effectuer des transactions purement financières. De
l'argent est ici en jeu, ce qui implique que les utilisateurs doivent
posséder une compréhension de base des principales menaces et des
mesures de sécurité. La fraude en est l'un des aspects, ce qui mène aux
objectifs pédagogiques possibles d'un cours sur ce sujet.


**Idée directrice** Comme partout dans la vie quotidienne et aussi dans
l'utilisation de Internet, nous courons le risque de tomber sur des
escrocs. Les utilisateurs d'Internet peuvent prendre des mesures
concrètes pour diverses escroqueries, mais seule la sensibilisation est
possible pour les autres.


**Idées fondamentales** La motivation des escrocs sur Internet est la
même que dans le monde réel : ils veulent votre argent et utilisent à
cet effet des astuces appropriées. Les différences entre les techniques
employées à ces fins profitent des propriétés particulières d'Internet,
par exemple la possibilité d'automatisation et la facilité avec laquelle
il est possible de communiquer avec un très grand nombre d'utilisateurs.
Le résultat est que même les fraudes impliquant de petites sommes
d'argent s'avèrent rentables si suffisamment de gens tombent dans le
panneau.


**Objectifs de disposition** Les étudiants ont une idée des principales
escroqueries auxquelles ils peuvent être confrontés lors de
l'utilisation d'Internet. Ils appliquent ces connaissances à leur propre
comportement d'utilisateur et, le cas échéant, font preuve de la
prudence nécessaire et mettent en œuvre les mesures de sécurité
appropriées.


**Objectifs pédagogiques opérationnels** Les étudiants peuvent citer de
mémoire et expliquer dans leurs propres termes au moins quatre grands
types de fraude. Exemple : numéroteur, systèmes boule de neige et autres
formes de fraude par e-mail, fraude aux enchères, phishing. Pour chaque
type de fraude, ils peuvent en outre nommer une règle de comportement
qui les aidera à être attentifs à l'avenir.


**Exemple 2 : adressage dans les protocoles de réseau**


Les diplômés en technique des systèmes issus d'un établissement
d'enseignement professionnel sont des gens qui seront chargés dans
l'avenir, entre autres, de la création et de l'administration d'un
réseau local ou encore de l'installation et de la maintenance d'un
serveur Web. Pour ce faire, ils ont besoin d'une courte initiation au
fonctionnement des différents protocoles de réseau, notamment la famille
des protocoles TCP/IP.


**Idée directrice** L'adressage est à la fois un aspect essentiel des
protocoles réseau et une condition indispensable pour une communication
opérationnelle. Les futurs techniciens système doivent maîtriser les
principes théoriques de l'adressage avec les protocoles réseau et
comprendre les relations aux différents niveaux (adresses MAC, adresses
IP, ports TCP).


**Idées fondamentales** La base de l'adressage dans les protocoles de
réseau est l'encapsulation, c'est-à-dire la délégation de tâches à
différentes couches et instances.


**Objectifs de disposition** Les étudiants ont conscience des
différentes couches de l'adressage dans le travail quotidien et peuvent
ainsi efficacement limiter les erreurs.


**Objectifs pédagogiques opérationnels** Les étudiants sont capables,
sans aide, d'esquisser un schéma des différentes couches de protocole et
d'indiquer quel adressage a lieu à quel niveau et aussi à quoi sert cet
adressage dans chaque cas (par exemple les adresses MAC identifient les
cartes réseau dans le réseau local, les adresses IP identifient les
interfaces réseau sur une plus grande distance, les ports TCP
identifient les applications). Ils sont en outre capables de retrouver
par eux-mêmes l'adresse MAC ainsi que l'adresse IP sur un ordinateur
Windows ou Linux et d'afficher la table de routage.


**Exemple 3 : Principes fondamentaux des compilateurs**


Les messages d'erreur du compilateur représentent l'un des premiers
obstacles rencontrés en abordant la programmation. De nombreux débutants
ressentent une certaine frustration à devoir corriger des erreurs telles
que l'oubli d'une virgule dans le texte du programme avant de pouvoir
finalement exécuter ce dernier. Les futurs programmeurs doivent donc
bien comprendre le mode de fonctionnement d'un compilateur.


**Idée directrice** Le chemin de la source au programme exécutable est
central. Les programmeurs doivent connaître ce chemin et savoir quelles
stations sont traversées par le texte source jusqu'à sa conversion en
code machine, par exemple la phase d'analyse lexicale, syntaxique et
sémantique.


**Idées fondamentales** Par exemple, les stratégies d'analyse (top-down,
bottom-up, etc.), la différence entre la syntaxe et la sémantique,
d'autres aspects de la grammaire ou du découplage des différents
composants d'un compilateur (analyseur, optimiseur, générateur de code)
par le code intermédiaire.


**Objectifs de disposition** Les programmeurs peuvent mieux classer les
messages d'erreur du compilateur et y réagir de manière appropriée. Ils
acceptent les messages d'erreur comme faisant normalement partie de leur
travail et ne réagissent pas avec frustration.

**Objectifs pédagogiques opérationnels** Les programmeurs peuvent
décrire sans aide et dans leurs propres mots les phases traversées par
un compilateur lors de la traduction du code source (balayage, analyse,
génération du code, optimisation). Ils peuvent associer les messages
d'erreur courants du compilateur Java (par exemple *}* missing ou method
index0f in the type String not applicable for argument (int)) à ces
phases et corriger l'erreur.
