**3**

\

**Les professeurs d'informatique ne sont pas la Hotline des TIC**

\

Il est 9H10 ce jeudi matin au Lycée d'enseignement professionnel Z. et
la professeure d'informatique G. s'offre une courte pause café en salle
des professeurs. Arrive alors le professeur d'anglais qui lui parle de
son projet de journal en ligne qu'il souhaite réaliser avec sa classe.
Il s'agit maintenant de connecter les appareils photo numériques aux
ordinateurs de la salle de classe pour charger les photos. Mais ça ne
fonctionne pas, peut-être parce que les ports USB sont désactivés ? Il
demande alors à G. de jeter un coup d'œil avant le prochain cours.
Pendant la pause de midi, deux autres collègues viennent lui exposer
leurs problèmes : l'un se plaint d'un lecteur de DVD défectueux et
l'autre voudrait connecter son portable au réseau Wifi de l'école, mais
il n'y arrive pas.

\

**Problème :** de nombreuses écoles disposent d'une infrastructure TIC
très complète, mais les services professionnels de maintenance et
d'assistance y font fréquemment défaut. Conséquence : les professeurs
d'informatique sont fortement sollicités par leurs collègues pour des
tâches de maintenance ou pour des demandes d'assistance.

\

Les entreprises et les écoles ont ceci en commun que la seule mise en
place d'une infrastructure TIC est insuffisante et ne représente
généralement qu'une petite partie des coûts encourus dans le domaine des
TIC. Un service de maintenance et d'assistance est indispensable pour
garantir l'utilisation de l'infrastructure.

\

La maintenance désigne globalement le contrôle, l'entretien et la
réparation des matériels et des logiciels. L'assistance désigne l'aide
apportée aux utilisateurs lors de l'utilisation des moyens TIC. Une
insuffisance de services de maintenance et d'assistance dans une école
résulte en une sous-exploitation de ses moyens TIC. Le coût
d'acquisition des matériels et des logiciels intervient une seule fois,
est généralement élevé, mais peut être calculé avec précision. La
maintenance et l'assistance sont malheureusement souvent négligées lors
de la budgétisation des acquisitions de TIC et les coûts annuels qui en
résultent sont sous-évalués. \[Ges\] et \[GD01\] recommandent un minimum
de 1 % par ordinateur pour la maintenance et l'assistance.

\

Pour des raisons financières, ces recommandations ne sont pas mises en
œuvre dans de nombreuses écoles ; une grande partie des services de
maintenance et d'assistance est assurée par les professeurs
d'informatique sur le modèle du volontariat. Aucun dédommagement
raisonnable ni compensation pour l'accomplissement de ces tâches n\'est
généralement prévu et les enseignants sacrifient de leur temps libre
pour maintenir le fonctionnement. À moyen terme, la disponibilité pour
de telles tâches supplémentaires s'amenuise. Il en résulte une
dégradation de la qualité des services de maintenance et d'assistance,
laquelle s'accompagne souvent d'une insatisfaction générale voire même
de conflits entre les utilisateurs de l'infrastructure TIC et les
professeurs d'informatique.

\

Les professeurs d'informatique sont tout bonnement surqualifiés pour les
opérations de maintenance et pour l'assistance des utilisateurs. Il
existe des solutions plus efficaces et moins coûteuses. Le service
d'assistance TIC d'une école devrait en outre être en mesure de réagir
efficacement en cas de problèmes pendant les cours, ce qui n'est pas le
cas des professeurs d'informatique du fait de leurs activités
d'enseignement.

\

Les écoles doivent trouver d'autres solutions que l'emploi des
professeurs d'informatique pour assurer les services de maintenance et
d'assistance des TIC. Après tout, le professeur d'économie domestique ne
va pas cuisiner à la cantine à ses heures perdues, la professeure
d'anglais n'est pas engagée par l'école pour effectuer des traductions
et le professeur d'économie ne tient pas la comptabilité de
l'établissement. Il est de la responsabilité de la direction de l'école
d'organiser des structures qui garantissent une assistance TIC
suffisante. Les professeurs d'informatique peuvent ici avoir un rôle de
conseiller, être chargés d'assurer une formation continue ou encore
participer à une stratégie de TIC au niveau de l'ensemble de l'école.

::: {#Section1 dir="ltr"}
\

Il appartient en outre aux professeurs d'informatique concernés de
s'impliquer directement lorsque les décideurs n'ont pas suffisamment
conscience de la problématique de la maintenance et de l'assistance des
TIC et de proposer des solutions constructives. Ces propositions peuvent
inclure la mise en place de séances de consultation régulières sur les
TIC, l'implication des professeurs ou des élèves, de courtes sessions de
perfectionnement en interne sous la forme de séminaires autour d'un
panier-repas ou encore l'appel à du personnel d'assistance aux TIC
externe.

\

**Solution :** seuls des services de maintenance et d'assistance
professionnels et efficaces peuvent garantir une utilisation optimisée
des moyens informatiques dans une école. Les professeurs d'informatique
n'ont pas vocation à se charger des tâches de maintenance et
d'assistance et doivent se protéger contre toute sollicitation excessive
à accomplir de telles tâches. Il appartient à la direction de l'école de
veiller à la mise en place de structures professionnelles pour la
maintenance et l'assistance des TIC.

\

\
:::
