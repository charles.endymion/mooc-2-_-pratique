**4**

\

**Les enseignants d'informatique n'ont pas la priorité auprès de
l'assistance TIC**

\

Le professeur d'informatique F. se présente auprès du service
d'assistance TIC local de l'école. Il a besoin pour les heures à venir
d'un logiciel d'apprentissage qui n'est pas présent sur les ordinateurs
de l'école. Il faut à présent installer ce logiciel, mais l'employée du
service d'assistance B. lui oppose un refus : elle ne peut pas donner
suite à cette demande d'installation immédiate et non prévue. F. propose
alors de procéder lui-même à l'installation. Après tout, en tant que
qu\'enseignant d'informatique il pourrait très facilement la réaliser ;
il a seulement besoin des codes d'accès. Mais B. ne donne pas son
accord : elle a la responsabilité de toute l'infrastructure. En
l'absence d'essais préalables, le risque résultant de l'installation est
trop élevé. F. repart fortement contrarié et déplore l'obstination du
service d'assistance TIC.

\

**Problème :** le service d'assistance TIC n'accepte d'apporter des
modifications à l'infrastructure que si celles-ci sont maîtrisées. Les
professeurs d'informatique ont souvent des demandes spéciales en matière
de matériel et de logiciel et veulent pouvoir concrétiser leurs souhaits
à volonté. Il se crée ici une zone de tensions.

\

Il existe une zone de tension permanente entre l'assistance TIC d'une
école et les d\'enseignants. L'objectif de l'assistance TIC est une
infrastructure la plus simple et la plus homogène possible. Les
modifications sur le réseau et les nouvelles installations doivent
impérativement être maîtrisées et ne doivent avoir lieu qu'à des moments
parfaitement définis. La priorité est donnée à la fiabilité du
fonctionnement de l'infrastructure et à sa sécurité.

\

Les professeurs, par contre, souhaitent le plus de souplesse possible.
L'installation de nouveaux logiciels ou les modifications de la
configuration doivent être rapides et sans contrainte administratives.

\

Tous les professeurs sont concernés par cette zone de tensions, mais
c'est avec les professeurs d'informatique que le potentiel de conflit
est le plus élevé. Il y a risque pour qu'ils effectuent eux-mêmes les
modifications techniques ou qu'ils s'immiscent trop dans le travail de
l'assistance TIC.

\

**Solution :** il appartient au service d'assistance TIC de définir et
de mettre en œuvre des procédures précises pour le traitement des
demandes d'installation. Tous les professeurs, plus particulièrement les
professeurs d'informatique, doivent s'en tenir à ces procédures.

\
