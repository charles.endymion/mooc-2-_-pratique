**8**

\

**Idées fondamentales**

\

Les professeurs de latin ou de biologie ont vraiment la belle vie ! Il
est vrai que « de bello Gallico » et l'anatomie humaine n'ont pas
beaucoup changé au cours des 2 derniers millénaires, à part peut-être le
fait que le professeur de biologie doit maintenant aussi aborder
l'analyse de l'ADN et la génétique. En informatique, par contre, la
fréquence des changements est un peu plus élevée !! Internet et World
Wide Web, Java, XML, l'informatique distribuée, les services Web, la
sémantique du Web ne sont que quelques exemples de nouveaux sujets. La
période de validité des cours préparés se limite souvent à quelques
années à peine et les plans de formation sont à revoir très
régulièrement. Un effort tout particulier est exigé de la part des
professeurs d'informatique qui doivent constamment se tenir informés des
dernières versions d'un produit. À cela vient s'ajouter le risque non
négligeable que les étudiants s'égarent dans les détails spécifiques au
produit et que les connaissances acquises seront bientôt périmées.

\

**Problème :** l'informatique évolue à une vitesse fulgurante, de
nouvelles technologies et de nouveaux produits voient continuellement le
jour. Dans l'enseignement de l'informatique, le risque de se laisser
trop influencer par cette évolution est élevé, une influence qui peut
même amener à perdre de vue les véritables fondamentaux de la formation.

\

Le développement très rapide des technologies et des produits dans le
secteur de l'informatique a plusieurs conséquences sur la formation :

\

**de nouvelles sessions de formation** Les besoins changeants de
l'économie conduisent régulièrement à la création de nouveaux cours de
formation, qui deviennent souvent caducs ou sont complètement abandonnés
après quelques années à peine. Ainsi, dans le passé, les écoles
formaient des opérateurs, des analystes et des programmeurs Cobol, alors
que les compétences recherchées aujourd'hui sont principalement celles
des créateurs de sites Web, des programmeurs Java ou des spécialistes de
la sécurité.

\

**supports de cours d'informatique** Le secteur de l'informatique n'est
pas très attrayant pour les éditeurs de manuels d'enseignement, car les
programmes changent fréquemment et de coûteuses mises à jour sont
nécessaires. L'offre en supports de cours d'informatique de qualité est
donc très limitée et plusieurs éditeurs spécialisés dans l'enseignement
se sont retirés de ce marché éphémère.

\

**charge des professeurs d'informatique** L'accent devrait être mis sur
l'enseignement proprement dit. Que faut-il enseigner ? Comment préparer
une pédagogie adaptée et assurer une transmission intéressante des
connaissances ? Mais le quotidien de l'enseignement se concentre souvent
sur l'initiation aux nouvelles technologies ou aux nouveaux produits. En
plus de cela, il faut régulièrement adapter les documents de cours, les
fiches d'exercice et les présentations aux dernières versions présentes
sur le marché.

\

**période de validité des connaissances** Les connaissances acquises par
les étudiants durant les cours d'informatique deviennent souvent
inutiles peu de temps après. L'accent n'est pas mis sur des concepts
pérennes, mais plutôt sur la connaissance des produits et des tâches
associées. Beaucoup d'étudiants n'ont pas conscience qu'en informatique
aussi, il existe des connaissances et des concepts durables.

\

Ces problèmes soulignent d'autant plus l'importance du choix judicieux
de la teneur des cours en informatique. Ce choix est cependant plus
difficile que dans d'autres spécialités, car l'informatique couvre un
éventail de sujets très large et est enseignée dans des établissements
de formation différents.

\

\

\

\

::: {#Section1 dir="ltr"}
Les idées fondamentales de Bruner \[Bru60\], adaptées à l'enseignement
de l'informatique par Schwill \[Sch93\], se sont avérées être un outil
approprié pour le choix du contenu des cours. Alors que Bruner ne
formule que très vaguement la notion d'idée fondamentale, Schwill en
donne une définition précise en se basant sur quatre critères :
horizontal, vertical, temps et sens. Nous reprenons ici la définition de
Schwill et la complétons par un cinquième critère : la représentation.
Une idée fondamentale est un fait qui

\

**critère horizontal** peut être utilisé ou reconnu sous de nombreuses
formes dans différents domaines.

\

**critère vertical** peut être démontré et communiqué à chaque niveau
intellectuel.

\

**critère de temps** est clairement perceptible dans l'évolution
historique et reste pertinent à long terme.

\

**critère de sens** est en rapport avec le langage et la pensée au
quotidien du monde réel.

\

**critère de représentation** peut être représenté à différents niveaux
cognitifs (énactif, iconique, symbolique).

\

Un enseignement qui s'appuie sur des idées fondamentales garantit le
choix d'un contenu pérenne, ce qui est à la fois pertinent et stimulant
pour les étudiants.

\

Il n'est cependant souvent pas facile de faire ressortir les idées
fondamentales, et la tentation de vouloir qualifier un sujet d'important
et de fondamental sans réflexion approfondie est d'autant plus grande.
Les cinq critères énoncés ci-dessus permettent de vérifier si un fait
est effectivement une idée fondamentale.

\

**Qu'apportent les idées fondamentales à l'enseignement ?**

\

L'examen du contenu sur la base des cinq critères apporte une aide
précieuse dans la préparation des leçons. L'enseignant est obligé de
réfléchir sur la signification d'un fait : pourquoi la substance
est-elle importante ? Pourquoi les élèves doivent-ils la comprendre ? La
réponse à ces questions s'intègre directement dans l'enseignement. Le
contrôle du critère de représentation donne lieu à une présentation
intuitive du fait, le critère horizontal couvre les analogies dans la
vie quotidienne tandis que le critère vertical mène à des explications
facilement compréhensibles.

\

Les réflexions relatives aux idées fondamentales aident également
l'enseignant à structurer les documents du cours. Ceux-ci sont séparés
conséquemment en contenus pérennes et en contenus dont la durée de vie
est plus courte, souvent spécifiques à un produit. Ces derniers doivent
être actualisés plus fréquemment, alors que la période de validité des
contenus pérennes est plus longue. Grâce à la séparation, seules des
parties des documents nécessitent une révision occasionnelle.

\

Pour les élèves, les étudiants ou les stagiaires, les idées
fondamentales simplifient la compréhension de faits complexes. Les faits
peuvent être intégrés plus facilement dans un ensemble plus grand et le
transfert des connaissances est encouragé. Les connaissances acquises
précédemment et réellement comprises peuvent plus facilement être
transposées à des situations nouvelles.

\

En insistant sur les idées fondamentales, on évite le risque de se
perdre dans des détails éphémères, spécifiques à un produit. Mais cela
ne suffit pas à identifier le plus simplement possible toutes les idées
fondamentales dans un domaine précis. Suivant le public visé, les
concepts fondamentaux d'un sujet peuvent être extrêmement pertinents ou
dénués de toute importance pour son enseignement. Les formes normalisées
des bases de données relationnelles présentent peu d'intérêt si la
formation sur les bases de données s'adresse aux utilisateurs, mais sont
primordiales pour les futurs développeurs. Il est de la responsabilité
de l'enseignant de choisir parmi les idées fondamentales celles qui
seront significatives pour le public visé. Les discussions avec les
collègues sont d'excellents moyens d'identifier les idées fondamentales.
De bonnes indications sur ces dernières peuvent souvent se retrouver
dans les publications spécialisées, notamment les classiques d'un
domaine. Il est également possible d'envisager une coopération avec les
étudiants en vue de développer les idées fondamentales d'un sujet et
d'exploiter ainsi leur métaréflexion pour leur propre enseignement.

\

**Solution :** la formation informatique doit s'articuler autour de
contenus pérennes -- les concepts fondamentaux et les méthodes. Le
principe des idées fondamentales représente un instrument qui permet de
vérifier le caractère significatif d'un sujet ou d'un fait. Cette
vérification fournit des indices importants pour la préparation de la
substance et l'organisation du cours.

\

Mais les idées fondamentales ne se rencontrent pas uniquement en
informatique, chaque domaine scientifique se caractérise par des idées
fondamentales. L'enseignement des mathématiques à l'université, par
exemple, ne se concentre pas en priorité sur les fonctions de la
dernière calculatrice, mais explique des idées telles que la déduction
et l'induction ou encore la discrétisation.

\

Même en physique, il existe un large consensus au sujet des
connaissances fondamentales les plus importantes. En mécanique, ce sont
des approches scientifiques telles que l'expérience, la modélisation, la
formulation mathématique et l'interprétation qui sont indiquées et
l'enseignement communique les lois de conservation fondamentales de la
mécanique, de l'énergie et des impulsions. La deuxième loi de la
thermodynamique établit la relation entre les macro-états et les
micro-états sous-jacents. Ces sujets occupent depuis toujours une place
prépondérante dans l'enseignement. Ce n'est qu'après avoir dressé les
bases permettant de comprendre les phénomènes naturels qu'il sera
possible d'expliquer des « systèmes physiques » tels que les moteurs ou
les centrales nucléaires.

\

L'accent mis sur les fondements de la physique comme un objet central de
l'enseignement a fait ses preuves. Les principes communiqués permettent
aux élèves de classifier les nouveaux développements ou les nouvelles
connaissances. Le choix d'une substance stable à long terme permet aux
enseignants et aux éditeurs de matériel éducatif une analyse et une mise
en œuvre approfondies.

\

Contrairement aux matières traditionnelles, l'informatique en tant que
sujet d'enseignement est encore mal établie. Les enseignants ne peuvent
généralement pas s'en remettre à une liste exhaustive d'idées
fondamentales et n'ont souvent pas d'autre choix que d'identifier
eux-mêmes celles qui seront adaptées au public visé dans un domaine
précis.

\

Dans ce contexte des idées fondamentales, nous recommandons la
consultation de l'ouvrage *The New Turing Omnibus: Sixty-Six Excursions
In Computer Science* par Dewdney \[Dew01\]. Ce livre présente des
approches simples et motivantes pour différent sujets. Il est à la fois
un recueil d'exemples et une source d'idées. Un autre livre à
recommander est *Computers Ltd.: What They Really Can't Do* de David
Harel \[Har01\], une présentation résumée des sujets fondamentaux de
l'informatique théorique, de la calculabilité et de la théorie de la
complexité.

\

Nous citerons ci-après sans aucune prétention à l'exhaustivité un
certain nombre d'idées fondamentales, avec une description détaillée des
trois premières.

\

**Exemple 1 : idée fondamentale pour les formats graphiques : graphiques
matriciels et vectoriels**

\

Un graphique matriciel (également appelé graphique en pixels ou Bitmap)
mémorise les informations de chaque point (pixel) acquis d'une image.
Les graphiques matriciels sont généralement dans l'un des formats BMP,
GIF, JPEG, TIFF ou PNG. Un graphique vectoriel décrit une image par des
fonctions mathématiques : les vecteurs définissent des droites, des
courbes ou des surfaces. Quatre informations sont nécessaires pour
mémoriser l'image d'un cercle avec un graphique vectoriel : la position
du centre, le diamètre du cercle, la couleur de la ligne circulaire et
l'épaisseur du trait. Contrairement aux graphiques matriciels, les
graphiques vectoriels peuvent être redimensionnés à volonté sans aucune
perte de qualité. Les propriétés des lignes, courbes ou surfaces des
graphiques vectoriels sont conservées et peuvent être modifiées
postérieurement. Les graphiques vectoriels ne conviennent pas à la
représentation de photographies et d'images similaires, car il est
quasiment impossible de définir un modèle mathématique de celles-ci
(Fig. 8.1)

![](8_FR_V2_html_12c4d571ec9bf3ad.gif){width="425" height="184"}

**Fig. 8.1.** Différences entre un graphique matriciel et un graphique
vectoriel

\

Nous allons vérifier à l'aide des cinq critères si la distinction entre
les graphiques matriciels et vectoriels est effectivement une idée
fondamentale.

\

**Critère horizontal** Une idée fondamentale doit être applicable dans
divers domaines. Les autres domaines dans lesquels peut être appliquée
l'idée des graphiques matriciels et vectoriels ne semblent pas évidents
au premier abord. Un argument pourrait être qu'en architecture, par
exemple, les modèles sont représentés par des dessins au trait sur des
plans ou par des images proches de la réalité. Les dessins des plans
correspondent alors aux graphiques vectoriels et les images réalistes
aux graphiques matriciels. Mais les formes de représentation en
architecture sont trop proches du graphisme proprement dit  ; ce qu'il
faut rechercher, ce sont des domaines qui n'ont aucun rapport apparent
avec les graphiques. Ceci nécessite une réflexion approfondie sur l'idée
fondamentale qui se cache derrière ces deux formats.

\

Avec les graphiques vectoriels, les objets géométriques sont décrits par
des objets élémentaires comme des lignes ou des cercles. Les graphiques
matriciels sont décomposés en pixels. La description des objets en
utilisant une série d'objets standard simples ou par une description
détaillée des différentes composantes de l'objet se rencontre très
régulièrement dans la vie quotidienne. Les instructions pour assembler
une armoire de chez IKEA, par exemple, font référence à des « objets
élémentaires » tels que les parois latérales ou la porte de gauche. La
nomenclature de l'armoire contient des indications telles que le nombre
de vis d'une taille donnée.

\

**Critère vertical** Une idée fondamentale doit pouvoir être démontrée à
chaque niveau intellectuel. Il est en fait très facile de vérifier si le
critère vertical est rempli. Il suffit de savoir si l'idée fondamentale
d'un concept pourrait être expliquée à un enfant, ce qui est extrêmement
simple à réaliser dans le cas de la différenciation entre les graphiques
matriciels et vectoriels : un enfant connaît les graphiques matriciels
sous la forme de pions de couleur formant une mosaïque. Les graphiques
vectoriels sont quant à eux des jeux consistant à relier d'un trait des
numéros pour former des séries de polygones (Fig. 8.2).

\

![](8_FR_V2_html_64beb37917f86189.gif){width="548" height="340"}

**Fig. 8.2.** Les graphiques matriciels et vectoriels expliqués aux
enfants

\

Les réflexions relatives au critère vertical révèlent la valeur que lui
accorde l'enseignant : l'explication adressée aux enfants mène
directement aux avantages et aux inconvénients des deux formats. Les
images au format matriciel occupent plus de place en mémoire que les
graphiques vectoriels et ne peuvent pas être agrandies ou réduites
facilement. Au contraire, les graphiques vectoriels occupent très peu de
place en mémoire et peuvent être redimensionnés presque à volonté.

\

**Critère de temps** Une idée fondamentale doit avoir été valable dix
ans auparavant et devra toujours être pertinente dans dix ans. C'est le
cas de notre exemple. L'architecture utilise depuis longtemps déjà des
plans au trait qui peuvent être décrits comme des graphiques vectoriels.
En art, il existe le style appelé pointillisme (ou néo-impressionnisme)
qui a connu son heure de gloire avec des peintres tels que Georges
Seurat et Camille Pissarro. Le pointillisme se caractérise par des
dessins constitués de petits points de couleur individuels, parfait
exemple d'image matricielle. En informatique aussi, ces deux formats
graphiques ont une longue tradition.

\

**Critère de sens** Une idée fondamentale doit être pertinente dans le
monde des étudiants. Elle doit être claire, ce qui justifie la
confrontation avec les faits pour les étudiants. L'avantage est évident
dans notre exemple : l'utilisateur d'un ordinateur rencontre
régulièrement les deux formats graphiques. Les appareils photo
numériques et les scanners produisent des graphiques matriciels, les
programmes d'illustration des graphiques vectoriels.

\

**Critère de représentation** Une idée fondamentale peut généralement
être comprise de manière énactive par une action positive, visualisée
sous forme iconique et décrite symboliquement sous une forme compacte.
Les représentations énactives sont parfois difficiles à trouver, mais
sont très utiles car elles fournissent de bonnes analogies. Des
indications de représentations énactives sont mentionnées dans le
critère vertical pour les graphiques matriciels et vectoriels.

\

**Exemple 2 : limitation d'une recherche à un sous-ensemble à l'aide de
métadonnées**

\

Lors d'une recherche sur Internet, il est souvent utile de réduire tout
d'abord à un sous-ensemble le volume incalculable de documents proposés
à la sélection sur la base de critères (métadonnées) d'ordre supérieur.
La recherche peut se restreindre aux documents dans une certaines
langue, à un type de fichier donné ou encore aux documents d'un site Web
bien précis. Pour rechercher les universités françaises possédant un
département de robotique, par exemple, il convient de limiter la
recherche aux domaines se terminant par .fr. La recherche proprement
dite ne sera effectuée que sur ce sous-ensemble (Fig. 8.3). Nous allons
maintenant vérifier si la restriction de la recherche à un sous-ensemble
peut être considérée comme une idée fondamentale :

\

**critère horizontal** Prenons un patient qui se présente à un hôpital
et se plaint de fortes douleurs aiguës. Nous allons d'abord essayer de
localiser la douleur puis d'orienter le patient vers des spécialistes
compétents dans le service approprié.

\

**Définition du sous-ensemble**

**Classification de la pertinence**

ensemble complet de documents (non trié)

sous-ensemble non trié

classement en fonction de l'indice de pertinence

1\. .........

2\. .........

3\. .........

4\. .........

5\. .........

**Fig. 8.3.** Limitation d'une recherche à un sous-ensemble

\

**critère vertical** Si une personne achète une paire de chaussures
neuves, elle limite d'abord sa recherche dans le secteur de la chaussure
à la pointure désirée, au modèle de chaussure et éventuellement à
d'autres critères (métadonnées) comme la couleur, la matière ou le
fabricant. L'acheteur potentiel peut ensuite affiner son choix parmi la
gamme de chaussures présentée.

\

**critère de temps** La restriction à un sous-ensemble correspond à
l'utilisation d'un filtre. Les techniques de filtrage interviennent
depuis longtemps dans de nombreux domaines. Les filtres sont également
utilisés en informatique, par exemple le filtrage des fichiers au niveau
du système d'exploitation ou, plus généralement, un filtre textuel.

\

**critère de sens** L'avantage de la limitation à un sous-ensemble
spécifique est évident : au lieu de rechercher une aiguille dans une
botte de foin, la recherche est orientée dans la partie pertinente de la
botte.

\

**critère de représentation** La restriction d'une recherche à un
sous-ensemble peut être illustrée par la recherche d'une pièce précise
d'un puzzle qui en compte un très grand nombre. Suivant que la pièce
recherchée fait partie du contour ou de l'horizon, seules seront
examinées les pièces du bord ou celles ayant la couleur bleue du ciel.

\

**Exemple 3 : diviser pour régner comme principe algorithmique
fondamental**

\

Diviser pour régner (Divide and Conquer) est l'une des idées
fondamentales les plus importantes de l'informatique. Ainsi, un problème
complexe sera tout d'abord divisé en problèmes plus petits et plus
faciles à résoudre. Ces petits problèmes sont alors résolus et les
solutions partielles sont finalement assemblées en une solution globale.
Le principe de diviser pour régner intervient dans de nombreux
domaines : dans les bases de données, en développement de logiciel ou
dans la conception d'algorithmes. L'application répétitive de ce
principe conduit à la récurrence, autre idée fondamentale de
l'informatique.

\

**Saisie**

1ère division (avec Ø)

2ème division (avec Ø)

1er assemblage

2ème assemblage

3ème assemblage

**Résultat**

**Fig. 8.4.** Illustration du principe de « Diviser pour régner » avec
l'exemple de l'algorithme de tri Quicksort

\

La vérification des cinq critères se résume à ce qui suit :

\

**critère horizontal** Le principe « Diviser pour régner » régit le
monde, cette méthode a fait ses preuves en politique depuis Caius Julius
César. Quiconque veut déléguer des tâches dans la vie quotidienne sera
immanquablement amené à diviser le travail en opérations plus simples.

\

**critère vertical** Diviser pour régner s'applique dès la maternelle :
essayez d'imaginer les 250 enfants d'une commune regroupés dans une
seule et même salle de classe !

\

**critère de temps** Nous pouvons saluer la politique « Divide et
impera » de César.

\

**critère de sens** Une surcharge de travail est l'annonce de
l'application du principe « Diviser pour régner ».

\

**critère de représentation** Il est très facile d'illustrer le principe
« Diviser pour régner », par exemple par une approche méthodique dans la
réalisation d'un puzzle.

\

**Exemple 4 : 5x3 idées fondamentales**

\

Les 15 idées fondamentales suivantes sont simplement des exemples et ne
se veulent aucunement exhaustives. La structure est définie en fonction
du public visé et d'après une subdivision possible de l'informatique en
cinq domaines, présentée par Peter Denning \[Den03\].

\

+----------------+----------------+----------------+----------------+
| \              | **Utilisateur  | **Technicien   | **Ingénieur    |
|                | de TIC**       | en             | i              |
|                |                | informatique** | nformaticien** |
+----------------+----------------+----------------+----------------+
| **Calcul**     | Qu'est-ce      | Logique        | Machines de    |
|                | qu'un          | déclarative    | Turing         |
|                | programme ?    | booléenne      |                |
+----------------+----------------+----------------+----------------+
| **C            | Communication  | Codes          | Théorème       |
| ommunication** | synchrone et   | correcteurs    | d'é            |
|                | asynchrone     | d'erreurs      | chantillonnage |
|                |                |                | de Shannon     |
+----------------+----------------+----------------+----------------+
| **             | Gestion des    | Organisation   | Ressources     |
| Coordination** | versions       | des opérations | communes       |
+----------------+----------------+----------------+----------------+
| **Au           | Techniques de  | Équilibrage    | Algorithmes    |
| tomatisation** | filtrage       | des charges    | génétiques     |
+----------------+----------------+----------------+----------------+
| **             | Index d'un     | Mémoire        | Métadonnées    |
| Mémorisation** | moteur de      | temporaire     |                |
|                | recherche      |                |                |
+----------------+----------------+----------------+----------------+

\

Nous indiquons pour chaque idée fondamentale quelques mots-clés en
rapport avec le contenu et démontrons pourquoi et dans quel contexte
l'idée fondamentale est importante pour le public concerné.

\

**Calcul**

\

**Qu'est-ce qu'un programme ?** Les utilisateurs doivent avoir une idée
de ce qu'est effectivement un programme. Un programme peut être défini
comme une spécification statique de processus dynamiques. Cette
définition est toutefois beaucoup trop abstraite pour qu'un utilisateur
puisse l'exploiter, alors que l'idée qu'elle recèle peut être illustrée
au moyen d'exemples simples.

\

**Logique déclarative booléenne** La logique intervient dans de nombreux
domaines : interrogation des bases de données, programmation ou encore
configuration des règles de filtres anti-spam ou de pare-feu. Par
conséquent, les techniciens en informatique doivent connaître les
principes de base de la logique déclarative booléenne.

\

**Machines de Turing** Les déclarations sur la calculabilité ou la
complexité se réfèrent à un modèle mathématique précis qui définit les
données et les opérations autorisées. Les machines de Turing, les
fonctions récurrentes ou le calcul Lambda sont des exemples de ces
modèles mathématiques. Un informaticien diplômé d'une université doit
être familiarisé avec ces principes théoriques.

\

**Communication**

\

**Communication synchrone et asynchrone** Les utilisateurs sont
aujourd'hui confrontés à une multitude de moyens de communication :
e-mail, SMS, Chat, forums, RSS et Podcasts pour ne citer que quelques
exemples. L'utilisation de l'outil adéquat à bon escient présuppose la
compréhension des principes fondamentaux de la communication comme les
différences entre la communication synchrone et asynchrone ou encore
entre les services d'émission et de collecte.

\

**Codes correcteurs d'erreurs** Lors de la transmission de données, le
récepteur reçoit fréquemment des données erronées en raison de canaux de
transmission défectueux. Les techniciens en informatique doivent
comprendre comment définir les codes de détection et de correction
d'erreur en ajoutant de la redondance.

\

**Théorème d'échantillonnage de Shannon** De nombreuses technologies
s'articulent autour de la conversion de signaux analogiques en signaux
numériques et inversement. Les informaticiens doivent savoir comment
reconstruire et interpoler des fonctions à partir de valeurs
échantillonnées et avoir une idée sur la relation entre
l'échantillonnage et le spectre.

\

**Coordination**

\

**Contrôle de la version** L'utilisation d'un système de contrôle de
version se justifie dès que plusieurs personnes travaillent ensemble sur
un même document. Les utilisateurs sont déjà confrontés aux avantages et
aux inconvénients de ces systèmes en utilisant le mode Suivi des
modifications dans MS Word. Il est donc utile de comprendre les
problèmes courants rencontrés avec des processus exécutés en parallèle.

\

**Organisation des opérations** Les services d'assistance informatique
doivent souvent planifier des opérations avec un temps d'exécution
critique, par exemple lors du remplacement d'un serveur ou d'une
infrastructure de réseau. Les principes fondamentaux de l'ordonnancement
et de la gestion des opérations s'avèrent très utiles pour de telles
tâches.

\

**Accès contrôlé aux ressources communes** La société a mis en place des
mécanismes de contrôle centraux partout où peuvent se produire des
problèmes de synchronisation. Ces mécanismes jouent un rôle important
dans les protocoles de communication ou lors de la programmation de
processus concurrents et font donc partie des connaissances de tout
informaticien.

\

**Automatisation**

\

**Techniques de filtrage** Dans de nombreux programmes, les utilisateurs
ont la possibilité de définir des filtres : filtre anti-spam, filtre de
recherche de certains fichiers ou filtre dans une feuille de calcul.
Mais en l'absence de connaissance des principes de définition des
filtres, les solutions de filtrage s'avèrent souvent inefficaces, voire
contre-productives.

\

**Équilibrage des charges** La surveillance du taux d'utilisation des
ressources fait également partie des tâches des services d'assistance
informatique dans le domaine des réseaux. Les connaissances relatives à
l'équilibrage des charges et à l'ordonnancement permettent au
spécialiste des réseaux de gérer plus efficacement ces ressources.

\

**Algorithmes génétiques** La technique s'appuie souvent sur des
procédés utilisés par la nature pour une résolution presque optimale de
nombreux problèmes. Quelques exemples issus de l'informatique sont les
algorithmes génétiques, la logique floue ou les réseaux neuronaux. Les
informaticiens ont besoin de connaître certains de ces procédés
d'optimisation.

\

**Mémorisation**

\

**Index d'un moteur de recherche** L'utilisation rationnelle et efficace
des moteurs de recherche sur l'Internet semble à priori d'une simplicité
enfantine. Mais la compréhension élémentaire du mode de fonctionnement
d'un moteur de recherche, par exemple du processus d'indexation et de
normalisation des documents texte, reste incontournable pour ne pas en
limiter inutilement l'efficacité.

\

**Mémoire temporaire** Les stocks ou réserves temporaires font partie de
notre quotidien. Les outils utilisés le plus fréquemment tels que le
marteau ou le tournevis sont rangés dans l'appartement, pas au garage.
Les services d'assistance informatique doivent comprendre les principes
généraux de la mise en cache et des serveurs proxy et connaître leurs
principaux domaines d'application en informatique.

\

**Métadonnées** L'acquisition de données sur des données est un principe
fondamental présent dans les systèmes d'exploitation, les bases de
données ou l'architecture des grands sites Web. Les ingénieurs en
informatique doivent posséder des connaissances approfondies sur les
métadonnées et connaître les différences entre les métadonnées
normalisées et non normalisées ou encore entre les thésaurus contrôlés
et non contrôlés.

\
:::
