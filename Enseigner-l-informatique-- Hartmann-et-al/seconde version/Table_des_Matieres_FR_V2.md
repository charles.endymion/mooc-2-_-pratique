**eXamen.press**

\

**eXamen.press** est une collection qui transmet la théorie et la
pratique dans tous les domaines de l'informatique pour l'enseignement
secondaire et supérieur.

\

Werner Hartmann · Michael Näf

Raimond Reichert

\

\

\

**Planification** **et mise en œuvre de l'enseignement de
l'informatique**

\

\

\

\

\

\

Avec 38 illustrations

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

\

![](Table_des_Matieres_FR_V2_html_3347c5343d1ce305.gif){width="185"
height="69"}

\

Werner Hartmann

Michael Näf

Raimond Reichert

\

\

\

\

\

\

\

\

\

\

\

**1ère réédition corrigée 2007**

\

Informations bibliographiques de la Deutschen Nationalbibliothek

La Deutsche Nationalbibliothek répertorie cette publication dans la
Deutschen Nationalbibliografie (bibliographie nationale allemande). ;

Les informations bibliographiques détaillées peuvent être consultées sur
l'Internet à l'adresse http://dnb.d-nb.de.

\

**ISSN 1614-5216**

**ISBN-13 978-3-540-34484-1 Springer Berlin Heidelberg New York**

\

Le présent ouvrage est protégé par la loi sur les droits d'auteur. Tous
les droits qui en résultent, même en cas d'utilisation partielle, sont
réservés, notamment ceux de la traduction, de la réimpression, de la
présentation, de l'extraction des illustrations et des tableaux, de la
radiodiffusion, du microfilmage ou de la reproduction par d'autres
moyens et du stockage dans des équipements de traitement de données.
Toute reproduction du présent ouvrage ou de parties de celui-ci, même
dans des cas particuliers, n'est autorisée que dans les limites des
dispositions légales de la Loi sur la propriété intellectuelle de la
République fédérale d'Allemagne du 9 septembre 1965, dans l'édition en
vigueur, et doit systématiquement faire l'objet d'une rémunération. Les
infractions sont passibles de poursuites en vertu de la Loi sur la
propriété intellectuelle.

\

Springer est une entreprise du groupe Springer Science+Business Media

springer.de

\

© Éditions Springer Berlin Heidelberg 2006

\

La reproduction de noms usuels, noms commerciaux, marques, etc. dans le
présent ouvrage, même sans signalisation particulière, n'implique pas
que ces noms peuvent être considérés comme libres de droits dans le sens
de la législation sur la protection des marques et peuvent ainsi être
utilisés par quiconque. Le plus grand soin a été accordé à la rédaction
du texte et aux illustrations. L'éditeur et l'auteur ne peuvent
cependant pas assumer la responsabilité légale ou autre pour
d'éventuelles informations erronées et leurs conséquences.

\

PAO : données prêtes à l'impression des auteurs

Fabrication : LE-TEX, Jelonek, Schmidt & Vöckler GbR, Leipzig

Couverture : KünkelLopka Werbeagentur, Heidelberg

Imprimé sur du papier sans acide 176/3180YL -- 5 4 3 2 1 0

\

\

**Préface**

\

\

\

\

\

Que faut-il enseigner et comment l'enseigner ? C'est la question que se
posent tous les enseignants et formateurs en informatique en préparant
et en dispensant leurs cours, aussi tant bien dans l'enseignement
traditionnel qu'en formation professionnelle. Il existe de nombreuses
publications relatives à la justification et au contenu de la matière
« informatique » à l'école, mais seuls quelques rares documents traitent
de la méthodologie de l'enseignement de l'informatique. C'est là
qu'intervient le présent ouvrage : il apporte un soutien méthodologique
à la conception de l'enseignement.

\

À qui s'adresse ce livre ? Ce livre est destiné aux professeurs
d'informatique des établissements d'enseignement professionnel et des
lycées, aux formateurs internes des entreprises, aux chargés professeurs
d'informatique de cours des instituts universitaires de technologie
ainsi qu'aux futurs enseignants. Les différents publics visés se
retrouvent dans la formulation et les exemples choisis. Le lecteur est
invité à transposer les observations et les exemples à ses propres
habitudes d'enseignement.

\

Mais Toutefois, une méthodologie de l'enseignement de l'informatique qui
s'adresse à la fois aux enseignants du secondaire, aux professeurs
chargés de cours en d'université et aux animateurs de la formation
continue professionnelle est-elle réellement possible ? Oui, nous en
sommes convaincus de par notre longue expérience de l'enseignement de
l'informatique dans différents établissements et instituts. Les
problèmes spécifiques à l'informatique sont les mêmes, quel que soit le
niveau : l'informatique est abstraite, l'informatique elle subit des
mutations rapides et son l'enseignement de l'informatique inclut des
travaux sur ordinateur.

\

Que propose ce livre ? Il contient toute une série d'instructions pour
la planification et la mise en œuvre de l'enseignement de
l'informatique. Tous les chapitres sont organisés selon le même modèle
et commencent par un exemple illustrant parfaitement un problème. Cette
introduction est suivie d'une définition concise du problème et de son
analyse ainsi que de solutions qui ont été éprouvées en pratique et qui
sont regroupées sous la forme d'une synthèse. Les solutions sont ensuite
illustrées au moyen d'exemples concrets issus du quotidien de
l'enseignement. Ces exemples couvrent toute la gamme des sujets abordés
dans l'enseignement de l'informatique, depuis la formation à une
application jusqu'à l'enseignement de l'informatique dans une
université. Ils sont destinés à fournir au lecteur des idées et des
suggestions pour ses propres cours.

\

\

\

\

Exemple de problème rencontré au quotidien

\

**Description concise du problème**

\

Analyse du problème

\

Déduction de la solution

\

**Description concise de la solution**

\

*en option :* Exemples

\

*en option :* Bibliographie complémentaire

\

Que ne propose pas ce livre ? Il ne remplace pas la lecture d'ouvrages
traitant de la pédagogie en général. Nous supposons que nos lectrices et
lecteurs sont déjà familiarisés avec les diverses méthodes et techniques
d'enseignement ; il existe de bon bons ouvrages de référence à ce sujet.
De plus, les aspects méthodico-pédagogiques traités dans cet ouvrage
livre sont délibérément représentés par des exemples choisis. Le livre
ne prétend pas non plus offrir l'unique solution scientifiquement fondée
aux problèmes soulevés. L'enseignement est un domaine complexe très
difficile à appréhender dans son intégralité et - pour rester dans le
langage de l'informatique - à modéliser et à mettre en œuvre.

\

Qui se cache derrière ce livre ? Les trois auteurs de cet ouvrage
possèdent chacun leur propre expérience à différents niveaux de
l'enseignement et sur différents sujets enseignés. Mais cCe livre
n'aurait cependant pas pu voir le jour sans les idées et les exemples
des élèves collectés au cours des dernières années par les trois auteurs
dans le cadre des cours d'informatique et des sessions de formation à
l'EPF de Zurich, à la Pädagogischen Hochschule de Bern et dans le cadre
des stages intensifs de perfectionnement pour les formateurs en
informatique en entreprise. De nombreuses parties de l'ouvrage
s'appuient sur le contexte pédagogique en général que les auteurs ont
appris lors des sessions de formation de Karl Frey à l'EPF de Zurich.
Les auteurs sont reconnaissants à envers Karl Frey pour cette vision
d'une pédagogie professionnelle, scientifiquement fondée et tout de même
proche de la réalité pratique. Nous adressons également nos
remerciements à Beat Döbeli Honegger pour ses remarques critiques et sa
relecture minutieuse et à Matthias Dreier pour la réalisation des
illustrations.

\

\

**Table des matières**

\

\

\

\

\

\

\

**Partie I Classification et délimitation**

\

1 L'informatique en tant que sujet de l'enseignement informatique 3

\

2 Les professeurs d'informatique enseignent l'informatique 9

\

3 Les professeurs d'informatique ne sont pas la Hotline hotline des TCI
technologies de l'information et de la communication (TIC) 11

\

4 Les professeurs d'informatique n'ont pas la priorité auprès de
l'assistance TCIC 15

\

5 Le besoin en formation continue des professeurs d'informatique est
très élevé 17

\

**Partie II Choix du contenu des cours**

\

6 L'enseignement de l'informatique englobe la connaissance des concepts

et la connaissance des produits 23

\

7 Adapter le contenu des cours au public visé 29

\

8 Idées fondamentales 31

\

**Partie III Planification de l'enseignement**

\

9 Les différents accès à l'enseignement de l'informatique 47

\

10 Objectifs pédagogiques dans l'enseignement de l'informatique 51

\

11 Les cours d'informatique doivent être soigneusement planifiés 57

\

**Partie IV Méthodes d'enseignement**

\

12 Méthodes pour l'enseignement de l'informatique 63

\

13 Pédagogie expérientielle 67

\

14 Travail en groupe 73

\

15 Programmes dirigés 83

\

16 Apprentissage par la découverte 91

\

17 Pédagogie de projet 99

\

\

\

**Partie V Techniques d'enseignement**

\

18 Les structurants préalables pour en venir à l'essentiel 109

\

19 Des abstractions qui deviennent concrètes grâce au tiercé des
représentations 115

\

20 Les visualisations pour découvrir l'invisible 125

\

21 Lire avant d'écrire 131

\

**Partie VI Mise en œuvre de l'enseignement**

\

22 Séparer la théorie et la pratique 141

\

23 Distinguer les outils des objets 145

\

24 Les professeurs d'informatique ne peuvent pas tout connaître 151

\

25 Travail sur ordinateur : les mains dans le dos ! 153

\

26 Apprendre à gérer ses erreurs 157
