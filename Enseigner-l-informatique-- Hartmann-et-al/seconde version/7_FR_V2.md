**7**

\

**Adapter le contenu des cours au public visé**

\

C'est aujourd'hui le premier jour de B. à l'École de musique et d'arts
appliqués où il va enseigner les principes de l'Internet. Cela ne
devrait pas lui poser de problème particulier, car le matériel
pédagogique dont il dispose a fait ses preuves à l'IUT.B. va ainsi
présenter les différents services et protocoles de l'Internet. Mais
aujourd'hui, le courant n'a pas vraiment l'air de passer avec la classe.
Même l'analogie avec la bande dessinée sur la construction de la tour de
Babel ne provoque pas les rires habituels. Ces étudiants trouvent-ils
les principes de l'Internet à ce point barbants ?

\

**Problème :** l'enseignement de l'informatique s'adresse à des publics
variés dont les connaissances préalables et les besoins sont différents.
Celui qui enseigne à des publics différents court le risque de commettre
des erreurs dans son choix des thèmes principaux du cours et d'illustrer
ses sujets par des exemples non pertinents pour les étudiants présents.

\

En informatique, les différents publics ont souvent des besoins très
diversifiés. Un bon exemple est la sécurité informatique : un chercheur
étudie de nouveaux algorithmes de cryptage ou la vérification des
caractéristiques de sécurité des protocoles. En école d'ingénieur,
l'accent est surtout mis sur les principes éprouvés de la conception de
systèmes sécurisés. Pour le technicien système, le plus important sera
l'application correcte des mécanismes de sécurité, par exemple la
création d'un certificat de serveur et l'activation du cryptage dans un
serveur Web. Les utilisateurs, par contre, devront surtout être
sensibilisés à la sécurité proprement dite. Ils doivent connaître les
dispositions à prendre pour protéger leur propre ordinateur, avoir
conscience des menaces auxquelles ils sont potentiellement exposés en
naviguant sur la toile et savoir que le simple cryptage de la
communication avec le serveur Web n'offre pas une sécurité suffisante.

\

Les attentes en matière d'enseignement dans la sécurité informatique
sont différentes pour chaque public visé : au niveau universitaire, la
formation devra être pour l'essentiel indépendante du produit et aborder
les concepts et les principes fondamentaux de la discipline. Un
technicien système doit pouvoir mettre en application immédiatement la
formation qu'il a reçue sur le produit, mais la connaissance des
principes lui permet de situer les faits dans un contexte plus large.
Les utilisateurs sont avant tout concernés par la manipulation au
quotidien des outils informatiques. Les analogies sont utiles pour
comprendre les principes de base et la connaissance des produits est
indispensable pour mettre en œuvre des mesures concrètes.

\

Les professeurs d'informatique doivent parfaitement connaître le public
auquel ils s'adressent. Les principales questions à se poser sont : que
doivent apprendre les étudiants ? Pourquoi doivent-ils l'apprendre ? Que
doivent-ils savoir faire concrètement après leur formation ? Quels sont
les exemples concrets rencontrés au quotidien par le public visé ? La
formation devra être conçue et organisée en fonction des réponses à ces
questions.

\

**Solution :** la connaissance préalable des besoins du public visé est
une condition indispensable à un enseignement informatique de qualité.
C'est ce même public visé qui déterminera le niveau d'approfondissement
des connaissances sur les concepts et les produits.
