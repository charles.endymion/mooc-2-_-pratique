**18**


**Les structurants préalables pour en venir à l'essentiel**


« Bonjour à tous. Après avoir pris connaissance des principes
fondamentaux de la programmation pendant les deux premiers jours du
stage, nous allons à présent étudier quelques structures de données
importantes telles que les listes normales, les listes doublement
chaînées, les codes de hachage et les arbres binaires. » Suite à cette
introduction, l'enseignant C. a eu une sensation désagréable. Les
stagiaires n'avaient en fait aucune idée de la signification de ce
jargon.


**Problème :** l'enseignement de l'informatique traite souvent d'un
sujet dont les étudiants n'ont aucune idée. Une entrée en matière
directe avec des termes nouveaux ou des définitions n'est pas une
approche idéale. Les étudiants ne peuvent pas absorber les concepts
nouveaux ni établir la relation avec leurs connaissances préalables.


L'enseignement de l'informatique commence souvent par des termes
techniques compliqués : « nous allons aujourd'hui examiner de plus près
les unités arithmétiques et logiques, en anglais Arithmetic and Logic
Unit ou ALU ». De plus, l'enseignement est souvent structuré de sorte
que l'idée principale n'est communiquée qu'après la présentation d'une
multitude de définitions et de détails. Un structurant préalable (en
anglais Advance Organizer -- AO) peut ici apporter une solution
[Aus60]. Le structurant préalable regroupe les principales idées du
nouveau contenu à enseigner dès le début du cours. Il se base
exclusivement sur des termes et des idées déjà connus, ce qui permet aux
étudiants d'établir le lien entre le nouveau sujet et le savoir
existant. La compréhension et l'organisation sont ainsi facilitées et le
nouveau sujet est mieux intégré. En d'autres termes, un structurant
préalable va chercher les gens là où ils se trouvent. Il n'impose que
très peu de conditions préalables et établit des liens clairs entre ce
qui est connu et le sujet à venir. [Mayer le décrit comme suit
[May79] :]


[unfortunately, it is still not possible to offer a foolproof definition
of what constitutes an advance organizer. A good advance organizer
provides an organized conceptual framework that is meaningful to the
learner, and that allows the learner to relate concepts in the
instructional material to elements of the framework. In the present
studies, good organizers have been concrete models or analogies or
examples, sets of general higher order rules, and discussions of the
main themes in familiar terms. In the present studies, poor organizers
have been specific factual prequestions, summaries, outlines, and
directions to pay attention to specific key facts or terms.
]*(Il n'est malheureusement pas encore possible de
proposer une parfaite définition de ce qui constitue un structurant
préalable. Un structurant préalable bien conçu définit un cadre
conceptuel organisé qui a un sens pour l'étudiant et qui lui permet
d'établir un lien entre les concepts dans le matériel didactique et les
éléments du cadre. D'après les études actuelles, les structurants qui
peuvent être considérés bien conçus ont été des modèles concrets, des
analogies ou des exemples, des ensembles de règles générales d'ordre
supérieur et des discussions en termes familiers sur les thèmes
principaux. Toujours d'après les études actuelles, les structurants mal
conçus ont été des questions préalables factuelles spécifiques, des
résumés, des ébauches et des instructions demandant d'accorder une
attention particulière à certains faits ou termes spécifiques.)*


Le structurant préalable (ou épitome) est une technique d'enseignement
bien documentée dans la pédagogie générale et dont l'effet positif est
confirmé. Le structurant préalable s'avère particulièrement utile en
informatique où de nombreux nouveaux développements sont élaborés à
partir de connaissances et de principes existants à la base desquels ils
peuvent être expliqués. Un structurant préalable s'impose en outre
d'autant plus que le thème est abstrait. Les structurants préalables qui
établissent une analogie avec des sujets familiers de la vie quotidienne
s'avèrent particulièrement efficaces dans l'inhibition des craintes
éprouvées face à certains thèmes de l'informatique : « en fait, ce n'est
pas plus compliqué que d'enfourner une tarte, de jouer à la belote ou
encore de poster une lettre. »


**Solution :** les structurants préalables expliquent les idées
essentielles du nouveau sujet à traiter en faisant appel à des concepts
et des termes connus, favorisant ainsi le lien avec les connaissances
existantes. La compréhension par les étudiants s'en trouve améliorée et
ces derniers retiennent mieux.


**Exemple 1 : routage**


Un structurant préalable pour un cours de deux heures ayant pour thèmes
l'adressage et le routage sur Internet dans un lycée d'enseignement
technique pourrait se présenter ainsi :


lorsque vous surfez sur le Web, à chaque clic sur un lien, votre
navigateur envoie une requête à un serveur Web. Le serveur répond alors
en envoyant la page souhaitée. Du point de vue de l'utilisateur, ce
processus a l'air très simple. Que se passe-t-il alors dans les
coulisses ? Comment les paquets de données trouvent-ils leur chemin vers
le serveur et en reviennent-ils ? Ce sont ces questions que nous allons
traiter aujourd'hui.


En fait, cela fonctionne comme le courrier postal auquel vous êtes
habitués. Imaginons que vous déposiez dans une boîte aux lettres de la
rue du Louvre à Paris une carte postale adressée à un ami à Bienne, en
Suisse. Que se passe-t-il alors ? La carte postale se retrouve tout
d'abord au centre de tri local du 1er arrondissement de Paris. La règle
appliquée ici est que tout courrier qui n'est pas destiné au 1er
arrondissement est envoyé au centre régional de tri de Gonesse, ce qui
est le cas de notre carte postale. Il existe là aussi une règle selon
laquelle tout ce qui n'est pas destiné à la France mais à l'Europe est
envoyé au train postal.


Notre carte postale parvient ainsi par voie ferrée jusqu'au centre de
tri postal de Berne puis est acheminée au bureau de poste de Bienne.
Elle sera ici confiée au préposé au courrier chargé du quartier
correspondant à la rue indiquée sur la carte postale, et celui-ci
viendra la déposer dans la boîte aux lettres portant le numéro figurant
au début de l'adresse.


Quelles sont les points essentiels de ce mécanisme ? Les bureaux de
poste locaux au début et à la fin du trajet doivent connaître
précisément la rue et le nom des personnes. La connaissance de la
direction et de la destination approximatives est par contre suffisante
pour tous les centres intermédiaires. Le centre régional de Gonesse, par
exemple, envoie tout ce qui n'est pas destiné à la France au train
postal. Les employés du centre de tri ne sont pas concernés par les
autres centres qui se trouvent entre la gare d'où part le train postal
et le destinataire du courrier. Chaque courrier est ainsi retransmis au
poste suivant par petits sauts.


Dans le cours qui suit, les étudiants s'intéressent à l'adressage des
ordinateurs au sein du réseau local et à la manière dont les paquets de
données trouvent leur chemin d'un réseau local à l'autre. L'enseignante
établit ici toujours la même analogie avec la carte postale. Lorsque les
étudiants observent concrètement les paramètres du réseau sur un
ordinateur, par exemple, la passerelle par défaut du réseau correspond
alors au bureau de poste local de notre carte postale.


**Exemple 2 : fonctionnement des moteurs de recherche**


Un structurant préalable pour un cours de deux heures ayant pour thèmes
les robots d'indexation et l'indexage dans un moteur de recherche dans
un lycée d'enseignement technique pourrait se présenter ainsi :


vous utilisez presque quotidiennement des moteurs de recherche Internet,
mais ces outils sont étonnamment efficaces. Ils sont capables de
délivrer en quelques fractions de secondes des résultats à partir de
milliards de pages Web. Après ces deux heures, nous connaîtrons le
secret de la rapidité de ces moteurs de recherche.


Je peux toutefois vous en décrire les grandes lignes en une minute :
imaginez un ouvrage technique de 900 pages consacré aux réseaux
informatiques, par exemple. Vous voulez maintenant savoir ce qu'est le
MPLS. Comment procédez-vous ? Vous consultez bien évidemment l'index
alphabétique contenant tous les termes importants avec les numéros des
pages où ils sont traités. Grâce à l'index, il est inutile de parcourir
la totalité du livre, il vous suffit de consulter quelques pages
indiquées en regard du terme recherché. Le gain de temps est
considérable.


Le mystère commence donc à s'éclaircir. L'index est en fait une
structure de données dans laquelle le contenu de l'ouvrage a été
prétraité pour accélérer la recherche d'après des mots-clés. Un moteur
de recherche fonctionne de la même façon : il crée un gigantesque index
des termes trouvés dans les pages Web et, pour chaque terme, note les
pages sur lesquelles il apparaît.


Les élèves ont ensuite été initiés au fonctionnement du robot
d'indexation, des composants d'indexation et de l'index. L'analogie avec
l'index dans un livre a été régulièrement rappelée : les ordinateurs
permettent aux moteurs de recherche de combiner entre elles différentes
listes de termes, c'est la raison pour laquelle une requête peut
contenir plusieurs mots-clés. Cette approche n'est pas possible
directement dans un livre, il faudrait combiner manuellement les listes
de numéros de page correspondant à plusieurs termes. L'index d'un livre
a cependant pour avantage d'avoir été établi manuellement. Une page
apparaît également dans l'index lorsqu'elle contient uniquement
l'expression « Multiprotocol Label Switching » au lieu de l'acronyme
MPLS, une opération impossible à réaliser pour la majorité des moteurs
de recherches car elle imposerait un traitement linguistique
supplémentaire. La présentation du cours ébauchée ci-dessus est
assurément plus compréhensible pour les étudiants qu'une introduction
telle que « nous allons aujourd'hui parler du principe de fonctionnement
des moteurs de recherche sur Internet. Concrètement, nous allons nous
intéresser à la recherche de document sur le Web à l'aide de robots
d'indexation de type Spider ou Crawler, puis l'indexation du texte dans
les documents trouvés et finalement l'enregistrement des mots-clés dans
un index, la structure de données centrale d'un moteur de recherche.


La représentation des documents que nous obtiendrons ainsi est
généralement appelée vectorisation descriptive des documents. »


**Exemple 3 : bases de données relationnelles**


Les deux derniers structurants préalables utilisent des analogies avec
la vie quotidienne pour faciliter la compréhension. Cette approche est
utile, mais en aucun cas obligatoire. Un structurant préalable peut très
bien également faire appel aux connaissances informatiques existantes.
Une unité de cours d'initiation aux bases de données relationnelles, par
exemple, pourrait illustrer les idées essentielles en se servant de
l'organisation d'une pension pour chats. Toutes les informations
nécessaires sont ici gérées à l'aide d'un tableur. Une page du tableur
contient tous les clients (Fig. 18.1) et l'autre les chats (Fig. 18.2).


Les principes fondamentaux des bases de données relationnelles sont les
suivants : les données sont gérées sous forme d'enregistrements dans des
tableaux. Un enregistrement se compose d'une série d'attributs. Certains
attributs sont définis comme attributs-clés et interviennent dans
l'identification des enregistrements ou dans la combinaison des
tableaux. Un langage de requête permet d'interroger les informations
enregistrées dans les tableaux.


L'exemple de la pension pour chats permet de présenter et d'expliquer
très facilement ces principes. Même un langage de requête tel que le SQL
peut être examiné de plus près sans détails techniques. Le propriétaire
de la pension dit à sa fille : « écoute, j'ai besoin de connaître les
noms de toutes les personnes qui amèneront leurs chats le 10 avril. Je
veux les prévenir que je serai absent l'après-midi pour qu'elles
puissent s'organiser en conséquence. » Dans un premier temps, la fille
recherche tous les enregistrements du tableau Katzen (Chats) dont la
colonne "von" (de) contient 10. April et note le chiffre qui figure
dans la colonne Besitzer (Maître). À l'aide de ce chiffre, elle
recherche alors le nom du maître correspondant dans le tableau Kunden
(Clients) et le note. L'enseignant pourra revenir à cet exemple plus
tard dans le cours et montrer la requête SQL correspondante :


SELECT Kunden.Name FROM Kunden, Katzen  
WHERE Katzen.von = '10. April' AND Katzen.Besitzer = Kunden.Nr


<img alt="Fig. 18.1 " src="Images/18.1_figure.jpg" width="450px"/>


**Fig. 18.1.** Tableau des clients


<img alt="Fig. 18.1 " src="Images/18.2_figure.jpg" width="450px"/>

**Fig. 18.2.** Tableau des chats

