**16**


**Apprentissage par la découverte**


L'enseignant en informatique S. raconte à ses collègues qu'elle voulait
faire découvrir à ses étudiants l'étude d'un projet dans une
infrastructure d'application Web. Elle poursuit, non sans une certaine
frustration, en affirmant qu'ils avaient même du mal à se familiariser
avec l'infrastructure elle-même : « j'ai cru que les étudiants
pourraient s'en sortir par eux-mêmes à l'aide de deux manuels et des
nombreux matériels disponibles sur Internet. Même si nous avons
finalement utilisé pendant le cours une infrastructure semblable à
l'idée de départ, ils l'ont jugée très difficile à comprendre ! Les
aspects à prendre en considération sont en fait beaucoup trop nombreux
pour qu'ils puissent les étudier chacun en détail, ils se voient donc
contraints de se concentrer sur l'essentiel. Il s'agit là d'un vrai
problème. J'étais persuadée qu'ils en seraient capables après mon
cours ! »


**Problème :** l'enseignement de l'informatique se caractérise souvent
par la transmission d'une théorie suivie d'exercices pratiques. Des
aspects importants tels que le travail autonome, la créativité et la
réflexion critique sont peu pris en compte ici. L'aptitude à savoir
découvrir soi-même de nouveaux thèmes joue cependant un rôle essentiel
dans le quotidien professionnel.


Le caractère abstrait de l'informatique constitue un défi particulier
pour l'enseignement : les élèves doivent faire preuve d'une certaine
capacité d'abstraction, en particulier l'aptitude à observer les choses
simultanément sur différents niveaux d'abstraction. Une autre difficulté
réside dans le fait qu'il existe de nombreux aspects que les élèves ne
peuvent pas découvrir par eux-mêmes. Exemples : les algorithmes sur des
thèmes tels que le tri efficace (avec Quicksort ou Heapsort), les
procédures de chiffrement (par exemple RSA) ou les procédures d'échange
sécurisé de clés sur des canaux non sécurisés (Diffie-Hellman).
L'enseignement de l'informatique se caractérise ainsi souvent par la
transmission d'une théorie suivie d'exercices pratiques.


La situation décrite dans l'enseignement est en totale contradiction
avec le quotidien des informaticiens qui inclut la découverte en toute
autonomie de nouveaux contenus. Il faut donc encourager cette compétence
méthodologique dès la formation. [Seymour Papert note avec pertinence :
« you can't teach people everything they need to know. The best you can
do is position them where they can find what they need to know when they
need to know it. » ]{lang="en-GB"}*(Vous ne pouvez pas enseigner aux
gens tout ce qu'ils ont besoin de savoir. La meilleure chose à faire
consiste à les amener dans une position depuis laquelle ils pourront
découvrir ce qu'ils ont besoin de savoir au moment où ils auront besoin
de le savoir.)*


Les situations d'enseignement ouvertes permettent aux élèves de
développer activement par eux-mêmes de nouveaux contenus.
L'apprentissage par la découverte est une méthode d'enseignement
appropriée. Il encourage la créativité, l'autonomie, la pensée critique
et l'échange mutuel. L'apprentissage par la découverte autorise un haut
niveau de personnalisation, ce qui permet à l'enseignant de mieux
répondre aux besoins individuels des élèves.


L'idée fondamentale de l'apprentissage par la découverte est la
suivante : les étudiants acquièrent de nouvelles connaissances en
faisant des expériences personnelles et en remettant les choses en
question. Ils développent leurs propres théories (parfois naïves) par
l'étonnement, l'émerveillement et le doute  ; ils se voient ainsi
parfois contraints d'abandonner de vieilles idées et de rejeter des
hypothèses précédemment établies. Pour un apprentissage réussi, il est
souvent plus efficace de laisser les élèves produire activement leurs
propres explications des phénomènes observés plutôt que de leur
présenter des formules mnémotechniques ou des théories qu'ils devront
apprendre par cœur.

L'apprentissage par la découverte nécessite du temps et de l'espace
libre. Il n'existe à priori rien de vrai ou de faux dans la recherche du
nouveau, le plus important est que toutes les connaissances soient
retenues et organisées. La documentation et la présentation des
résultats aident à organiser ses réflexions et sont un moyen important
de développement et d'observation critique de ses propres idées.


**Choix du thème et préparation à l'apprentissage par la découverte**


La réussite de l'apprentissage par la découverte dépend du thème choisi
et de la présentation des tâches. La sélection d'un contenu approprié
relève de la responsabilité de l'enseignant, tout comme l'élaboration
des instructions relatives à l'apprentissage par la découverte. Il
existe trois impératifs essentiels (basés sur \[FFE04\]) :


**ouverture du thème** Le domaine doit posséder une certaine ouverture.
Apprentissage par la découverte, cela veut dire aller à la découverte
explorative d'un sujet, poser des hypothèses, les vérifier et les
échanger avec d'autres élèves. La résolution d'une tâche prédéfinie
n'est pas de l'apprentissage par la découverte. L'objet de
l'enseignement doit comporter de multiples facettes, inclure différents
aspects et doit pouvoir être découvert en suivant différentes voies. La
norme de virgule flottante IEEE 754 ou l'algorithme de tri Quicksort,
par exemple, ne sont pas des sujets à découvrir. Les étudiants peuvent
toutefois se faire une idée de la manière de représenter des nombres à
virgule flottante dans le système binaire ou imaginer leur propre
méthode de tri. L'ouverture d'une tâche peut essentiellement se
présenter sous deux formes : la forme la plus simple où il existe
plusieurs solutions possibles pour un problème et la forme la plus
ouverte où les étudiants définissent eux-mêmes l'aspect à explorer d'un
sujet.



**matériel complet** Le matériel nécessaire est mis à disposition par
l'enseignant et préparé afin que tous les étudiants puissent comprendre
et traiter l'information. Aucune assistance par l'enseignant
d'informatique ne devrait être nécessaire ici. L'apprentissage par la
découverte consiste à développer des choses nouvelles en toute autonomie
en se basant sur le matériel fourni et les connaissances propres.
L'objectif est de développer des idées propres et non de filtrer et de
structurer certaines informations, par exemple la syntaxe correcte d'une
instruction complexe dans un langage de programmation donné à partir
d'un précis en dix volumes. Cette collecte autonome d'informations peut
très bien constituer un objectif en soi, mais elle déborde très vite des
limites de la salle de cours et devrait avoir lieu dans un cadre
temporel plus vaste.


**tâche et évaluation** Dans l'apprentissage par la découverte, les
tâches à explorer doivent être conçues afin d'autoriser des solutions,
des approches et des perspectives différentes. Tout comme le thème,
elles doivent offrir une certaine ouverture pour que les étudiants
disposent de la liberté nécessaire pour faire leurs propres découvertes.
Il est important dans l'évaluation des contributions que toutes les
propositions et idées soient prises au sérieux. L'enseignant ne doit pas
intervenir immédiatement en présence de solutions incomplètes, un
certain degré de « laisser faire » est nécessaire.


**Solution :** l'aptitude au travail autonome peut être stimulée
volontairement dans l'enseignement par l'utilisation de différentes
méthodes pédagogiques. L'apprentissage par la découverte est l'une de
ces méthodes et contribue à une personnalisation de l'enseignement.


**Exemple 1 : conception de sites Web destinés aux malvoyants**


Comment une personne aveugle « voit-elle » un site Web ? À quoi faut-il
être attentif en créant des sites Web adaptés aux malvoyants ? Les
aveugles et les malvoyants peuvent-ils réellement faire usage de la
multitude d'informations disponibles sur Internet ou sont-ils
désavantagés ? De quelles aides techniques disposent actuellement les
aveugles ? Quel est leur rôle dans la société d'information ? Tant de
questions qui surgissent dès que l'on endosse soi-même le rôle d'un
aveugle en se bandant les yeux et en essayant de naviguer sur la toile
avec un programme de lecture de pages Web approprié. Cette méthode peut
être appliquée dans une formation sur la publication de sites Web, par
exemple.


Ce thème présente l'ouverture requise, car il est possible d'y découvrir
un éventail d'aspects très large \[Swi\]. Il y a ainsi des aspects
pratiques tels que « Il n'est pas possible de faire lire à haute voix
une page d'accueil sans texte » (Fig. 16.1), par exemple, mais des
détails techniques tels que les problèmes posés par les animations Flash
ou les cadres peuvent eux aussi s'avérer intéressants. Les élèves
établiront-ils peut-être la maxime de la conception selon laquelle un
site Web ne peut être considéré comme un site Web de qualité que
lorsqu'il est également accessible aux aveugles et aux malvoyants. Ou
alors ils jetteront un pont vers les moteurs de recherche qui, eux
aussi, « voient » les sites comme des aveugles. Des connaissances
importantes pour l'optimisation des sites Web pour les moteurs de
recherches pourront être dérivées de ces travaux. Toutefois, ce thème
peut également être considéré dans un contexte social plus large : la
loi oblige les sites des institutions gouvernementales à être
accessibles aux personnes souffrant d'une déficience visuelle. Ces
directives sont-elles respectées ? Ppeuvent-elles réellement être mises
en application ?


**\<script** language=\"*JavaScript*\" type=\"*text/javascript*\"**\>**  
document.writeln(\'\<a href=\"/info.html?sub\"\>Informations\</a\>\<br\>\<img src=\"bild.gif\"\>\<br\>\');  
document.writeln(\'\<a href=\"/weiter.html\"\>La suite ici\</a\>\<br\>\<img src=\"logo.gif\"\>\<br\>\');  
function preload(imgObj, imgSrc) {  
eval(imgObj + \' = new Image()\')  
eval(imgObj + \'.src = \"\' + imgSrc + \'\"\')}  
**\</script\>**  
**\<a** href="index2.html">**<img** name="Bildchen" border="0" src="Pfeil.jpg">**</a**>  
**\<img** border=\"*0*\" src=\"*DSC002005.jpg*\"**\>** 

**Fig. 16.1.** Que doit lire à haute voix un programme de lecture de
page Web ?


**Exemple 2 : un tour de cartes pour l'étude des mathématiques
discrètes**


De nombreux tours de cartes sont basés sur des faits mathématiques
utilisés astucieusement, principalement dans le domaine du calcul
stochastique ou des mathématiques discrètes. Qu'en serait-il d'une
analyse mathématique approfondie qui se baserait sur des tours de
cartes ? Une approche systématique nous permettrait-elle de trouver le
« truc » des tours de cartes les plus fascinants ? Le tour de cartes
marche-t-il à tous les coups ou seulement de manière aléatoire dans de
nombreux cas ? Pouvons-nous modifier ou étendre le tour de cartes ou
trouver nous-mêmes de nouveaux tours ?


Les étudiants se familiarisent avec les mathématiques discrètes à l'aide
d'un tour de cartes : l'enseignant commence par montrer un tour avec une
élève qui y a préalablement été initiée. Les autres élèves spéculent
alors sur le fonctionnement du tour et consignent leurs hypothèses par
écrit. Ils peuvent jouer les rôles du magicien et de son assistant à
l'aide d'une simulation sur ordinateur et, par des configurations de
cartes qu'ils auront eux-mêmes définies, tenter de découvrir le « truc »
du tour. Après cette phase de découverte initiale, tous les élèves
reçoivent des instructions sur le tour de cartes, s'entraînent à le
réaliser à l'aide d'une simulation sur ordinateur et peuvent ensuite
vérifier leurs hypothèses au moyen de leurs propres configurations de
cartes. Plusieurs suites sont ensuite possibles : pourquoi le tour
fonctionne-il ? Fonctionnerait-il aussi avec un nombre de cartes plus
grand ou plus petit ? Les spectateurs peuvent-ils choisir six cartes au
lieu de cinq ?


Cet exemple part d'un problème clairement défini : les élèves doivent
tout d'abord s'intéresser au tour de cartes proprement dit et au moins
en comprendre le fonctionnement. Différents scénarios sont cependant
possibles pour la suite : on peut essayer d'analyser précisément le
contexte mathématique du tour ou même apporter une preuve qu\'il
fonctionne toujours. Différentes variantes du tour peuvent être
élaborées et le tour présenté peut être utilisé pour mener une réflexion
plus large sur ce qui fait qu'un tour de carte est attrayant et réussi.
Le plus important est qu'aucune limite ne soit fixée à la créativité et
à l'imagination dans la recherche de connaissances propres.


<img alt="Fig. 16.2. Vue de l'écran de CardGame" src="Images/16.1_figure.jpg" width="450px"/>


**Fig. 16.2** Vue de l'écran de CardGame

Le programme CardGame est utilisé pour analyser le tour de cartes (Fig.
16.2; \[Swi\]). Dans ce programme, les élèves peuvent jouer le rôle du
magicien ou de son assistant. Les cartes sont choisies au hasard au
début d'une partie, chaque carte pouvant être remplacée en particulier.
Les élèves peuvent ainsi découvrir comment les cartes doivent être
choisies pour le codage et la façon dont elles doivent être interprétées
pour le décodage.


**Exemple 3 : découverte des algorithmes de la théorie des graphes**


Cet exemple est destiné aux lycées et aux instituts universitaires de
technologie où sont enseignés des thèmes choisis de l'informatique
théorique. Les étudiants découvrent des problèmes NP-complets et des
algorithmes de solution à l'aide du logiciel \[Swi\] : en fonction de
leurs intérêts, ils examinent les aspects de la durée, les cas extrêmes,
l'exactitude, les applications dans le monde réel ou élaborent même
leurs propres algorithmes. Les problèmes posés sont issus de la théorie
des graphes : aptitude du graphe à la colorisation, couverture de
sommet, clique, voyageur de commerce, aptitude à satisfaire des
fonctions logiques et problèmes similaires.


L'enseignement de l'informatique présente ici généralement les
algorithmes et, au niveau universitaire, traite en plus de la réduction
d'autres problèmes NP-complets à ce problème. Avec cette approche, les
étudiants doivent tout d'abord comprendre les problèmes tels qu'ils sont
posés, par exemple la définition d'une couverture de sommet dans un
graphe. En l'absence d'une compréhension intuitive du problème, il
arrive très fréquemment que les algorithmes de résolution possibles
demeurent dans le domaine du nébuleux et la réduction d'un problème à un
autre devient une affaire purement formelle, sans révélations profondes.


L'environnement d'apprentissage GraphBench permet aux étudiants
d'explorer toutes les instances d'un problème et d'arriver ainsi à une
perception intuitive de la nature des problèmes. Ils peuvent ensuite
découvrir différents algorithmes de résolution en observant la séquence
animée. GraphBench permet en outre de suivre en temps réel la réduction
d'un problème à un autre. Une modification de la configuration du graphe
avec le problème de la couverture de sommet peut être observée
parallèlement dans l'instance de problème correspondant d'un circuit
hamiltonien. Les réductions abstraites deviennent ainsi nettement plus
tangibles.


Un environnement d'apprentissage tel que GraphBench permet de multiples
découvertes et l'orientation volontaire par l'enseignant dans une
direction donnée dépendra des objectifs et des connaissances préalables.
Chaque élève ne doit-il étudier qu'un seul problème ou l'ensemble de la
classe doit-elle apprendre à connaître toute une série de problèmes
NP-complets ? Faut-il mettre en avant les aspects algorithmiques ou
plutôt traiter des réductions ? Ou faut-il plutôt aborder les questions
de la calculabilité ? Les élèves peuvent approfondir certaines
questions, par exemple : Comment fonctionnent les algorithmes ? Puis-je
trouver moi-même un algorithme qui résout le problème ? Combien d'étapes
sont-elles nécessaires dans l'algorithme jusqu'à ce qu'une solution soit
trouvée ? Quel est l'algorithme le plus rapide ? Existe-t-il des
applications pratiques de ce problème ? Existe-t-il des limites dans
l'utilisation pratique des algorithmes de résolution ?


Le logiciel GraphBench est un outil permettant aux élèves d'explorer les
différents problèmes de graphe, de générer des instances de problème
mais aussi d'observer les algorithmes de résolution et les réductions de
problèmes animés. GraphBench contient en outre un environnement de
programmation pour la mise en œuvre de leurs propres algorithmes de
résolution.


<img alt="Fig. 16.3.** Vue de l'écran de GraphBench : problème d'aptitude à la colorisation des graphe " src="Images/16.2_figure.jpg" width="450px"/>

**Fig. 16.3** Vue de l'écran de GraphBench : problème d'aptitude à la
colorisation des graphes


