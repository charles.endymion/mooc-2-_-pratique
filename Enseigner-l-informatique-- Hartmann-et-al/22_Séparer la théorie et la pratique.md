**22**


**Séparer la théorie et la pratique**


Un cours dans la salle d'informatique : l'enseignant explique une
situation et donne des instructions. L'application se bloque sur l'un
des ordinateurs, il est impossible de la faire redémarrer. De plus, un
élève n'a pas enregistré ses données parce qu'il a cliqué sur le mauvais
bouton en répondant au message de sécurité « Voulez-vous enregistrer les
modifications ? ». D'autres élèves ont perdu le fil du cours et, par
ennui, surfent sur le net ou dialoguent avec des amis et, depuis presque
un quart d'heure, une élève essaie désespérément de trouver l'erreur
qu'elle a pu commettre.


**Problème :** chaque élève a son propre rythme de travail sur
ordinateur, ce qui peut donner lieu à toutes sortes de difficultés. Il
est donc très difficile de fournir des explications sur le sujet étudié
ou des instructions sur l'utilisation de l'ordinateur pendant que la
classe est en train d'y travailler.


La plupart des cours d'informatique se déroule dans la salle
d'informatique, lieu où sont à la fois communiqués des éléments
conceptuels et réalisés des exercices. Dans les écoles, une partie de la
théorie peut éventuellement être transmise dans la salle de classe
habituelle avant de passer aux exercices pratiques dans la salle
d'informatique. Quand tous les élèves possèdent leur propre ordinateur
portable, seule une salle de travaux pratiques est alors nécessaire.


Ces circonstances externes font que l'étude de la théorie et les
exercices pratiques ont souvent lieu dans la même salle, avec pour
conséquence une alternance rapide entre la théorie et la pratique, ce
qui peut parfois poser problème. Premièrement, chaque élève a son propre
rythme de travail sur ordinateur, les problèmes ne se produisent donc
pas en même temps. Il n'est par conséquent pas très utile que
l'enseignant donne à toute la classe des informations complémentaires
sur le fonctionnement du programme. Deuxièmement, l'utilisation de
l'ordinateur demande des efforts aux étudiants qui détournent alors leur
attention du contenu pédagogique proprement dit. De plus, une
interruption du travail sur ordinateur par des instructions énoncées par
l'enseignant entraîne un risque de surmenage  ; de nombreux étudiants,
absorbés par de multiples problèmes, ignorent purement et simplement les
consignes. Troisièmement, il y a mélange de deux plans : les concepts et
la mise en application de ces concepts dans les programmes qu'utilisent
les élèves.


D'autres matières enseignées telles que la chimie font également appel à
des outils et à des expériences pratiques, mais la communication de la
théorie et l'expérimentation en laboratoire sont clairement séparées.
Une séparation entre théorie et pratique s'impose aussi dans
l'enseignement de l'informatique :


**séparation dans l'espace** Dans l'idéal, il faudrait disposer de deux
salles : la salle des cours théoriques et une salle de travaux pratiques
avec des ordinateurs. S'il n'existe qu'une seule salle mais qu'elle est
assez grande, elle peut alors être divisée (Fig. 22.1) : les places
normales à l'avant et au centre, les postes équipés d'un ordinateur sur
l'extérieur. L'enseignant est ainsi moins tenté de mélanger la théorie
et la pratique, les étudiants sont alors moins distraits par
l'ordinateur pendant les cours théoriques.


<img alt="Fig. 22" src="Images/22_figure.jpg" width="450px"/>


**Fig. 22.1.** Séparation dans l'espace entre la théorie et la pratique


La séparation décrite est bien évidemment moins justifiée dans les
classes où chaque élève possède son propre ordinateur portable.
Cependant, c'est justement dans ces classes qu'il est essentiel de bien
séparer la partie pendant laquelle est transmise la théorie de la partie
travail sur ordinateur.


**séparation dans le temps** L'enseignant ne s'adresse à l'ensemble de
la classe que pendant le cours théorique. Pendant le travail sur
ordinateur, il s'adresse seulement aux élèves de manière individuelle.
Tout le monde peut ainsi travailler à son propre rythme et personne
n'est dérangé par des interruptions.


**distribution du matériel pédagogique** Pendant la partie théorique,
les élèves peuvent utiliser un script ou un manuel. Ces matériels
doivent être indépendants de tout produit et ainsi durables. Les
exercices font appel à des fiches de travaux pratiques ou à des pages
Web qui doivent être adaptées au matériel et aux logiciels utilisés.
Grâce à cette séparation de la théorie et de la pratique, les
différentes parties de la préparation du cours sont plus facilement
réutilisables ultérieurement, un aspect particulièrement important dans
l'enseignement de l'informatique où les enseignants doivent pouvoir
réutiliser plusieurs fois certaines parties de leur cours. L'algorithme
Quicksort, par exemple, peut ainsi être présenté exactement de la même
manière qu'il y a 20 ans, tout comme le concept des adresses absolues et
relatives dans une feuille de calcul qui n'a pas non plus changé.


La séparation de la théorie et de la pratique est justifiée même dans
les cours d'application dont l'objectif essentiel est d'acquérir des
connaissances à travers un logiciel concret. Exemple avec le traitement
de texte : les feuilles de style ont été traitées dans la partie
théorique, puis approfondies à l'aide d'exemples concrets au cours des
exercices pratiques. Les stagiaires pourront ainsi s'adapter plus
facilement lorsqu'ils se retrouveront plus tard devant un autre
traitement de texte. Les cours axés sur l'acquisition rapide de
compétences par la méthode dite « Drill & Practice » ne sont efficaces
qu'à court terme.


**Solution :** dans l'enseignement de l'informatique, la théorie et la
pratique peuvent être séparées sous trois aspects : dans l'espace, par
un aménagement approprié de la salle de classe, dans le temps, par une
division claire de l'emploi du temps en une partie de cours théorique et
des exercices pratiques sur ordinateur, et en contenu, par des documents
distincts pour la partie théorique indépendante du produit et la partie
pratique dépendante du produit.
