**11**


**Les cours d'informatique doivent être soigneusement planifiés**


B. est maître de conférence dans un institut universitaire de
technologie, il est spécialisé dans les technologies Internet. Il
souhaite également communiquer ces connaissances dans le cadre de la
formation continue et anime ainsi des stages sur le fonctionnement,
l'installation et la configuration du serveur Web Apache. Toutefois,
alors que la journée de formation se termine, B. constate non sans une
certaine frustration qu'il a complètement sous-estimé le temps
nécessaire : seuls les deux tiers du programme prévu ont été traités et,
pour couronner le tout, le serveur a planté pendant que les stagiaires
étaient en train de réaliser un exercice important. Les avis exprimés
par les stagiaires à l'issue du cours étaient de ce fait relativement
négatifs  ; B. se demande alors quels changements il devra apporter la
prochaine fois.


**Problème :** de nombreux professeurs et maîtres de conférences en
informatique enseignent non seulement dans les établissements auxquels
ils sont rattachés, mais animent également des stages à l'extérieur,
stages dont les conditions générales différentes imposent une autre
approche que l'enseignement pur qui, lui, s'étend sur plusieurs
semaines.


Les stages d'une ou deux journées de perfectionnement ou de formation
continue à l'intérieur d'une entreprise ne peuvent être planifiés de la
même manière que l'enseignement pur, qui lui s'étend sur plusieurs
semaines voire une année entière. Les principales différences sont les
suivantes :


**population visée** L'enseignement s'adresse à des étudiants qui,
suivant le niveau, assistent plus ou moins volontairement aux cours. Le
fait d'assister aux cours dans une matière donnée ne résulte
généralement pas d'un intérêt particulier pour cette matière, mais parce
que cette dernière fait partie du programme d'études. Les stages, par
contre, sont suivis par des adultes, souvent de leur propre initiative
parce qu'ils souhaitent se perfectionner. Contrairement aux étudiants,
ils doivent payer le stage ou convaincre leur hiérarchie que cette
formation est indispensable pour leur travail, sans oublier les dossiers
qui s'accumulent souvent à leur poste de travail pendant leur absence.


Les stagiaires en veulent pour leur argent, leurs attentes sont plus
marquées que celles des étudiants ou des élèves. S'il n'existe quasiment
aucun problème de discipline lors des stages, certains stagiaires
peuvent toutefois s'avérer mécontents. Par conséquent, il est primordial
que les stagiaires connaissent précisément à l'avance ce que peut leur
apporter le stage. Une description appropriée du stage voire un test
d'aptitude volontaire préalable peuvent contribuer à des attentes
réalistes en matière de contenu du stage et d'objectifs d'apprentissage.


**programme de formation** L'enseignement dans une école suit un plan
pédagogique qui définit au moins l'orientation générale du programme de
formation sur une longue période. Les contenus sont ici de nature plutôt
générale et leur pertinence est considérée à long terme. Les
participants à un stage de formation, par contre, veulent apprendre des
éléments concrets sur un thème spécifique, si possible qui puissent leur
apporter des avantages directs dans l'exercice de leur profession. Pour
les animateurs de stage, la tentation est alors grande de se concentrer
sur la communication des connaissances des faits concrets, là où les
avantages immédiats semblent les plus évidents. Le fait d'arriver à un
équilibre délicat entre les idées et les concepts fondamentaux d'une
part et la connaissance des produits aux avantages directs d'autre part
peut justement constituer l'un des points forts d'un stage de formation.


**objectifs pédagogiques** Dans l'enseignement traditionnel, les
objectifs pédagogiques sont considérés à long terme et ne sont pas
forcément nécessaires pour chaque cours. Dans les stages de formation,
par contre, la période disponible est clairement délimitée et la
planification d'objectifs pédagogiques détaillés est de ce fait
particulièrement importante. Des objectifs pédagogiques formulés avec
une certaine souplesse peuvent être communiqués dans un descriptif du
stage et définir ainsi les attentes des participants.

**gestion du temps** Lorsqu'un enseignement s'étend sur plusieurs
semaines, ce qui n'a pas pu être traité dans un cours par manque de
temps pourra être poursuivi lors de la prochaine. Si un sujet pose des
problèmes particuliers, l'enseignant peut en tenir compte et ralentir le
rythme. Un stage, par contre, n'a lieu qu'une seule fois et pour une
durée limitée. Si certains sujets mentionnés dans le descriptif n'ont
pas été traités, les stagiaires risquent de manifester leur
mécontentement car ils les ont finalement payés ! Il faut donc envisager
plusieurs variantes lors de la préparation d'un stage de formation :
quels sont les sujets incontournables et quels thèmes pourraient
éventuellement être abrégés ou même carrément ignorés ? L'emploi du
temps présente-t-il suffisamment de marge de manœuvre en cas d'imprévu ?


**connaissances préalables** Une salle de classe est un public
relativement homogène. Dans le cas d'un stage, par contre, il est
souvent difficile de connaître à l'avance le niveau de chacun des
participants dont les connaissances sont généralement très disparates.
Il est donc indispensable que le descriptif du stage décrive avec
précision les connaissances préalables requises et certains organismes
de formation proposent même à cet effet une auto-évaluation en ligne. Le
déroulement du stage devrait dans tous les cas être organisé en prenant
pour hypothèse que l'éventail des niveaux de connaissance sera très
large. Cela implique notamment la nécessité de personnaliser les
exercices pratiques, avec pour objectif que chaque stagiaire obtienne un
résultat positif pour la majorité des tâches à accomplir. Si seuls les
plus rapides sont en mesure d'arriver au bout des exercices, les autres
participants risquent de se sentir frustrés.


**contexte social** Avec le temps, un enseignant apprend à bien
connaître les élèves de sa classe et les élèves se connaissent également
entre eux. Une certaine confiance s'établit et des règles sont définies,
consciemment ou inconsciemment. Cependant, la situation est toute
différente dans le cas d'un stage de formation : ici, le formateur ne
connaît pas les participants et ces derniers connaissent tout au plus
l'un ou l'autre des stagiaires. Dans une formation en entreprise, il
peut parfois exister des liens hiérarchiques dont un formateur n'est pas
nécessairement informé. Il est donc nécessaire de planifier avec soin
l'interaction sociale pendant le stage au cours de la préparation.
Comment doit se dérouler l'introduction du stage ? Que doit dire le
formateur sur lui-même ? Les participants doivent-ils se présenter les
uns aux autres ? Comment seront formés les groupes de travail ? Comment
établir, dans le peu de temps disponible, un climat d'ouverture qui
mettra les participants en confiance pour poser des questions ?


**infrastructure** La défaillance de la lampe du projecteur ou
l'interruption de l'accès Internet peuvent avoir des conséquences
catastrophiques au cours d'un stage. Il est ainsi nécessaire de prévoir,
lors de la préparation d'un stage, la manière dont faire face à un
problème technique soudain. Y a-t-il un autre projecteur ? Un ordinateur
de rechange ? Les diaporamas sont-ils bien présents sur l'ordinateur
portable du formateur ? Que faire en cas d'interruption de l'accès
Internet ou de défaillance de l'ordinateur prévu pour les exercices
pratiques ? En exagérant un peu, la solution pourrait se résumer à la
formule d'Andy Grove (PDG d'Intel de 1987 à 1998) : « Seuls Les
Paranoïaques Survivent ».


**Solution :** les animateurs et formateurs de stages d'informatique
doivent tenir compte des spécificités des stages de formation. Les
objectifs pédagogiques et l'emploi du temps doivent être soigneusement
préparés et les exercices pratiques sur ordinateur personnalisés. Il est
indispensable d'être très exigeant envers la disponibilité de
l'infrastructure.
