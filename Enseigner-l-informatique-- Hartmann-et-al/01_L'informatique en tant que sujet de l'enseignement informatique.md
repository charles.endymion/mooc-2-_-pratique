**1**

**L'informatique en tant que sujet de l'enseignement informatique**

« Généralisation de l'enseignement informatique ! ». Cette annonce parue dans la presse agace au plus haut point N., professeur enseignant au lycée. À peine les établissements scolaires du département ont-ils été équipés d'ordinateurs performants par le Conseil Général, de sorte que le traitement de texte, la recherche sur Internet et l'usage de quelques logiciels sont passés dans les mœurs et couramment utilisés par les enseignants que l'on annonce à grands cris l'instauration de « l'enseignement informatique ». Tant qu'à faire, il suffirait qu'un professeur de musique utilise un simple logiciel de création de partitions et éventuellement un programme d'apprentissage assisté par ordinateur pour pouvoir prétendre que l'informatique est intégrée dans son enseignement. Comment peut-on, dans ces circonstances, persuader les responsables du caractère indispensable de l'enseignement informatique dans un lycée moderne ?

**Problème :** L'expression « enseignement informatique » englobe souvent différents aspects de l'informatique. L'enseignement « informatique » (ou de l'informatique) se retrouve en fait de l'école primaire à l'université. Cette confusion des termes rend plus difficile une analyse factuelle dont le sujet est l'ordinateur et l'école.

Les technologies de l'information et de la communication (TIC) jouent différents rôles dans le domaine de la formation et de l'enseignement.
Il est donc primordial de clairement différencier ces rôles pour pouvoir procéder à un examen approfondi de l'informatique, de l'enseignement et de la formation. Nous allons distinguer ci-après cinq rôles différents des TIC dans la formation.

**Les TIC comme outil au quotidien** Des outils tels qu'un traitement de texte, un tableur, un programme graphique ou un navigateur Internet font aujourd'hui partie du quotidien professionnel et sont tout aussi omniprésents dans la sphère privée. Aucune connaissance informatique particulière, par exemple des notions de programmation, n'est requise pour pouvoir utiliser ces outils, mais la compréhension des principes fondamentaux de l'informatique est indispensable pour pouvoir s'en servir efficacement. Cette compréhension fait défaut à de nombreuses utilisatrices et de nombreux utilisateurs. Ce n'est donc pas par hasard que l'on estime qu'une part non négligeable du coût total d'un équipement informatique (TCO, Total Cost of Ownership -- Coût total de possession) résulte d'une formation insuffisante des utilisatrices et des utilisateurs. De nombreux établissements scolaires essaient d'intégrer cette formation informatique fondamentale dans l'enseignement des autres matières et de la faire prodiguer par des enseignants de toutes les disciplines, mais sans enseignement approfondi de l'informatique proprement dite.

TIC dans l'enseignement

Matière enseignée

Support

Logiciel d'apprentissage

e-Learning

Outil

au quotidien

dans l'enseignement spécialisé

**Fig. 1.1.** Les différents rôles de l'ordinateur dans la formation


**Les TIC comme outil dans l'enseignement spécialisé**
L'enseignement fait également appel à des outils qui sont spécifiques au domaine de spécialité : citons, par exemple, des progiciels de calcul formel en mathématique, des logiciels de modélisation moléculaire en chimie, des programmes statistiques ou des logiciels SIG en géographie ou encore des programmes de traitement d'image dans la création visuelle. Ces outils sont plus complexes que les programmes courants et leur utilisation plus exigeante. Il faut, d'une part, posséder des connaissances approfondies des principes fondamentaux de la spécialité concernée (il est vrai qu'un logiciel de mathématique peut éviter des calculs complexes, mais il ne saura pas choisir la stratégie de résolution à adopter et encore moins interpréter les résultats) et, d'autre part, se familiariser longuement avec l'usage des outils spécialisés, qui sont souvent des systèmes informatiques relativement élaborés.

**Logiciels d'apprentissage** Les possibilités multimédia de l'ordinateur sont utilisées dans l'enseignement en assistance au processus d'apprentissage, depuis le simple exercice de vocabulaire jusqu'aux environnements d'apprentissage interactifs, par exemple pour la simulation des relations économiques. L'aspect essentiel des logiciels d'apprentissage est l'interaction entre l'homme et la machine : l'ordinateur remplit le rôle de l'enseignant et interagit avec les étudiants.

**e-Learning** Les nouvelles possibilités de communication basées sur
l'Internet ont radicalement modifié la communication autour de
l'enseignement : l'enseignement en ligne, l'e-Learning, l'apprentissage
mixte, etc. rendent l'enseignement indépendant du lieu et du temps et
permettent en outre de modifier durablement l'efficacité du processus
d'apprentissage. En e-Learning, les outils TIC sont principalement
utilisés pour faciliter l'interaction entre les acteurs humains.

**Les TIC en tant que matière enseignée** Dans le passé, il était
indispensable de savoir programmer pour pouvoir utiliser un ordinateur.
Par conséquent, l'enseignement de l'informatique s'articulait autour des
thèmes en rapport avec la programmation et le matériel, tels que les
fonctions et les circuits logiques. L'ordinateur en tant que tel était
le sujet principal de l'enseignement. Les algorithmes ainsi que la
programmation constituent toujours un élément-clé de la formation
informatique dorénavant complétée par des sujets tels que les bases de données ou les réseaux. L'article de Peter Denning, *Great Principles of Computing* \[Den03\], contient une
segmentation de la spécialité « informatique », laquelle peut également
s'avérer utile pour les écoles. Il existe bien évidemment d'autres
segmentations possibles ; Denning lui-même mentionne des alternatives
dans son article. Nous reproduisons ici les cinq domaines définis par
Denning :

<blockquote>
<p><strong>Calcul</strong> Ce qui peut être calculé, les limites du calcul. Algorithmes, structures de commande, structures de données, automates, langages, machines de Turing, ordinateurs universels, complexité de Turing, complexité de Chaitin, autoréférence, logique prédictive, approximations, heuristique, incalculabilité, conversions, réalisations physiques.</p>
<p><strong>Communication</strong> Envoi de messages d’un point à un autre. Transmission de données, entropie de Shannon, codage vers le support, capacité de canal, suppression de bruit, compression de fichier, cryptographie, réseaux en paquets reconfigurables, contrôle d’erreur de bout en bout.</p>
<p><strong>Coordination</strong> Plusieurs entités coopèrent dans le sens d’un résultat unique. Humain à humain (boucles d’action, flux d’opérations assistées par des ordinateurs de communication), homme à ordinateur (interface, entrée, sortie, temps de réponse), ordinateur à ordinateur (synchronisations, concurrence, interblocage, sérialisabilité, actions atomiques).</p>
<p><strong>Automatisation</strong> Exécution de tâches cognitives par un ordinateur. Simulation de tâches cognitives, distinctions philosophiques relatives à l’automatisation, expertise et systèmes experts, amélioration de l’intelligence, essais de Turing, apprentissage et reconnaissance par la machine, bionique.</p>
<p><strong>Mémorisation</strong> Stockage et récupération des informations. Hiérarchies de stockage, emplacement de référence, mise en cache, espace adressable et mappage, noms, partage, emballement, recherche, récupération par nom, récupération par contenu.</p>
</blockquote>

En résumé, on peut constater que les rôles de l'ordinateur dans la
formation sont multiples. Il est premièrement un outil présent au
quotidien, deuxièmement un support de communication et de transmission
d'informations et troisièmement il est lui-même le sujet de la
formation. Ces rôles ne sont généralement pas assez différenciés et
l'expression générique « enseignement informatique » est utilisée aussi
bien pour les logiciels d'apprentissage au cours des premières années de
la scolarité que pour la conception de systèmes informatiques dans un
institut universitaire de technologie. Cette confusion des termes est
source de nombreux malentendus. Il est donc essentiel de distinguer
clairement les différents rôles des TIC dans la formation. Nous
allons nous limiter dans cet ouvrage à l'enseignement de l'informatique
en tant que matière et nous nous contenterons d'effleurer le rôle de
l'ordinateur en tant qu'outil ou support d'apprentissage. L'utilisation
d'environnements de formation avec assistance par ordinateur ne peut
bien évidemment pas être éludée dans un livre consacré à la didactique
de l'informatique. En effet, car s'il est un domaine prédestiné à
l'usage de l'ordinateur comme support de cours, c'est bien celui-ci.

**Solution :** lLes technologies de l'information et de la communication
jouent différents rôles dans le domaine de la formation et de
l'enseignement : outil au quotidien ou dans l'enseignement spécialisé,
support d'assistance au processus d'apprentissage et sujet de formation
proprement dit. Pour éviter les malentendus, il convient ici de préciser
l'expression « enseignement informatique » : celui-ci désigne un enseignement dont la matière est l'ordinateur lui-même.

**Exemple 1 : logiciels standard en tant qu'outil et sujet d'étude**

Les logiciels dits standard, tels que les traitements de texte,
tableurs, programmes de traitement graphique ou moteurs de recherche
Internet sont aujourd'hui utilisés dans quasiment tous les cycles
scolaires. À titre d'exemple, de plus en plus de rédactions et de
dissertations sont écrites sur ordinateur, ce qui facilite les
corrections et les modifications ultérieures. Même les corrections
rédactionnelles sont en partie électroniques : les corrections
linguistiques pures sont apportées par le professeur en mode suivi des
modifications et les remarques à propos du contenu sont enregistrées au
format MP3 dans des fichiers de remarque qui sont mis à disposition sur
une plate-forme d'échange commune.

Cette application des logiciels standard en tant qu'outils de formation
peut être comparée à l'utilisation d'une calculatrice dans
l'enseignement de la géographie ou à la consultation de termes
spécifiques dans une encyclopédie dans l'enseignement de l'histoire. Le rôle du rôle du professeur de géographie n'est pas d'expliquer le calcul des pourcentages et l'utilisation efficace de la calculatrice. Il part en effet du principe que les élèves ont acquis les
connaissances et les aptitudes nécessaires pendant leurs cours de mathématiques. De même, les enseignants des différentes matières doivent pouvoir se baser sur le fait que les principes fondamentaux nécessaires à l'utilisation des logiciels standard ont été préalablement acquis par les élèves.

Contrairement à une idée largement répandue, les logiciels standard sont
aujourd'hui devenus trop complexes pour se contenter d'apprendre à les
manipuler « sur le tas ». Prenons comme exemple un traitement de texte :
de nombreux documents électroniques sont créés en contradiction avec les
règles de l'art. Ils ne font appel à aucun modèle de mise en forme, la
table des matières n'est pas générée automatiquement et les photos y
sont insérées avec une résolution trop importante. Une initiation aux
concepts fondamentaux du traitement de texte est indispensable. Le
traitement de texte n'est pas un simple outil, il est aussi une matière
à enseigner. Le thème « Écriture -- hier et aujourd'hui » permet
d'illustrer un certain nombre de concepts informatiques : les composants
individuels des textes mènent au concept d'objet, les objets d'un
traitement de texte possèdent des propriétés pour lesquelles il existe
des réglages par défaut ; les opérations modifient les attributs des
objets et doivent être exécutées dans un ordre donné. Cette liste peut
être prolongée à volonté. Vous trouverez des exemples supplémentaires,
également au sujet des tableurs et des bases de données, dans le support
de cours *Informatik und Alltag* qui propose une introduction
applicative de l'informatique au niveau secondaire \[Fri98\].

**Exemple 2 : logiciels mathématiques en tant qu'outils
spécialisés et sujets d'étude**

L'ordinateur a considérablement modifié l'enseignement des mathématiques
au cours des dernières décennies. Les calculatrices à fonctions
graphiques permettent de générer les graphes des fonctions d'une simple
pression sur une touche. Les progiciels de calcul formel calculent
symboliquement les dérivées et les intégrales. Ces calculs sont plus
rapides que s'ils étaient effectués à la main et le risque d'erreur est
plus faible. Des systèmes de calcul formel puissants peuvent en outre
accomplir des tâches qu'il serait impossible de mener à bien
manuellement.

Mais l'ordinateur en tant qu'outil dans l'enseignement des mathématiques
est également un sujet pour l'enseignement de l'informatique. Des thèmes
tels que les analyseurs syntaxiques, les algorithmes numériques, les
problèmes de l'arithmétique finie, l'infographie et la géométrie
algorithmique jusqu'aux questions de calculabilité jouent un rôle
important lors de l'élaboration d'outils mathématiques complexes. Une
utilisation compétente et efficace d'un tel outil serait quasiment
impossible sans la compréhension de certains concepts fondamentaux qui
résident derrière les systèmes de calcul formel.

**Exemple 3 : applets Web en tant que logiciels d'enseignement et
matière enseignée**

De petites applets interactives présentes sur l'Internet font
aujourd'hui office de logiciels d'enseignements dans de nombreux
domaines : la visualisation des forces de friction en mécanique, la
simulation des modèles prédateur-proie en biologie ou la superposition
de tonalités en musique n'en sont que trois exemples. Les applets
permettent la visualisation des processus dynamiques, la modification
interactive des paramètres d'un modèle et la simulation d'exemples
réalistes.

En enseignement de l'informatique, la programmation d'applets,
l'élaboration de GUI ainsi que les principes d'utilisabilité et de
conception multimédia peuvent constituer des sujets d'étude. Et c'est
justement dans les petits environnements pédagogiques que les exigences
en matière de convivialité et de robustesse des logiciels sont
particulièrement élevées.

**Exemple 4 : outils de TCAO en e-Learning et en tant que sujet
d'étude**

Il existe de nombreux outils assistés par ordinateur (TCAO -- Travail
collaboratif assisté par ordinateur) qui facilitent la collaboration à
distance. Ceux-ci vont des simples utilitaires de messagerie instantanée
pour la communication synchrone jusqu'aux synergiciels de coordination
et d'échange de documents ou aux Wikis pour la création et l'édition
communes de pages Web, en passant par les plates-formes de présentation
et la vidéoconférence. Cette liste peut être complétée par des outils
tels que les blogs, les sites Web avec des tests simples ou des sondages
et bien plus encore.

En enseignement informatique, ces systèmes peuvent servir d'exemples aux
difficultés rencontrées avec les applications partagées. Ils permettent également d'illustrer de nombreux concepts sophistiqués : multithreading, problèmes de concurrence, architecture
client-serveur, performances des applications serveur, streaming audio
et vidéo, gestion des versions ou questions de sécurité, pour ne citer
que quelques exemples.


