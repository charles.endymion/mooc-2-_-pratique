**9**


**Les différents accès à l'enseignement de l'informatique**


« Les bases de données ? Rien de plus simple !! » promet la publicité.
Toute une armada d'assistants (Wizards) accomplissent en un tour de main
les tâches même les plus ardues ; l'utilisateur ne s'occupe plus de
rien. Pour ce faire, il suffit de sélectionner la base de données
souhaitée dans une liste de suggestions et de répondre à quelques
questions de l'assistant. Cependant, si c'est aussi simple, pourquoi
aurions-nous encore besoin d'une formation sur les bases de données ? La
motivation des étudiants à se faire expliquer des concepts tels que les
entités, les règles de normalisation, l'intégrité ou la cohérence est
faible. Ce ne sont pas les principes théoriques qui sont demandés, mais
des compétences qui peuvent être mises en pratique quasi-immédiatement.


**Problème :** l'écart s'est creusé entre les principes théoriques et
les applications pratiques dans l'enseignement de l'informatique.
L'approche systématique d'un sujet -- depuis ses principes théoriques
jusqu'à ses applications pratiques -- court le risque d'être peu
motivante pour les étudiants qui ne perçoivent souvent pas son intérêt.
Une approche purement pratique, par contre, peut se heurter à des
limites en raison de l'absence de principes fondamentaux permettant de
maîtriser des problèmes complexes.


L'enseignement de l'informatique s'articule souvent autour de l'étude de
systèmes informatiques. Il s'agit ici de créer de tels systèmes ou de
les utiliser pour résoudre des problèmes concrets. La priorité est
accordée ici aux aspects théoriques ou aux compétences pratiques,
suivant les objectifs ou les aspects de la matière enseignée importants
pour les étudiants. Une bonne illustration est l'exemple des bases de
données :


**utilisateur de base de données** Les priorités des utilisateurs de
bases de données sont la saisie, la recherche et la suppression des
données. Les aspects de la sécurité et de la protection des données ont
également leur importance.


**administrateur de base de données** L'administrateur de base de
données est responsable du bon fonctionnement du système de base de
données ainsi que des aspects tels que la gestion des utilisateurs et le
contrôle d'accès, la configuration de l'interface utilisateur, la
maintenance des outils administratifs, l'optimisation des paramètres de
la base de données ou encore le contrôle des fichiers journaux.


**concepteur de base de données** Les concepteurs de bases de données
sont essentiellement concernés par leur architecture, qui s'articule
autour d'aspects tels que le processus de développement, la
normalisation, le langage d'interrogation, l'intégrité des données ou la
migration d'une base de données.


Différentes approches de l'enseignement des bases de données sont
proposées pour les publics décrits ci-dessus, lesquelles seront étudiées
en détail ci-après.


**utilisateur de base de données** L'approche recommandée de la
formation des utilisateurs inclut un exemple de base de données préparé
à l'avance à l'aide duquel seront illustrées les opérations de saisie et
d'insertion de nouvelles données, de modification et de suppression des
données existantes et d'interrogation de la base de données. C'est après
seulement que seront abordés les concepts fondamentaux des bases de
données tels que les relations et l'intégrité référentielle. Cette
approche -- du spécifique à la généralité -- tient compte de l'intérêt
des utilisateurs pour l'application pratique.

**administrateur de base de données** Là aussi, un exemple de base de
données existante constitue un point de départ approprié. Cependant,
contrairement à la formation des utilisateurs, cette base de données
peut s'avérer imparfaite et présenter des défauts évidents, par exemple
dans l'interface utilisateur. Une analyse des faiblesses peut donner
lieu à des modifications de l'interface utilisateur ou des autorisations
d'accès. Cette approche -- également appelée de l'extérieur vers
l'intérieur -- ne présente pas successivement les concepts individuels
des bases de données, mais démarre directement par l'utilisation d'une
base de données réelle. Les futurs administrateurs de bases de données
sont confrontés à des problèmes réalistes.



**concepteur de bases de données** La formation des futurs concepteurs
de bases de données s'articule autour des modèles de données et de leur
mise en œuvre. Elle commence généralement par une partie théorique qui
suit la logique de la spécialité et au cours de laquelle sont présentés
les principaux termes et concepts clés, par exemple les principes
fondamentaux des bases de données, les modèles de données, les entités,
les attributs, les relations, les clés, les formes normales, etc.
L'approche ne doit cependant être en aucun cas ascendante : les
exigences relatives aux bases de données peuvent également être
analysées et spécifiées dans le sens descendant en partant de grandes
solutions de bases de données existantes.


Cet exemple montre que la structure des grands modules d'enseignement ne
doit pas toujours être organisée en suivant une approche systématique de
la spécialité. La structure des cours et des sessions de formation peut
aller des solutions informatiques de la vie quotidienne jusqu'aux
fondements théoriques en passant par la modification des solutions
existantes, c'est-à-dire du spécifique à la généralité. Les séances de
formation peuvent être organisées étape par étape dans le sens
ascendant. L'introduction d'un langage de programmation peut commencer
par aborder les structures d'exécution, puis les structures de données
et ensuite les éléments structurels tels que les classes, les méthodes
ou l'héritage. De nombreux développements en informatique peuvent
facilement être expliqués en cours dans un contexte historique. Un sujet
peut également être abordé volontairement sous deux angles différents,
par exemple le développement logiciel du point de vue des données ou des
processus.


Il n'existe pas d'approche correcte unique dans la planification des
grands modules d'enseignement. Le plus important est que l'enseignant
établisse une structure claire et que celle-ci se reflète distinctement
dans la formation. La structure des longues séances de formation ne doit
pas toujours obéir au même schéma ; des approches variées dans la
formation informatique contribuent à la diversité et tiennent compte des
différents modes d'enseignement.


**Solution :** Les longues séances de formation dans l'enseignement de
l'informatique ne doivent pas nécessairement obéir à la logique de la
spécialité. Le plus important est que la formation présente une
structure clairement identifiable. Une approche ascendante, descendante,
qui suit l'évolution historique, du spécifique à la généralité ou
inversement ainsi que d'autres approches contribuent à la diversité de
l'enseignement.
