**24**


**Les enseignants en informatique ne peuvent pas tout connaître**


« Mon exemple fonctionne très bien avec l'installation par défaut de
RedHat 9, mais pas sous Debian 3.1 Sarge Kernel 2.6.6. Debian
fonctionne-t-il avec des adresses de base aléatoire dans la pile ? Le
problème viendrait-il de là ? »


« Mon application ne fonctionne que sous PHP4. Pourquoi les paramètres
CGI ne sont-ils pas lus correctement sous PHP5 ? »


« J'utilise l'environnement de développement Eclipse pour notre projet
de programmation. Comment appliquer la fonction Rechercher/Remplacer
avec des expressions régulières à tous les fichiers d'une
arborescence ? »


« Hier, j'ai révisé le cours théorique sur les supports de mémorisation.
Sur les CD, les petites erreurs ou les dommages visibles peuvent être
corrigés à l'aide d'un code de Reed-Solomon. Mais celui-ci n'a pas été
approfondi dans le cours. Comment ce codage fonctionne-t-il
exactement ? »


« Comment faire pour créer une lettre-type avec Microsoft Word et
récupérer les informations d'adresse dans une base de données Access ? »


**Problème :** dans l'enseignement de l'informatique, les enseignants
sont souvent confrontés à des questions auxquelles ils ne peuvent pas
répondre tout de suite.


Les questions soulevées par l'enseignement de l'informatique concernent
souvent des informations bien précises, des particularités techniques de
certains logiciels ou encore la connaissance d'un produit spécifique.
Les élèves ou les stagiaires posent notamment de nombreuses questions
pendant les exercices pratiques sur ordinateur. Cependant, l'enseignant
ne peut apporter une réponse directe à bon nombre de ces questions. Il
faut donc élaborer une stratégie pour y faire face et en aucun cas
succomber à la pression qui impose à un enseignant compétent d'avoir
réponse à tout. Au contraire : les questions posées et les problèmes
rencontrés peuvent représenter un excellent sujet de cours dont le thème
sera le comportement à adopter et la démarche à entreprendre pour leur
résolution.


Les étudiants peuvent trouver eux-mêmes la réponse à de nombreuses
questions. L'enseignant peut donner des indications pour isoler le
problème, sur les sources d'informations disponibles ou encore sur les
requêtes qui peuvent mener au but. Cette approche encourage le travail
autonome des étudiants.


Il est intéressant d'utiliser toutes les sources d'informations
disponibles. Les points de départ possibles sont les pages d'aide de
l'application concernée, les moteurs de recherche généraux et les
catalogues sur Internet ou encore les forums spécifiques et les listes
de diffusion qui concernent l'application.


La plupart des problèmes rencontrés sur les ordinateurs ne sont pas
nouveaux et les solutions sont souvent déjà documentées sur Internet.
Une recherche ciblée dans les archives d'un Newsgroup s'avère souvent
fructueuse. Lorsqu'une application se bloque avec un message d'erreur,
il suffit de copier ce dernier dans le champ de saisie d'un moteur de
recherche. Cette méthode permet généralement de trouver les pages Web
appropriées où sont décrites l'erreur, les causes possibles ainsi que
les solutions.


**Solution :** les enseignants ne peuvent pas tout connaître. Les
questions posées peuvent être une bonne occasion de prévoir un cours sur
le thème des stratégies de résolution des problèmes. La multiplication
des sources d'informations permet de répondre aux questions et de
résoudre les problèmes rapidement.
