# Déposer une ressource pour travailler entre pair·e·s

Dans cette formation pour apprendre à enseigner l'informatique, nous créons des fiches et ressources, pour des activités et leur rendus. Comment les partager (entre nous, ou plus largement avec d'autres collègues) ?

On se propose de le faire sur la plateforme de dépôt [GITLAB](https://fr.wikipedia.org/wiki/GitLab) basée sur [GIT](https://fr.wikipedia.org/wiki/Git).

Ci-dessous, un résumé des étapes, que nous détaillons ensuite.

1. S'**inscrire** ou se **connecter** à son compte gitlab ; c'est **votre** espace ressources
2. **Accéder** à l'espace ressources original et le **dupliquer** 
3. **Accéder** à votre copie des ressources 
4. **Déposer** une ressource dans votre espace :
    - soit en la créant via l'interface web de gitlab depuis votre navigateur internet
    - soit en la téléversant


## Rejoindre l'espace de partage

1. S'[incrire](https://gitlab.com/users/sign_up) ou se [connecter](https://gitlab.com/users/sign_in) si vous avez déjà un compte
2. Accéder à la [page du projet](https://gitlab.com/mooc-nsi-snt/mooc-2-ressources) et créer une version de cet espace _chez vous_ ie dans votre propre espace _gitlab_ ; dans le _jargon_ git cette opération se nomme _fork_ (ou _Créer une divergence_ en français) c'est d'ailleurs le nom du bouton sur lequel il va falloir cliquer (ci-dessous pour la version en anglais) :

    ![fork](fork.png)

    Au moment de la création de ce _fork_ il vous est proposé un choix de visibilité pour votre projet : il faut choisir public pour pouvoir donner accès à votre travail.

3. Dès lors vous pouvez accéder à votre espace dépôt qui aura pour url : `https://gitlab.com/votre_pseudo/mooc-2-ressources`. Et choisir la vue _web_ de votre espace en cliquant le bouton `Web IDE` :
    ![Bouton WEB IDE](webide_bouton.png)
    
    Vous devriez maintenant avoir une interface similaire à celle-là :
    ![Interface WEB IDE](webide_interface.png)

<!--
y [demander l'accès](https://gitlab.com/mooc-nsi-snt/mooc-2-ressources/-/project_members/request_access) comme [illustré ici](https://gitlab.com/mooc-nsi-snt/mooc-2-_-pratique/-/raw/master/Ressources/Images/request-access.png)
  - Nous [validerons l'accès](https://gitlab.com/mooc-nsi-snt/mooc-2-ressources/-/project_members?tab=access_requests) en tant que co-développeur qq heures plus tard.

    NOTE IL FAUDRA CLIQUER POUR CHAQUE PERSONNE !
-->

## 4. Déposer une ressource

Il est important de se rappeler qu'il faut déposer un unique fichier, qui sera une fiche élève ou _prof_ ou encore un _déroulé_ de séance etc. Les formats seront du pdf, du markdown ou un dossier compressé au format zip. Ces formats très répandus garantissent que la ressource pourra être accessible par le plus grand nombre.

- Bien prendre connaissance de [l'arborescence](https://gitlab.com/mooc-nsi-snt/mooc-2-ressources) 
- Suivre une règle de _nommage précise_ : `nom_du_fichier_$login-sur-gitlab.md`
  - par exemple `fiche_module1_shorau.md` où `shorau` est votre login
- Créer ou téléverser une ressource (voir les 2 chapitres suivants)
- Bien noter le lien du fichier lui même par exemple `https://gitlab.com/mooc-nsi-snt/mooc-2-ressources/-/blob/main/1_Penser-Concevoir-Elaborer/fiche_module1_shorau.md` avec cet exemple pour pouvoir transmettre ce lien aux pair-es.

### Créer une ressource directement via gitlab

Dans **votre espace** :

- Sélectionner `New file` fans le menu lié à un répertoire (ici le premier) :<br>
    ![ajout sol1 etape 1](webide_add_sol1_1.png)
- Choisir un nom (ici il s'agira d'un fichier markdown) :<br>
    ![ajout sol1 etape 2](webide_add_sol1_2.png)        
    - Je rédige (un aperçu est même disponible) :<br>
        ![ajout sol1 etape 3](webide_add_sol1_3.png)        
    - Je valide les changements en faisant un _commit_ :<br>
        ![ajout sol1 etape 4](webide_add_sol1_4.png)
      Le _commit_ permet de valider les modifications apportées à votre espace gitlab, et de ne pas les laisser à l'état de _brouillon_     
    - Je sélectionne la branche _main_ **pour ne pas en créer une autre** et je dois laisser un message dans la zone _Commit Message_ :<br>
        ![ajout sol1 etape 5](webide_add_sol1_5.png)        


### Téléverser une ressource existante

Dans **votre espace** :

- Sélectionner `Upload file` :<br>
    ![ajout sol2 etape 1](webide_add_sol2_1.png)
- Déposer votre ressource et réaliser le _commit_, sur la branche _main_<br>
    ![ajout sol2 etape 2](webide_add_sol2_2.png)


## Quels formats de fichier pour les ressources ?

Nous privilégions les ressources aux formats simples et _relativement_ universel :

- fichiers markdown et pdf sont vivement conseillés, visualisables directement depuis votre espace gitlab
- si vous avez des vidéos, il faudra que vous assuriez vous-même leur hébergement sur un site de diffusion et en proposer l'accès
- on évitera tout document type suite bureautique