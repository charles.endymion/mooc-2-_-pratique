# Markdown, du balisage _ultra-light_

Il s'agit d'un langage de balisage très léger qui est utilisé dans les cellules de texte des notebooks jupyter mais pas que. Le fichier source qui a servi à créer la page web que vous lisez a été faite en markdown (md en abrégé). Voici les grandes lignes du markdown.

## Des Titres

Si on tape ceci :

```md
# Ceci est un titre de niveau 1
## Ceci est un titre de niveau 2
### Ceci est un titre de niveau 3
```

On obtient :

# Ceci est un titre de niveau 1
## Ceci est un titre de niveau 2
### Ceci est un titre de niveau 3

## Des listes 

### Non ordonnée 

Si on tape ceci :

```md
- item 1
- item 2
- item 3
```

ou encore 

```md
* item 1
* item 2
* item 3
```

On obtient :

- item 1
- item 2
- item 3

### Ordonnée

Si on tape ceci :

```md
1. item 1
2. item 2
3. item 3
```

On obtient :

1. item 1
2. item 2
3. item 3

Notez qu'on peut démarrer à un autre entier que 1 et qu'incrémenter les numéros n'est pas important :

```md
3. item 3
4. item 4
4. item 5
```

3. item 3
4. item 4
4. item 5



## Des emphases

Si on tape ceci :

```md 
On peut insister sur une partie de texte : **ceci est important** et d'ailleurs  __cela aussi__

Bref 2 caractères étoile * ou alors 2 soulignés _ 

Sinon on peut aussi utiliser l'*emphase pour distinguer une portion de texte* ou _comme ça_
```

On obtient :

On peut insister sur une partie de texte : **ceci est important** et d'ailleurs  __cela aussi__

Bref 2 caractères étoile `*` ou alors 2 soulignés `_` 

Sinon on peut aussi utiliser l'*emphase pour distinguer une portion de texte* ou _comme ça_


## Des images

Si on tape ceci (il faudra veiller au chemin relatif du fichier image):

```md 
![une rosace](2_360.png)
```

On obtient :

![une rosace](2_360.png)



## Des liens

Si on tape ceci :

```md 
Pour vous entrainer vous pouvez faire [ce tutoriel en français](https://www.markdowntutorial.com/fr/)
```

On obtient :
    
Pour vous entrainer vous pouvez faire [ce tutoriel en français](https://www.markdowntutorial.com/fr/)


## Des formules mathématiques

Si on tape ceci :

```md 
$$\frac{\pi}{4} = \lim_{n\rightarrow\infty} \frac{\varphi(n)}{n^2}$$
```

On obtient :

$$\frac{\pi}{4} = \lim_{n\rightarrow\infty} \frac{\varphi(n)}{n^2}$$

```md
Des formules plus simples intégrées à la lignes sont possible avec un seul $ comme délimiteur : $\forall n\in\mathbb{N}, n\geq 0$.
```


Des formules plus simples intégrées à la lignes sont possible avec un seul `$` comme délimiteur : $\forall n\in\mathbb{N}, n\geq 0$.

## Des tableaux

Si on tape ceci :

```md 
| Langage | Inventeur        | Année |
| :------ | :-------:        | ----: |
| Python  | Guido van Rossum | 1991  |
| Java    | James Gosling    | 1995  |
| aligné à gauche | centré | aligné à droite |
```

On obtient :

| Langage | Inventeur        | Année |
| :------ | :-------:        | ----: |
| Python  | Guido van Rossum | 1991  |
| Java    | James Gosling    | 1995  |
| aligné à gauche | centré | aligné à droite |


## Du code 

Si on tape ceci :
```md 
    ```python
    def foo(n):
        return n**2
    ```
```


On obtient :

```python
def foo(n):
    return n**2
```
