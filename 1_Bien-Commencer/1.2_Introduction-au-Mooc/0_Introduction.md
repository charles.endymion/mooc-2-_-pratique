# Pratiquer l'enseignement de l'Informatique au Lycée

## Sciences Numériques et Technologie / Numérique et Sciences Informatiques

L’année scolaire 2018-2019 a vu l’instauration de deux nouvelles matières : La SNT  qui se veut un enseignement de culture Informatique en classe de seconde pour toutes et tous et la NSI en tant que spécialité dans la réforme du baccalauréat général. Même si les deux dernières décennies du 20ème siècle avaient vu des options d’Informatique, il s’agit cette fois d’une lame de fond puisque la SNT est à destination de tous les élèves de seconde du lycée général et technologique tandis que la NSI est une spécialité intégrée au curricula des élèves au même titre que les autres disciplines.
Cet entrée dans le cursus des élèves s’est de plus accompagnée de la création d’un CAPES d’Informatique en 2019-2020 suivie d’une Agrégation d’Informatique pour 2021-2022. L’Informatique est donc reconnue par l’Éducation Nationale comme une discipline à part entière et autonome des Mathématiques, de la Physique-Chimie, de l’Économie-Gestion ou encore des Sciences de l’Ingénieur. Une nouvelle discipline est née.

Contrairement aux autres disciplines pour lesquelles la didactique est déjà bien assise dans le second degré, en Informatique les quelques expériences d'introduction de l'enseignement (dans les années 70 et au milieu des [années 80](https://www.epi.asso.fr/revue/42som.htm)) ont laissé la didactique au stade embryonnaire.

Aujourd'hui, l'Informatique revient en force dans le secondaire et avec elle la tâche immense et passionnante de façonner sa didactique. Heureusement, nous ne partons pas «from [scratch](https://scratch.mit.edu/)». D'abord parce que des leçons, sont à tirer des expériences passées, ensuite parce que des transpositions didactiques avec les autres disciplines plus anciennes sont probablement possibles, enfin parce que l’Informatique est une discipline reconnue et à part entière depuis des années dans le supérieur.

Réussir la didactique de l'Informatique en s'aidant de transpositions _verticale_ entre le post-bac et le second degré ou _horizontale_ (avec les autres disciplines ou avec ce qui a été tenté dans le passé) va demander une forte adaptation voire de la  créativité pour réinventer de nouvelles pratiques professionnelles. Il faudra absolument prendre en compte que les apprenantes et les apprenants sont deux à trois ans plus jeunes ; ou encore intégrer des spécificités disciplinaires comme la gestion de projet.

Le but étant de préparer au mieux nos élèves de lycée à esquisser de bonnes images mentales des concepts fondamentaux de l'Informatique et préparer leur avenir en tant que simple citoyen ou comme spécialiste de la discipline.

## Enseigner l'Informatique

Si la didactique de l'Informatique est une discipline jeune, elle n'est toutefois pas vierge de jalons, de repères que d'autres ont posés. Dans ce Mooc, nous vous proposons des références à l'ouvrage : "Enseigner l'Informatique" par W. Hartmann, M. Näf et R. Reichert, dans lequel, de façon très pragmatique les auteurs illustrent par des exemples ce qu'un enseignement d'Informatique est, ce qu'il n'est pas et ce qu'il ne devrait surtout pas devenir.

Dans une première partie intitulée "Classification et délimitation", l'ouvrage pose le cadre de ce que peut contenir un enseignement d'Informatique et du rôle d'un enseignant d'Informatique :

1. L'objet du cours d'Informatique c'est l'Informatique !
    L'informatique _se voit_ comme :
        - Outil, au quotidien et dans l'enseignement spécialisé
        - Support via les logiciels d'apprentissage et la formation à distance
        - Matière enseignée
2. Les enseignant d'Informatique enseignent l'Informatique...
    et ne sont ni experts dans l'évaluation de supports informatiques pour les besoins des enseignements, ni personnels du service de maintenance

Consulter la Partie I de l'ouvrage : _ici le lien_

Au fil du Mooc, nous mettrons des liens vers les chapitres de l'ouvrage en rapport avec le thème du module.
