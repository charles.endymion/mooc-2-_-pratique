# Interview David Roche 3/5, enseigner l'informatique au secondaire

Cette série d'interviews a pour but de recueillir l'expérience d'enseignants et d'enseignantes de NSI (spécialité Numérique et Sciences Informatiques):
comment sont-ils arrivés à enseigner cette discipline, quelles difficultés ont-ils rencontrées, quelle pédagogie ont-ils mis en oeuvre, ...  ? Ils et elles témoignent pour partager leurs pratiques avec de jeunes venus dans l'enseignement de NSI.

## Interview de David Roche, enseignant en NSI au Lycée Guillaume Fichet de Bonneville

**Sommaire des 5 vidéos**

* 1/5 [David Roche, qui es-tu ?](./1_interview_David_Roche1_5.md) 
* 2/5 [Comment enseigner et préparer un cours de NSI ?](./1_interview_David_Roche2_5.md) 
* **3/5 Pratiques en classe\.**
* 4/5 [Pratiques en conditions dégradées\.](./1_interview_David_Roche4_5.md)
* 5/5 [Enjeux d'égalité entre filles et garçons\.](./1_interview_David_Roche5_5.md)

## 3/5 Pratiques en classe

Activités en classe : David détaille les principes et méthodes qu'il mobilise pour conduire les activités de ses élèves en classe

[![Interview David Roche 3/5](https://mooc-nsi-snt.gitlab.io/portail/assets/vignette-interview-david-roche-600x311.png)](https://files.inria.fr/LearningLab_public/C045TV/ITW/NSI-ITW-DROCHE-3.mp4)

### Rentrons dans les détails de la pratique avec les élèves ...

... y compris qui pourraientt avoir plus de difficultés.

... « Là si je devais définir un terme pour définir ma pratique pédagogique, c'est : l'autonomie des élèves. Les élèves vont avoir des échéances qu'illes doivent respecter, mais à l'intérieur de ce laps de temps illes s'organisent un petit peu comme illes veulent, comme illes en envie, il va y avoir des élèves qui vont énormément travailler chez eux, grâce à ce système de site Internet, illes ont vraiment toutes les ressources, d'autres vont avoir beaucoup de mal à travailler chez eux et/ou en autonomie et celles et ceux là vont beaucoup plus travailler en classe. Pour les élèves qui vont vite, il y a toujours des choses, par exemple préparer l'épreuve pratique en Terminale, faire des activités de la banque nationale de sujets en première, toujours pour les élèves un peu plus rapide que les autres il y a toujours des choses à faire pour aller du loin [par exemple des projets], c'est vraiment l'autonomie qui est le cœur de mon travail »

- On voit donc une pédagogie différenciée qui ne freine pas les plus rapides mais permet au prof d'avoir de la disponibilioté pour qui en a le plus besoin.

... « pour entrer un peu plus dans les détails de ma pratique quotidienne, je dirais que un moment qui absolument fondamental est-ce que je l'appelle le ``bilan´´. Le bilan est le point de rendez-vous pour les élèves, qui ont une grande autonomie mais il y a ce moment là, avec une date, par exemple voilà le 15 mars on fait le bilan sur les bases de données. À ce moment là on va reprendre un petit peu toutes les notions à l'oral, un petit peu comme un cours magistral, sauf que les élèves obt déjà vu toutes les notions, mais ça permet d'entendre reprendre chaque élément, et puis ça peut aussi permettre de poser des questions (au delà des questions qui sont posées au fil du temps pendant les activités quand je passe dans les rangs) au moment de cette prise de recul, pour tourner la page et passer à autre chose. »

- On est donc bien là dans une pratique de pédagogie inversée (au sens large).

... « Une autre chose importante aussi est ce qu'on appelle les fiches de révision parce que souvent les élèves ont du mal à faire le tri entre qui est vraiment important de ce qui est plus anecdotique, évidemment je peux déjà insister là-dessus pendant le bilan, mais les élèves aime bien avoir quelque chose, comme une sorte de fiche de révision et donc depuis l'année dernière je leur propose carrément des fiches de révision. Et d'ailleurs pour faire mon bilan je me sers tout simplement la fiche de révision, donc lors du bilan ils ont à la fois la fiche de révision par écrit et moi qui ré-explique ce qu'illes savent déjà en théorie déjà, et illes confirment avoir vraiment besoin de cette partie auditive et récapitulative. »

- Ces deux leviers, très simples, permettent aussi aux élèves de s'auto-évaluer et de prendre du recul par rapport à leur propre démarche d'apprentissage dans l'idée d'apprendre à apprendre.






