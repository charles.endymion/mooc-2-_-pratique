# Interview David Roche 5/5, enseigner l'informatique au secondaire

Cette série d'interviews a pour but de recueillir l'expérience d'enseignants et d'enseignantes de NSI (spécialité Numérique et Sciences Informatiques):
comment sont-ils arrivés à enseigner cette discipline, quelles difficultés ont-ils rencontrées, quelle pédagogie ont-ils mis en oeuvre, ...  ? Ils et elles témoignent pour partager leurs pratiques avec de jeunes venus dans l'enseignement de NSI.

## Interview de David Roche, enseignant en NSI au Lycée Guillaume Fichet de Bonneville

**Sommaire des 5 vidéos**

* 1/5 [David Roche, qui es-tu ?](./1_interview_David_Roche1_5.md) 
* 2/5 [Comment enseigner et préparer un cours de NSI ?](./1_interview_David_Roche2_5.md) 
* 3/5 [Pratiques en classe\.](./1_interview_David_Roche3_5.md)
* 4/5 [Pratiques en conditions dégradées\.](./1_interview_David_Roche4_5.md)
* **5/5 Enjeux d'égalité entre filles et garçons\.**

## 5/5 Enjeux d'égalité entre filles et garçons.

Ici David témoigne sur son vécu par rapport à l'enjeu d'égalité entre filles et garçons.

[![Interview David Roche 5/5](https://mooc-nsi-snt.gitlab.io/portail/assets/vignette-interview-david-roche-600x311.png)](https://files.inria.fr/LearningLab_public/C045TV/ITW/NSI-ITW-DROCHE-5.mp4)

Comment résoudre le problème de déficit de filles en classe de NSI ? Comment plus attirer les filles vers cet enseignement ?

… « c'est un véritable enjeu, et à mon niveau une grande fierté [même si] je n'y suis pas pour grand-chose : j'ai l'immense privilège d'avoir 50 % de fille 50 % de garçon (47% / 53%) en terminale, oui j'avoue que je suis plutôt content » …

- On pourra lire ici le [témoignage de deux élèves](https://www.lemonde.fr/blog/binaire/2021/04/30/dites-les-filles-cest-quoi-linformatique-au-lycee) de David interviewées sur le blog Binaire du Monde.fr

 … « je vais commencer parce qui pour moi est une très mauvaise idée : ce serait d'adapter le contenu de l'enseignement (par exemple au niveau national les programmes ou la façon de les aborder au niveau d'une classe) pour attirer les filles, les programmes tels qu'il sont peuvent très bien contenir à la fois aux filles et aux garçons, il n'y pas de souci … ce serait même humiliant, de penser cela » … 
 
- On constate effectivement une tendance récurrente, face à la faible (15%-85% environ) répartition [filles-garçons](https://www.societe-informatique-de-france.fr/2019/12/note-sur-la-mise-en-place-de-la-specialite-nsi-decembre-2019), à proposer un contenu plus adapté aux filles, y compris jusqu'à proposer de créer un enseignement (non technique) d'humanité numérique à côté de l'informatique. 

… « là où il y a vraiment un travail à faire c'est au niveau de la présentation de la spécialité : tous les ans on doit on doit présenter aux élèves de seconde différentes spécialité et il faut vraiment insister sur le fait que c'est une spécialité aussi bien pour les filles et pour les garçons et surtout que c'est pas une spécialité pour ``geeks´´, l'informatique est une science au même titre que les mathématiques ou les sciences de la vie et de la terre et pas un truc … où on va inventer des portes automatiques ou des choses comme ça » … 

Au niveau des pratiques en classe … « je ne différencie pas du tout filles et garçons, il n'y a pas besoin, c'est totalement inutile et connaissant un petit peu les demoiselles que j'avais cette année en terminale, si elle s'était rendu compte que je commençait à différencier les choses elle l'aurait mal pris et aurait eu tout à fait raison.» 

C'est effectivement les mécanismes de "bias" qui induit une -parfois involontaire- discrimination à l'encontre des filles.

Alors que faire concrètement ?

- D'une part ne pas cacher le problème, mais bien expliciter que comme dans beaucoup de domaine il y a un problème au niveau [du genre et de l'informatique](https://www.youtube.com/watch?v=U2GsoIBdGyw) comme l'explique [Isabelle Collet](https://fr.wikipedia.org/wiki/Isabelle_Collet) avec quelques spécifictés, mais aussi …

- Il y a des [solutions et des remèdes](https://blog.adatechschool.fr/comment-rendre-linformatique-aux-femmes-selon-isabelle-collet) y compris au niveau de [l'éducation essentiellement des garçons](https://www.lemonde.fr/blog/binaire/2019/12/19/femmes-et-sciences-et-si-cetait-une-affaire-de-mecs/) qui sont (pas uniquement mais principalement) les opérateurs de cette inégalité.

Dans cette formation nous aborderons spécifiquement ces aspects.
