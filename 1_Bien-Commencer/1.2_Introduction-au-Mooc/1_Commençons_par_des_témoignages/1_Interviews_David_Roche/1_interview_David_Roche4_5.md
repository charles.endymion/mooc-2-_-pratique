# Interview David Roche 4/5, enseigner l'informatique au secondaire

Cette série d'interviews a pour but de recueillir l'expérience d'enseignants et d'enseignantes de NSI (spécialité Numérique et Sciences Informatiques):
comment sont-ils arrivés à enseigner cette discipline, quelles difficultés ont-ils rencontrées, quelle pédagogie ont-ils mis en oeuvre, ...  ? Ils et elles témoignent pour partager leurs pratiques avec de jeunes venus dans l'enseignement de NSI.

## Interview de David Roche, enseignant en NSI au Lycée Guillaume Fichet de Bonneville

**Sommaire des 5 vidéos**

* 1/5 [David Roche, qui es-tu ?](./1_interview_David_Roche1_5.md) 
* 2/5 [Comment enseigner et préparer un cours de NSI ?](./1_interview_David_Roche2_5.md) 
* 3/5 [Pratiques en classe\.](./1_interview_David_Roche3_5.md)
* **4/5 Pratiques en conditions dégradées\.**
* 5/5 [Enjeux d'égalité entre filles et garçons\.](./1_interview_David_Roche5_5.md)


## 4/5 Pratiques en conditions dégradées ?

Comment peut-on adapter la démarche pédagogique en enseignement de NSI quand, par exemple, on manque d'ordinateurs ou quand on a des classes trop chargées ?

[![Interview David Roche 4/5](https://mooc-nsi-snt.gitlab.io/portail/assets/vignette-interview-david-roche-600x311.png)](https://files.inria.fr/LearningLab_public/C045TV/ITW/NSI-ITW-DROCHE-4.mp4)

Jusqu'à maintenant on se met dans le cas où on a de bonnes conditions pour cet enseignement : un ordinateur par élève, travail en demi-groupes.

... « en classe en entière ou avec une disponibilité partielle des machines, adapter la démarche pédagogique est une question difficile ... malheureusement il y a des collègues qui sont dans ces conditions dégradées là. Les activités peuvent très bien s'envisager ``sur papier´´ puisque tous les documents sont au format d'imprimable, le cours (qui ne doit pas s'envisager comme un cours magistral, mais un document mis à disposition des élèves pour résoudre les activités), les élèves pourraient travailler hors machine sur les documents et sur machine pour expérimenter l'heure suivante. »

- Ici la problématique de travailler sur des machines personnelles (hors temps scolaire voir même en temps scolaire) se pose de fait, avec toutes les problématiques que cela entraîne.

... « Quasiment toutes les activités sont ``débranchées´´, par exemple sur les graphes en terminale, avec bien entendu des exceptions, par exemple l'observation du réseau ou avec un logiciel de simulation mais tout le reste se fait en débranché La seule chose qui m'embête de faire en débranché c'est la programmation, c'est extrêmement difficile pour un débutant de faire cela sur papier, il y a vraiment besoin de faire une approche par essais-erreurs, à part cela, la programmation tout le reste peut se faire en débranché. »

- Cette discussion permet aussi de rappeler que le programme d'informatique ne se limite surtout pas à la programmation, qu'il y a plein de notions à découvrir et plein d'autres problématiques à aborder.
