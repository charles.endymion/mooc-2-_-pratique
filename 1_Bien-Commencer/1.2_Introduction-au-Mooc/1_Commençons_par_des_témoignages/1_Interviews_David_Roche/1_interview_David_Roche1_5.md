# Interview David Roche 1/5, enseigner l'informatique au secondaire

Cette série d'interviews a pour but de recueillir l'expérience d'enseignants et d'enseignantes de NSI (spécialité Numérique et Sciences Informatiques):
comment sont-ils arrivés à enseigner cette discipline, quelles difficultés ont-ils rencontrées, quelle pédagogie ont-ils mis en oeuvre, ...  ? Ils et elles témoignent pour partager leurs pratiques avec de jeunes venus dans l'enseignement de NSI.

## Interview de David Roche, enseignant en NSI au Lycée Guillaume Fichet de Bonneville

**Sommaire des 5 vidéos**

* **1/5 David Roche, qui es tu ?** 
* 2/5 [Comment enseigner et préparer un cours de NSI ?](./1_interview_David_Roche2_5.md) 
* 3/5 [Pratiques en classe\.](./1_interview_David_Roche3_5.md)
* 4/5 [Pratiques en conditions dégradées\.](./1_interview1_David_Roche4_5.md)
* 5/5 [Enjeux d'égalité entre filles et garçons\.](./1_interview_David_Roche5_5.md)


## 1/5 David Roche, qui es tu ?

Dans cette vidéo David Roche nous raconte son parcours de professeur de Physique qui l'a conduit à l'enseignement de NSI  

[![Interview David Roche 1/5](https://mooc-nsi-snt.gitlab.io/portail/assets/vignette-interview-david-roche-600x311.png)](https://files.inria.fr/LearningLab_public/C045TV/ITW/NSI-ITW-DROCHE-1.mp4)


### De la mise en place de l'enseignement de l'informatique dans le secondaire

Une expérimentation qui a aidé au début des années 2000 à mieux comprendre comment on allait enseigner l'informatique, avec le logiciel [Alice](http://edutechwiki.unige.ch/fr/Alice), ou des outils comme [processing](https://processing.org/), avant de participer à la mise en place des programmes actuels. De même en cours de physique, l'enseignement [MPI](https://fr.wikipedia.org/wiki/Mesures_physiques_et_informatique) était précurseur, avant l'introduction de l'algorithmique en 2009 en seconde puis la mise en place progessive de l'enseignement de l'informatique que nous connaissons aujourd'hui. Pour en savoir plus …

- [Le numérique pour apprendre le numérique ?](https://www.lemonde.fr/blog/binaire/2020/01/20/savez-vous-que-demain-on-pigera-tou%c2%b7te%c2%b7s-le-numerique/) nous donne un panorama de la mise en place de cet enseignement, dont l'enseignement de spécialité [ISN](https://fr.wikipedia.org/wiki/Informatique_et_sciences_du_num%C3%A9rique).

- Dans [Enseignement de l’informatique et informatique dans l’enseignement, 50 ans déjà !](https://www.lemonde.fr/blog/binaire/2021/04/02/enseignement-de-linformatique-et-informatique-dans-lenseignement-50-ans-deja/), Monique Grandbastien qui a -entre autres- contibuer à favoriser le développement de l’informatique dans le second degré en formant des professeurs, nous permet de [re]découvrir cette aventure au service de nos enfants, et on peut même s'amuser à [faire un quiz](https://www.lemonde.fr/blog/binaire/2021/05/21/enseignement-de-linformatique-quelle-histoire/) pour tester ses connaissances sur ce pan de notre histoire éducative et accéder à un [document de référence](https://edutice.archives-ouvertes.fr/edutice-03161804v2/document#page=2) à ce sujet.



