# Interview David Roche 2/5, enseigner l'informatique au secondaire

Cette série d'interviews a pour but de recueillir l'expérience d'enseignants et d'enseignantes de NSI (spécialité Numérique et Sciences Informatiques):
comment sont-ils arrivés à enseigner cette discipline, quelles difficultés ont-ils rencontrées, quelle pédagogie ont-ils mis en oeuvre, ...  ? Ils et elles témoignent pour partager leurs pratiques avec de jeunes venus dans l'enseignement de NSI.

## Interview de David Roche, enseignant en NSI au Lycée Guillaume Fichet de Bonneville

**Sommaire des 5 vidéos**

* 1/5 [David Roche, qui es-tu ?](./1_interview_David_Roche1_5.md) 
* **2/5 Comment enseigner et préparer un cours de NSI ?** 
* 3/5 [Pratiques en classe\.](./1_interview_David_Roche3_5.md)
* 4/5 [Pratiques en conditions dégradées\.](./1_interview_David_Roche4_5.md)
* 5/5 [Enjeux d'égalité entre filles et garçons\.](./1_interview_David_Roche5_5.md)

## 2/5 Comment enseigner et préparer un cours de NSI ?

Dans cette vidéo David Roche nous explique comment et pourquoi il en est venu à mettre au point sa méthode d'enseignement de NSI

[![Interview David Roche 2/5](https://mooc-nsi-snt.gitlab.io/portail/assets/vignette-interview-david-roche-600x311.png)](https://files.inria.fr/LearningLab_public/C045TV/ITW/NSI-ITW-DROCHE-2.mp4)

### Comment s'est fait cet enseignement de l'informatique ?

... « c'était un saut dans l'inconnu puisque [dans les années 2000] personne n'avait envisagé [en France] d'enseigner l'informatique au secondaire à des adolescent·e·s d'environ 15 ans. Alors on est parti de ce qu'on connaissait, c'est-à-dire un cours, au tableau, expliquer des choses à nos élèves et puis après on fera des TPs, en reprenant le schéma de l'époque en sciences physiques. Et là, on s'est très vite rendu compte que, bah, très simplement quand on expliquait ce qu'est une boucle ou une conditions exetera au tableau ... il y avait les élèves qui s'endormaient ;) » ... 

« alors il fallait être peu plus un petit peu plus actif et éviter les cours magistraux tout simplement. Donc là, j'ai commencé à écrire des documents et les élèves suivaient les documents et moi j'étais là uniquement pour les aider. Donc cela réclamaint un gros travail en amont, cat il fallait tout écrire à l'avance et puis cela a commencé a relativement bien marché. » 

- L'ensemble des ressources sont disponibles ici sous une forme remaniée de [manuel SNT et ISN en ligne](https://pixees.fr/informatiquelycee) librement réutilisable, tout le programme y est implémenté.

.. « Au départ c'était des "PDF" à imprimer, et ensuite je suis passé au site internet, et la forme en est restée stable quelques années, l'idée était tout simplement d'avoir des  explications et puis des "à faire vous-même" (on en reparlera dans la suite). L'idée est donc d'expliquer une notion par écrit, les élèves essayent de faire des exemples, puis essayent de faire des choses de plus en plus complexe et moi je suis là pour uniquement pendant la séance "circuler" dans les rangs et regarder un petit peu comment avancent les élèves, commence illes réussissent à résoudre tous ces problèmes et surtout pour répondre à leurs questions parce que certain·e·s ont malgré tout besoin d'explication de vive vois, avec parfois,quelques difficultés avec l'écrit, pour expliquer exactement ce que j'avais écrit sur le site : c'est absolument fondamental ce côté auditif ».

 - On est donc dans une démarche d'apprentissage en autonomie qui va permettre à l'enseignant une disponibilité pour observer, évaluer et améliorer ses ressources pédagogiques et mettre en place une pédagogie différenciée selon les besoin de chaque élève.

### Comment un nouveau prof peut s'approprier cette démarche ?

.. et profiter de ces ressources faire travailler ses élèves les accompagnant ?

.. « toutes ces ressources à disposition sont un point de départ ... il faut éviter de les donner telles quelles aux élèves parce que c'est une façon personelle de voir, une façon de penser, ce n'est peut-être même pas forcément la bonne, enfin si jamais il y a une ``bonne façon´´ de voir ou penser. Vraiment, il faut d'abord s'imprénier de ces documents, puis oser repartir d'une page blanche nourri·e de ces documents. Il n'y a pas mieux pour maîtriser ce qu'on fait et faire quelque chose de personnel, de repartir de la page blanche, après avoir lu les documents et laissé passer un peu de temps. Puis repartir en disant bon alors moi je vois ça plutôt ça ... et j'encourage vivement les collègues, si illes sont repartie·e·s de que j'ai fait pour faire complètement autre chose, pour re-partager ce qui a été fait par elles et eux mêmes. C'est un des buts de ce MOOC de pouvoir mettre à diposition de tout le monde le fruit de leur travail. »

« Au niveau de la méthode, ce n'est plus directement du [HTML](https://fr.wikipedia.org/wiki/Hypertext_Markup_Language) mais les textes sont en [markdown](https://fr.wikipedia.org/wiki/Markdown) pour être facilement éditable avec toute une chaîne de production qui permet de transformer ces sources en HTML et, ce sont ces fichiers modifiables qui sont partagés sur [gitub](https://fr.wikipedia.org/wiki/GitHub) dont on peut faire ce qu'on veut qui sont licence [Creative Commons 0](https://creativecommons.org/publicdomain/zero/1.0/deed.fr). »

- Le dépôt de partage des sources est [disponible ici](https://github.com/dav74).

On apprendra dans ce MOOC à manipuler les outils en question (dépôt GIT et génération des documents finaux).
