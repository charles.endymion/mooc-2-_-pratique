# Interview Tessa Lelièvre-Osswald 5/5, enseigner l'informatique au secondaire

Cette série d'interviews a pour but de recueillir l'expérience d'enseignants et d'enseignantes de NSI (spécialité Numérique et Sciences Informatiques):
comment sont-ils arrivés à enseigner cette discipline, quelles difficultés ont-ils rencontrées, quelle pédagogie ont-ils mis en oeuvre, ...  ? Ils et elles témoignent pour partager leurs pratiques avec de jeunes venus dans l'enseignement de NSI.

## Interview de Tessa Lelièvre-Osswald, enseignante en NSI

**Sommaire des 5 vidéos**

* 1/5 [1/5 Tessa Lelièvre-Osswald, qui es-tu et quel est ton parcours ?](./1_interview_Tessa_Lelievre-Osswald1_5.md) 
* 2/5 [Comment se former et préparer ses cours ?](./1_interview_Tessa_Lelievre-Osswald2_5.md) 
* 3/5 [Pratique en classe\.](./1_interview_Tessa_Lelievre-Osswald3_5.md)
* 4/5 [Enseignement adapté aux différents élèves\.](./1_interview_Tessa_Lelievre-Osswald4_5.md)
* **5/5 Conseils aux autres collègues\.](./1_interview_Tessa_Lelievre-Osswald5_5.md)**


## 5/5 Conseils aux autres collègues 

_"C’est très important déjà de savoir qu'il faut continuer à apprendre : par exemple, moi-même, je n'avais pas été formée à tout ce qui est hardware [à contrario] de mes études très théoriques ; J'ai dû me former par moi-même et en discutant avec d'autres collègues ..."_

[![Interview Tessa Lelièvre-Osswald 5/5](https://mooc-nsi-snt.gitlab.io/portail/assets/vignette-interview-Tessa-600x311.png)](https://files.inria.fr/LearningLab_public/C045TV/ITW/NSI-ITW-TL-5.mp4)


### De la mise en place de l'enseignement de l'informatique dans le secondaire

Une expérimentation qui a aidé au début des années 2000 à mieux comprendre comment on allait enseigner l'informatique, avec le logiciel [Alice](http://edutechwiki.unige.ch/fr/Alice), ou des outils comme [processing](https://processing.org/), avantd e participer à la mise en place des programmes actuels. De même en cours de physique l'enseignement [MPI](https://fr.wikipedia.org/wiki/Mesures_physiques_et_informatique) était précurseur, avant l'introduction de l'algorithmique en 2009 en seconde puis la mise en place progessive de l'enseignement de l'informatique que nous connaissons aujourd'hui. Pour en savoir plus …

- [Le numérique pour apprendre le numérique ?](https://www.lemonde.fr/blog/binaire/2020/01/20/savez-vous-que-demain-on-pigera-tou%c2%b7te%c2%b7s-le-numerique/) nous donne un panorama de la mise en place de cet enseignement, dont l'enseignement de spécialité [ISN](https://fr.wikipedia.org/wiki/Informatique_et_sciences_du_num%C3%A9rique).

- Dans [Enseignement de l’informatique et informatique dans l’enseignement, 50 ans déjà !](https://www.lemonde.fr/blog/binaire/2021/04/02/enseignement-de-linformatique-et-informatique-dans-lenseignement-50-ans-deja/), Monique Grandbastien qui a -entre autres- contibuer à favoriser le développement de l’informatique dans le second degré en formant des professeurs, nous permet de [re]découvrir cette aventure au service de nos enfants, et on peut même s'amuser à [faire un quiz](https://www.lemonde.fr/blog/binaire/2021/05/21/enseignement-de-linformatique-quelle-histoire/) pour tester ses connaissances sur ce pan de notre histoire éducative et accéder à un [document de référence](https://edutice.archives-ouvertes.fr/edutice-03161804v2/document#page=2) à ce sujet.



