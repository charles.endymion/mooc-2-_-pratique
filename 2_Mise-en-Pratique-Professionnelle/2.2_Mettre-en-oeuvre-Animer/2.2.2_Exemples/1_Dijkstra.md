# Manipulation de graphes, recherche de chemin

Dans cet exemple, nous vous proposons deux approches pour l'initiation aux algorithmes de recherche de plus court chemin dans un graphe valué.

## Une activité pas à pas

Maxime Fourny nous propose une introduction de l'algorithme de Dijkstra, d'abord par une suite de manipulations par les élèves sur des exemples de graphes puis par une vidéo qui _déroule_ l'algorithme pas à pas.

- L'activité élève est donc constituée de deux éléments :
    1. [une suite d'activités](https://gitlab.com/sebhoa/mooc-2-ressources/-/blob/main/1_Penser-Concevoir-Elaborer/fiche_eleve_dijkstra.pdf) pour manipuler les graphes et la recherche d'un plus court chemin
    2. cette [vidéo d'introduction à l'algorithme de Dijkstra](https://www.youtube.com/watch?v=9YBq5bcw1pU)


La [fiche prof de l'activité](https://gitlab.com/sebhoa/mooc-2-ressources/-/blob/main/1_Penser-Concevoir-Elaborer/fiche_prof_dijkstra_maxime_fourny.md) à télécharger et à discuter sur le forum : [discussion autours de cette activité](https://mooc-forums.inria.fr/moocnsi/t/introduction-a-lalgorithme-de-dijkstra/3467).
