# Un premier exemple d'activité et de fiche _prof_

Charles Poulmaire a préparé une séance de cours niveau Terminale NSI, pour introduire les bases de données. 

- [La fiche élève](https://gitlab.com/sebhoa/mooc-2-ressources/-/blob/main/1_Penser-Concevoir-Elaborer/fiche_eleve_decouverte_BDR_charles.pdf)
- [Sa fiche _prof_ de l'activité](https://gitlab.com/sebhoa/mooc-2-ressources/-/blob/main/1_Penser-Concevoir-Elaborer/fiche_prof_decouverte_BDR_charles.md)

On retrouve sur le Forum, la [discussion autours de cette activité](https://mooc-forums.inria.fr/moocnsi/moocnsi/t/fiche-decouverte-des-bases-de-donnees-relationnelles-cp/3334) où pourrons être débattus quelques points d'attention parmi :

- L'objectif a-t-il bien été identifié ?
- Est-il clairement situé dans la progression générale de la classe au regard des programmes ou référentiels finaux ?
- Quels sont les acquis antérieurs sur lesquels je dois absolument articuler les nouvelles connaissances à transmettre ? Comment vais-je les faire ressortir ?
- L'objectif est-il accessible aux élèves sans remises à niveau ou remises au point ? En cas de doute, quels sont les points à vérifier ?
- Puis-je pronostiquer que certains élèves seront en difficulté, d'entrée de jeu, pour aborder cet objectif ? Pourquoi ? Comment je peux tenter d'aplanir ces difficultés ?