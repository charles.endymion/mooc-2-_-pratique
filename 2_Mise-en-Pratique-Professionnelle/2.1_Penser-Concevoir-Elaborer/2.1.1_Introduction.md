Dans cette première séquence du _Bloc 2. Mise en pratique_ nous vous proposons de vous entraîner à proposer un contenu de cours adapté aux élèves. Ce contenu pourra être recherché et adapté dans une banque de ressources ou construit entièrement.

# Comprendre les intentions d'une activité

L'acte d'enseigner ne se résume pas à être un exerciseur, c'est-à-dire enchaîner des exercices et leurs corrigés. Ainsi, lorsqu'un enseignant conçoit une progression sur un concept donné, plusieurs activités vont se succéder pour construire les apprentissages des élèves. Il ne faut pas bien sûr réinventer la roue. Même si certaines des activités peuvent être construites _from scratch_ par l'enseignant, bien souvent on est amené à réutiliser une activité d'un collègue, d'un document ressource voire même des activités trouvées sur le Web.

Avant d'être donnée aux élèves, une activité se doit d'être interrogée, questionnée. Une activité contient des intentions de la part du concepteur qui sont le plus souvent implicites. Ainsi, avant de proposer une activité, il est nécessaire de se l'approprier en se questionnant :

- quels objectifs en terme de connaissances et de compétences visées ;
- quel positionnement de l'activité dans la progressivitée annuelle, les pré-requis ;
- quels exercices "cibles" de l'activité au regard des objectifs de celle-ci ;
- quelle durée de l'activité ;
- quel déroulement prévu de l'activité ;
- comment anticiper les démarches que les élèves vont mettre en place ;
- comment anticiper les difficultés que vont rencontrer les élèves ;
- quel étayage pour lever ces difficultés ;
- comment gérer l'hétérogénéité de cette activité.


# Ressources

Nous voilà donc en train d'apprendre en faisant : on commence par la pratique, on prendra du recul dans une deuxième temps.

Pour se préparer nous offrons deux outils :

- Nous vous invitons à lire la [deuxième partie](lien.vers.le.pdf), du livre "Enseigner l'Informatique" de Werner Hartmann qui propose des principes pour choisir les contenus de cours et d'activités :

1. Différencier et maîtriser concepts et produits : chapitre 6
2. Adapter le contenu au public visé : chapitre 7
3. Mettre en avant les Idées Fondamentales : chapitre 8

Suite à la lecture des ces chapitres, vous pourrez répondre au [quiz](LIEN A AJOUTER).

- Nous vous proposons de regarder quelques [exemples](LIEN A AJOUTER) pour vous inspirer, voir même les reprendre, ou au contraire s'en détacher pour créer vous-même la ressource en question.

Vous avez pris connaissance de toute cela ? Alors à vous ! Le travail sollicité est le suivant :

1. Préparer ou chercher l'activité élève et la mettre à disposition sur votre espace gitlab
2. Rédiger la fiche prof qui contiendra le titre de votre activité élève ainsi que :
    - le lien vers la fiche élève de l'activité
    - une liste de métas-informations, on vous guide ici :

## La fiche _prof_ d'une activité : choisir les métas-informations

Voici les métas-informations qu'il est courant de renseigner, cela nous aide à gérer nos ressources personelles, et à les partager, les faire évoluer à partir d'autres, etc… Cela permet surtout de bien expliciter sa démarche pédagogique. Ce format est inspiré à la fois des ressources de sites comme la [Main à la pâte](https://www.fondation-lamap.org/) et des standards officiels comme la [LOMfr](https://fr.wikipedia.org/wiki/Learning_Object_Metadata) (mais sa lourdeur :) …)

**Thématique :** Quel est le thème principal de l'activité ? On pourra ici donner un lien vers le programme officiel par exemple.

**Notions liées :** Seront précisées ici les notions directement rattachées à l'activité ou les sous-thèmes en relation avec le sujet principal.

**Résumé de l’activité :** Une description de ce que propose l'activité ; s'il s'agit d'une suite d'exercices ou d'une activité débranchée, s'il s'agit de programmer en utilisant un module particulier de Python etc.

**Objectifs :** Pourquoi propose-t-on cette activité ? Quelle acquisition de connaissance est visée ? Quelle manipulation favorise le développement d'une compétence ?

**Auteur :**

**Durée de l’activité :**

**Forme de participation :** individuelle ? en binôme ? en groupe ? en autonomie ?

**Matériel nécessaire :**

**Préparation :**

**Autres références :** liens éventuels vers d'autres cours ou activités similaires ou complémentaires

Une fiche _prof_ vierge est disponible ici : [Fiche prof vierge](../Ressources/00_fiche-prof-modele.md)
