# Accompagner l'individu et le collectif

Dans les modules précédents, nous avons créés des activités pour les élèves en détaillant la mise en oeuvre en classe. Il va s'agir dans ce troisième module de préciser comment accompagner chaque élève dans son apprentissage.

Qu'il soit en difficulté ou en avance, chaque élève est singulier et demande souvent de proposer des activités supplémentaires, de remédiation ou d'explorations complémentaires.

Nous vous proposons de commencer ce module par la lecture de la [quatrième partie](lien.vers.le.pdf) de l'ouvrage de Hartmann _etal_. Les auteurs nous présentent plusieurs méthodes d'enseignement : la pédagogie expérientielle, le travail de groupe, les programmes dirigés, les apprentissages par la découverte ou encore la pédagogie de projet. 

Autant de méthodes qui peuvent vous donner des idées pour proposer, en complément de votre activité initiale, une autre activité, individuelle ou collective.

Cette lecture pourra se compléter de la [cinquième partie](lien.vers.le.pdf) qui aborde les techniques d'enseignement. Parmi ces techniques l'une consiste à rendre concrètes des notions abstraites par le jeu des représentations énactiques, iconiques et symboliques.


## Travail demandé

Plusieurs types d'activité peuvent constituer le contenu de ce troisième module. On pourra par exemple :

- Proposer une activité de remédiation pour des élèves en difficulté
- Proposer une activité d'approfondissement d'une activité existante ; préciser s'il s'agit d'une activité pour la classe entière ou une activité supplémentaire/complémentaire proposée uniquement aux élèves en avance
- Proposer un projet en précisant :
    - les connaissances travaillées
    - le format pédagogique (durée, seul, binôme, groupe...)
    - les étayages pour assurer une progression des élèves
    - le calendrier (points d'étapes, rendus intermédiaires...)

L'activité produite pour ce module pourra soit faire référence à des activités proposées aux modules 1 et 2 soit être sans lien. Dans tous les cas, il faudra bien préciser non seulement la nature de l'activité mais également son contexte.